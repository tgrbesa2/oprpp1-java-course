package hr.fer.zemris.java.gui.layouts;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

public class LayoutTester {
	
	@Test
	public void testPreferredSize1() {
		JPanel p = new JPanel(new CalcLayout(2));
		JLabel l1 = new JLabel(""); l1.setPreferredSize(new Dimension(10,30));
		JLabel l2 = new JLabel(""); l2.setPreferredSize(new Dimension(20,15));
		p.add(l1, new RCPosition(1,1));
		p.add(l2, new RCPosition(3,3));
		Dimension dim = p.getPreferredSize();
		
		assertEquals(dim.getWidth(), 152);
		assertEquals(dim.getHeight(), 158);
	}
	
	@Test
	public void testPreferredSize2() {
		JPanel p = new JPanel(new CalcLayout(2));
		JLabel l1 = new JLabel(""); l1.setPreferredSize(new Dimension(108,15));
		JLabel l2 = new JLabel(""); l2.setPreferredSize(new Dimension(16,30));
		p.add(l1, new RCPosition(1,1));
		p.add(l2, new RCPosition(3,3));
		Dimension dim = p.getPreferredSize();
		
		assertEquals(dim.getWidth(), 152);
		assertEquals(dim.getHeight(), 158);
	}
	
	@Test
	public void testCalcException1() {
		JPanel p = new JPanel(new CalcLayout());
		JLabel l1 = new JLabel("");

		
		assertThrows(CalcLayoutException.class, () -> p.add(l1, new RCPosition(0,1)));
	}
	
	@Test
	public void testCalcException2() {
		JPanel p = new JPanel(new CalcLayout());
		JLabel l1 = new JLabel("");

		
		assertThrows(CalcLayoutException.class, () -> p.add(l1, new RCPosition(6,1)));
	}
	
	@Test
	public void testCalcException3() {
		JPanel p = new JPanel(new CalcLayout());
		JLabel l1 = new JLabel("");

		
		assertThrows(CalcLayoutException.class, () -> p.add(l1, new RCPosition(2,0)));
	}
	
	@Test
	public void testCalcException4() {
		JPanel p = new JPanel(new CalcLayout());
		JLabel l1 = new JLabel("");

		
		assertThrows(CalcLayoutException.class, () -> p.add(l1, new RCPosition(1,84)));
	}
	
	@Test
	public void testCalcException5() {
		JPanel p = new JPanel(new CalcLayout());
		JLabel l1 = new JLabel("");

		
		assertThrows(CalcLayoutException.class, () -> p.add(l1, new RCPosition(1,5)));
	}
	
	@Test
	public void testCalcException6() {
		JPanel p = new JPanel(new CalcLayout());
		JLabel l1 = new JLabel("");
		JLabel l2 = new JLabel("");
		
		p.add(l2, new RCPosition(2, 2));

		
		assertThrows(CalcLayoutException.class, () -> p.add(l1, new RCPosition(2,2)));
	}
}
