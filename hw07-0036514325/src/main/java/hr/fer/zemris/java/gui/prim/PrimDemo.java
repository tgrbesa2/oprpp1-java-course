package hr.fer.zemris.java.gui.prim;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class PrimDemo extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PrimListModel<Integer> plm;
	
	public PrimDemo() {
		super();
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Prim Demo");
		setSize(500, 200);
		plm = new PrimListModel<>();
		initGUI();
	}
	
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		
		JList<Integer> list1 = new JList<>(plm);
		JList<Integer> list2 = new JList<>(plm);
		
		JPanel p = new JPanel(new GridLayout(1, 0));
		p.add(new JScrollPane(list1));
		p.add(new JScrollPane(list2));
		
		cp.add(p, BorderLayout.CENTER);
		
		JButton next = new JButton("Dodaj idući prim broj");
		cp.add(next, BorderLayout.PAGE_END);
		
		next.addActionListener(e-> {
			plm.next();
		});
		
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new PrimDemo().setVisible(true);
		});
	}
}
