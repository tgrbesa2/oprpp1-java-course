package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.model.CalcModel;


/**
 * Part of strategy pattern used in CalcGUI.
 * Every button contains different strategy
 * depending of function button has to do.
 * @author tgrbesa
 *
 */
public interface StrategyFunction {
	/**
	 * Executes specified function or list of functions
	 * depending on button functionality.
	 * @param cm
	 */
	void execute(CalcModel cm);
}
