package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import javax.swing.JComponent;

/**
 * Implementation of drawing one BarChart using
 * paintComponent function in JComponent.
 * @author tgrbesa
 *
 */
public class BarChartComponent extends JComponent{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BarChart component;
	
	public BarChartComponent(BarChart component) {
		this.component = component;
		this.setBackground(Color.WHITE);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		//Distance for y-axis
		int leftLength = 10 * (Integer.toString(component.getMaxY())).length() + 20;
		//Formatter for writing of y-axis values
		String formatter = "%" + Integer.toString(component.getMaxY()).length() + "d";
		//Width distance between two bars
		double distanceX = (1.0 * (this.getHeight() - leftLength) / (this.getHeight())) / component.getListValues().size();
		
		//Drawing of x descritpion
		g.drawString(component.getxDescription(),(int)((this.getWidth() / 2) - (this.getWidth() * 0.07)),
				this.getHeight() - 5);
		
		//Axis drawing
		g.setColor(Color.GRAY);
		g.drawLine(leftLength, (int) (this.getHeight() - 40),
				(int) (leftLength + component.getListValues().size() * distanceX * this.getWidth() + 10), (int) (this.getHeight() - 40));
		g.drawLine(leftLength, (int) (this.getHeight() * 0.02),
				leftLength, (int) (this.getHeight() - 40));
		
		//Triangle at end of axis drawing
		int[] xpoints = new int[3];
		xpoints[0] = (int) (leftLength + component.getListValues().size() * distanceX * this.getWidth() + 10);
		xpoints[1] = xpoints[0];
		xpoints[2] = xpoints[0] + 10;
		
		int[] ypoints = new int[3];
		ypoints[0] = (int) (this.getHeight() - 35);
		ypoints[1] = (int) (this.getHeight() - 45);
		ypoints[2] = (int) (this.getHeight() - 40);

		g.drawPolygon(xpoints, ypoints, 3);
		
		xpoints[0] = leftLength - 5;
		xpoints[1] = leftLength + 5;
		xpoints[2] = leftLength;
		
		ypoints[0] = (int) (this.getHeight() * 0.02);
		ypoints[1] = ypoints[0];
		ypoints[2] = (int) (this.getHeight() * 0.02 - 10);
		
		g.drawPolygon(xpoints, ypoints, 3);
		
		
		for(int i = 0; i < component.getListValues().size() + 1; i++) {
			
			//Drawing of 'tracks' on axis
			g.setColor(Color.GRAY);
			g.drawLine((int)(leftLength + this.getWidth() * distanceX * i),
					(int) (this.getHeight() - 40), 
					(int)(leftLength + this.getWidth() * distanceX * i),
					(int) (this.getHeight() - 40 + 7));
			
			g.setColor(Color.BLACK);
			g.setFont(new Font("monospaced", Font.BOLD, 15));
			
			//X numbers writing
			if(i < component.getListValues().size()) {
				g.drawString(String.format("%d", component.getListValues().get(i).getX()), (int) (leftLength + (this.getWidth() * distanceX * 0.5) + this.getWidth() * distanceX * i),
						(int) (this.getHeight() - 25));
			}
			
			//Cell writing
			g.setColor(Color.ORANGE);
			if(i > 0) {
				g.drawLine((int)(leftLength + this.getWidth() * distanceX * i), 
						(int) (this.getHeight() - 40),
						(int) (leftLength + this.getWidth() * distanceX * i),
						(int) (this.getHeight() * 0.02));
			}
		}
		
		int j = 0;
		for(int i = component.getMinY(); i < component.getMaxY() + component.getDistanceY(); i += component.getDistanceY()) {
			double distance = (1.0 * (this.getHeight() - 40) / (this.getHeight())) / ((component.getMaxY() - component.getMinY()) + 1);
			
			
			g.setColor(Color.GRAY);
			g.drawLine((int) (leftLength - 7),
					(int)(this.getHeight() - 40 - j * distance * this.getHeight()),
					(int) (leftLength),
					(int)(this.getHeight() - 40 - j * distance * this.getHeight()));
			
			g.setColor(Color.BLACK);
			g.drawString(String.format(formatter, i), 15, (int) (this.getHeight() - 35 - distance * j * this.getHeight()));
			
			g.setColor(Color.orange);
			
			if(j != 0) {
				g.drawLine((int) (leftLength),
						(int) (this.getHeight() - 40 - this.getHeight() * distance * j),
						(int) (leftLength + component.getListValues().size() * distanceX * this.getWidth() + 10),
						(int) (this.getHeight() - 40 - this.getHeight() * distance * j));
			}
			j += component.getDistanceY();
			
		}
		
		g.setColor(Color.RED);
		for(int i = 0; i < component.getListValues().size(); i++) {
			double distanceWidth = (1.0 * (this.getHeight() - leftLength) / (this.getHeight())) / component.getListValues().size();
			double distanceHeight = (1.0 * (this.getHeight() - 40) / (this.getHeight())) / ((component.getMaxY() - component.getMinY()) + 1);
			g.fillRect((int) (leftLength + 2 + this.getWidth() *  distanceWidth * i),
					(int) (this.getHeight() - 40 - this.getHeight() * distanceHeight * (component.getListValues().get(i).getY() - component.getMinY())),
					(int) (this.getWidth() *  distanceWidth - 2),
					(int) (this.getHeight() * distanceHeight * (component.getListValues().get(i).getY() - component.getMinY())));
		}
		
		g.setFont(new Font("Dialog", Font.PLAIN, 12));

		
		g.setColor(Color.BLACK);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setTransform(AffineTransform.getQuadrantRotateInstance(3));
		
		g2d.drawString(component.getyDescription(), (int)-(this.getHeight() / 2), 12);
	}
}
