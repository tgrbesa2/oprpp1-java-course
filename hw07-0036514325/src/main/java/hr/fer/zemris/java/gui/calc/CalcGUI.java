package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleBinaryOperator;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * GUI configuration and main program
 * for calculator.
 * @author tgrbesa
 *
 */
public class CalcGUI extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CalcModelImpl cmi = new CalcModelImpl();
	private List<UnaryButton> unaryButtons = new ArrayList<>();
	
	public CalcGUI() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("CalculatorGUI");
		setSize(700, 500);
		initGUI();
		
	}
	
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new CalcLayout(2));
		
		cp.add(screen(cmi.toString()), "1,1");
		cp.add(button("=", EQUALS), "1,6");
		cp.add(button("clr", (cal) -> cal.clear()) , "1,7");
		cp.add(button("7", (cal) -> cal.insertDigit(7)), "2,3");
		cp.add(button("8", (cal) -> cal.insertDigit(8)), "2,4");
		cp.add(button("9", (cal) -> cal.insertDigit(9)), "2,5");
		cp.add(button("/", binaryOperatorFactory((v1, v2) -> v1 / v2)), "2,6");
		cp.add(button("reset", (cal) -> cal.clearAll()), "2,7");
		cp.add(button("4", (cal) -> cal.insertDigit(4)), "3,3");
		cp.add(button("5", (cal) -> cal.insertDigit(5)), "3,4");
		cp.add(button("6", (cal) -> cal.insertDigit(6)), "3,5");
		cp.add(button("*", binaryOperatorFactory((v1, v2) -> v1 * v2)), "3,6");
		cp.add(button("push", null), "3,7");
		cp.add(button("1", (cal) -> cal.insertDigit(1)), "4,3");
		cp.add(button("2", (cal) -> cal.insertDigit(2)), "4,4");
		cp.add(button("3", (cal) -> cal.insertDigit(3)), "4,5");
		cp.add(button("-", binaryOperatorFactory((v1, v2) -> v1 - v2)), "4,6");
		cp.add(button("pop", null), "4,7");
		cp.add(button("0", (cal) -> cal.insertDigit(0)), "5,3");
		cp.add(button("+/-", (cal) -> cal.swapSign()), "5,4");
		cp.add(button(".", (cal) -> cal.insertDecimalPoint()), "5,5");
		cp.add(button("+", binaryOperatorFactory((v1, v2) -> v1 + v2)), "5,6");
		
		
		unaryButtons = unaryButtonFactory();
		int j = 0;
		for(int i = 2; i < 6; i++) {
			cp.add(unaryButtons.get(j++), new RCPosition(i, 1));
			cp.add(unaryButtons.get(j++), new RCPosition(i, 2));
			
		}

		JCheckBox jcb = new JCheckBox("Inv");
		jcb.addItemListener(l -> {
			for(var ub : unaryButtons) {
				ub.inverse();
			}
		});
		cp.add(jcb, "5,7");
		
	}
	
	/**
	 * Model for calculator screen.
	 * @param text
	 * @return
	 */
	private JLabel screen(String text) {
		JLabel labela = new JLabel(text);
		labela.setBackground(Color.YELLOW);
		labela.setOpaque(true);
		labela.setHorizontalAlignment(SwingConstants.RIGHT);
		cmi.addCalcValueListener(model -> labela.setText(model.toString()));
		return labela;
	}
	
	/**
	 * Model for calculator basic buttons
	 * that execute single function.
	 * @param text
	 * @param sf
	 * @return
	 */
	private JButton button(String text, StrategyFunction sf) {
		JButton button = new JButton(text);
		button.setBackground(Color.LIGHT_GRAY);
		if(sf != null) {
			button.addActionListener(l -> {
				sf.execute(cmi);
			});
		}
		return button;
	}
	
	/**
	 * Method factory for binary operator buttons
	 * @param dbo
	 * @return
	 */
	private StrategyFunction binaryOperatorFactory(DoubleBinaryOperator dbo) {
		StrategyFunction tmp = (model) -> {
			if(!model.isActiveOperandSet()) {
				model.setActiveOperand(model.getValue());
				model.setPendingBinaryOperation(dbo);
				model.freezeValue(String.format("%f", model.getActiveOperand()));
				model.clear();
			} else {
				model.setActiveOperand(model.getPendingBinaryOperation()
						.applyAsDouble(model.getActiveOperand(), model.getValue()));
				model.setPendingBinaryOperation(dbo);
				model.freezeValue(String.format("%f", model.getActiveOperand()));
				model.clear();
			}
		};
		return tmp;
	}
	
	/**
	 * Unary button factory that returns
	 * all buttons with unary operations
	 * that need to listen on inv checkbox changes
	 * @return
	 */
	private List<UnaryButton> unaryButtonFactory() {
		List<UnaryButton> tmp = new ArrayList<>();
		tmp.add(new UnaryButton("1/x", "1/x", l -> INV.execute(cmi), l -> INV.execute(cmi)));
		tmp.add(new UnaryButton("sin", "arcsin", l -> SIN.execute(cmi), l-> ARCSIN.execute(cmi)));
		tmp.add(new UnaryButton("log", "10^x", l -> LOG.execute(cmi), l-> EXP10.execute(cmi)));
		tmp.add(new UnaryButton("cos", "arccos", l -> COS.execute(cmi), l-> ARCCOS.execute(cmi)));
		tmp.add(new UnaryButton("ln", "e^x", l -> LN.execute(cmi), l-> EXPE.execute(cmi)));
		tmp.add(new UnaryButton("tan", "arctan", l -> TAN.execute(cmi), l-> ARCTAN.execute(cmi)));
		tmp.add(new UnaryButton("x^n", "x^(1/n)", l -> POW.execute(cmi), l-> INVPOW.execute(cmi)));
		tmp.add(new UnaryButton("ctg", "arcctg", l -> CTG.execute(cmi), l-> ARCTG.execute(cmi)));

		return tmp;
	}
	
	/**
	 * Implementation of strategyfunction
	 * for button equals
	 */
	private StrategyFunction EQUALS = (model) -> {
		if(model.isActiveOperandSet()) {
			model.setValue(model.getPendingBinaryOperation()
					.applyAsDouble(model.getActiveOperand(), model.getValue()));
			model.clearActiveOperand();
		}
	};
	
	/**
	 * Strategies for invertible buttons!
	 */
	private StrategyFunction INV = (model) -> model.setValue(1.0 / model.getValue());
	private StrategyFunction SIN = (model) -> model.setValue(Math.sin(model.getValue()));
	private StrategyFunction ARCSIN = (model) -> model.setValue(Math.asin(model.getValue()));
	private StrategyFunction LOG = (model) -> model.setValue(Math.log10(model.getValue()));
	private StrategyFunction EXP10 = (model -> model.setValue(Math.pow(10, model.getValue())));
	private StrategyFunction COS = (model) -> model.setValue(Math.cos(model.getValue()));
	private StrategyFunction ARCCOS = (model) -> model.setValue(Math.acos(model.getValue()));
	private StrategyFunction LN = (model) -> model.setValue(Math.log(model.getValue()));
	private StrategyFunction EXPE = (model) -> model.setValue(Math.pow(Math.E, model.getValue()));
	private StrategyFunction TAN = (model) -> model.setValue(Math.tan(model.getValue()));
	private StrategyFunction ARCTAN = (model) -> model.setValue(Math.atan(model.getValue()));
	private StrategyFunction CTG = (model) -> model.setValue(1.0 / Math.tan(model.getValue()));
	private StrategyFunction ARCTG = (model) -> model.setValue(Math.atan(1.0 / model.getValue()));
	private StrategyFunction POW = (model) -> {
		model.setActiveOperand(model.getValue());
		model.freezeValue(String.format("%f", model.getActiveOperand()));
		model.setPendingBinaryOperation((v1, v2) -> Math.pow(v1, v2));
		model.clear();
	};
	private StrategyFunction INVPOW = (model) -> {
		model.setActiveOperand(model.getValue());
		model.freezeValue(String.format("%f", model.getActiveOperand()));
		model.setPendingBinaryOperation((v1, v2)-> Math.pow(v1, 1.0 / v2));
		model.clear();
	};


	public static void main(String[] args) {
		SwingUtilities.invokeLater(()-> {
			new CalcGUI().setVisible(true);
		});
	}
}
