package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Container;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Main program that takes one argument while running from command
 * prompt: file location. File needs to be specificaly written
 * for main program to parse document and create GUI
 * that paints chart.
 * @author tgrbesa
 *
 */
public class BarChartDemo extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BarChartDemo() {
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setSize(1000, 500);
		this.setTitle("BarChart");
		initGUI();
	}
	
	private void initGUI() {
		Container cp = getContentPane();
		cp.setBackground(Color.WHITE);
	}
	
	public static void main(String[] args) {
		List<XYValue> lista = new ArrayList<>();
		
		Path p = Paths.get(args[0]);
		StringBuilder sb = new StringBuilder();
		
		try(InputStream is = Files.newInputStream(p)) {
			byte[] buff = new byte[4096];
			while(true) {
				int r = is.read(buff);
				if(r < 1) break;
				sb.append(new String(Arrays.copyOf(buff, r), "UTF-8"));
			}
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading");
			System.exit(-1);
		}
		
		String[] allStrings = sb.toString().split("\n");
		String[] values = allStrings[2].trim().replaceAll(" +", " ").split(" ");
		for(var s : values) {
			String[] tmp = s.split(",");
			lista.add(new XYValue(Integer.parseInt(tmp[0]), Integer.parseInt(tmp[1])));
		}
		
		
		BarChart bc = new BarChart(lista, allStrings[0],
				allStrings[1],
				Integer.parseInt(allStrings[3]),
				Integer.parseInt(allStrings[4]),
				Integer.parseInt(allStrings[5]));
		BarChartComponent bcc = new BarChartComponent(bc);
		
		BarChartDemo bcd = new BarChartDemo();
		bcd.getContentPane().add(bcc);
		
		SwingUtilities.invokeLater(() -> {
			bcd.setVisible(true);
		});
	}
}
