package hr.fer.zemris.java.gui.calc;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.function.DoubleBinaryOperator;

import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalcValueListener;
import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;

/**
 * Implementation of functionalities of one
 * simple calculator.
 * @author tgrbesa
 *
 */
public class CalcModelImpl implements CalcModel {
	private boolean isEditable = true;
	private boolean isPositive = true;
	private String currentNumberString = "";
	private double currentNumberDouble = 0;
	private String frozenValue = null;
	private OptionalDouble activeOperand = OptionalDouble.empty();
	private DoubleBinaryOperator pendingOperator = null;
	private List<CalcValueListener> listeners = new ArrayList<>();

	@Override
	public void addCalcValueListener(CalcValueListener l) {
		listeners.add(l);
	}

	@Override
	public void removeCalcValueListener(CalcValueListener l) {
		listeners.remove(l);
	}

	@Override
	public double getValue() {
		return currentNumberDouble;
	}

	@Override
	public void setValue(double value) {
		this.currentNumberDouble = value;
		currentNumberString = String.format("%f", value);
		//isEditable = false;
		
		alertListeners();
	}

	@Override
	public boolean isEditable() {
		return isEditable;
	}

	@Override
	public void clear() {
		currentNumberString = "";
		currentNumberDouble = 0;
		isPositive = true;
		
		alertListeners();
	}

	@Override
	public void clearAll() {
		clear();
		frozenValue = null;
		activeOperand = OptionalDouble.empty();
		pendingOperator = null;
		
		alertListeners();
	}

	@Override
	public void swapSign() throws CalculatorInputException {
		if(!isEditable) {
			throw new CalculatorInputException();
		}
		if(isPositive) {
			currentNumberString = "-" + currentNumberString;
		} else {
			currentNumberString = currentNumberString.replace("-", "");
		}
		isPositive = (!isPositive);
		currentNumberDouble *= (-1.0);
		
		alertListeners();
	}

	@Override
	public void insertDecimalPoint() throws CalculatorInputException {
		if(currentNumberString.contains(".") || !isEditable || currentNumberString.equals("-") || currentNumberString.equals("")) {
			throw new CalculatorInputException();
		}
		
		currentNumberString += ".";
		
		alertListeners();
	}

	@Override
	public void insertDigit(int digit) throws CalculatorInputException, IllegalArgumentException {
		if(!isEditable) {
			throw new CalculatorInputException();
		}
		if(currentNumberString.equals("0") && digit == 0 && !currentNumberString.contains(".")) return;
		if(currentNumberString.length() >= 308) throw new CalculatorInputException();

		freezeValue(null);
		currentNumberString += digit;
		try {
			currentNumberDouble = Double.parseDouble(currentNumberString);
		} catch (Exception ex) {
			currentNumberString = currentNumberString.substring(0, currentNumberString.length() - 1);
			throw new CalculatorInputException("Wrong number added for parse!");
		}
		
		alertListeners();
	}

	@Override
	public boolean isActiveOperandSet() {
		return activeOperand.isPresent();
	}

	@Override
	public double getActiveOperand() throws IllegalStateException {
		if(!activeOperand.isPresent()) throw new IllegalStateException();
		
		return activeOperand.getAsDouble();
	}

	@Override
	public void setActiveOperand(double argument) {
		activeOperand = OptionalDouble.of(argument);
		
		alertListeners();
	}

	@Override
	public void clearActiveOperand() {
		if(activeOperand.isPresent()) {
			activeOperand = OptionalDouble.empty();
		}
		
		alertListeners();
	}

	@Override
	public DoubleBinaryOperator getPendingBinaryOperation() {
		return pendingOperator;
	}

	@Override
	public void setPendingBinaryOperation(DoubleBinaryOperator op) {
		pendingOperator = op;
		
		alertListeners();
	}
	
	@Override
	public String toString() {
		if(frozenValue != null) {
			return frozenValue;
		}
		
		if(currentNumberString.equals("-") || currentNumberString.equals("+") || currentNumberString.equals("") || currentNumberString.equals("0")) {
			if(!isPositive) {
				return "-0";
			}
			else {
				return "0";
			}
		}
		if(currentNumberString.startsWith(".")) {
			currentNumberString = "0" + currentNumberString;
		}
		
		if(currentNumberString.startsWith("-.")) {
			currentNumberString = "-0" + currentNumberString.substring(1);
		}
		
		if(currentNumberString.startsWith("0") && !currentNumberString.contains("."))  {
			currentNumberString = currentNumberString.substring(1);
		}
		return currentNumberString;
	}
	
	private void alertListeners() {
		for(var l : listeners) {
			l.valueChanged(this);
		}
	}
	
	public void freezeValue(String value) {
		frozenValue = value;
	}

}