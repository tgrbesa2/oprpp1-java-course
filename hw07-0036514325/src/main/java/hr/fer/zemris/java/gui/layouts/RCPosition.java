package hr.fer.zemris.java.gui.layouts;

/**
 * Class that contains information of position of some component
 * in GUI. Get functions return row number or column number.
 * 
 * @author tgrbesa
 *
 */
public class RCPosition {
	private int row;
	private int column;
	
	public RCPosition(int row, int column) {
		super();
		this.row = row;
		this.column = column;
	}

	public int getRow() {
		return row;
	}
	
	public int getColumn() {
		return column;
	}
	
	/**
	 * Static method factory that parses
	 * string into new RCPosition
	 * @param text
	 * @return
	 */
	public static RCPosition parse(String text) {
		return new RCPosition(Integer.parseInt(text.split(",")[0]), Integer.parseInt(text.split(",")[1]));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof RCPosition))
			return false;
		RCPosition other = (RCPosition) obj;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}
	
	
}
