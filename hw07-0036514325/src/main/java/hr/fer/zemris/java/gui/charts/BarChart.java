package hr.fer.zemris.java.gui.charts;

import java.util.List;

/**
 * Class contains all information of some chart.
 * @author tgrbesa
 *
 */
public class BarChart {
	private List<XYValue> listValues;
	private String xDescription;
	private String yDescription;
	private int minY;
	private int maxY;
	private int distanceY;
	public BarChart(List<XYValue> listValues, String xDescription, String yDescription, int minY, int maxY,
			int distanceY) {
		super();
		if(minY < 0) throw new IllegalArgumentException("minY must be positive value or 0!");
		if(minY >= maxY) throw new IllegalArgumentException("maxY must be bigger than minY");
		
		for(var value : listValues) {
			if(value.getY() < minY) throw new IllegalArgumentException("Value " + value.getY() + " must be greater than" + minY);
		}
		while((maxY - minY) % distanceY != 0) {
			maxY++;
		}
		
		this.listValues = listValues;
		this.xDescription = xDescription;
		this.yDescription = yDescription;
		this.minY = minY;
		this.maxY = maxY;
		this.distanceY = distanceY;
	}
	public List<XYValue> getListValues() {
		return listValues;
	}
	public String getxDescription() {
		return xDescription;
	}
	public String getyDescription() {
		return yDescription;
	}
	public int getMinY() {
		return minY;
	}
	public int getMaxY() {
		return maxY;
	}
	public int getDistanceY() {
		return distanceY;
	}
	
	
	
	
}
