package hr.fer.zemris.java.gui.layouts;

/**
 * Exceptions that occur in CalcLayout.
 * @author tgrbesa
 *
 */
public class CalcLayoutException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public CalcLayoutException() {
		super();
	}
	
	public CalcLayoutException(String message) {
		super(message);
	}
	
}
