package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.JButton;

/**
 * Extension of JButton for CalcGUI.
 * It is different from normal JButton becuase
 * it contains two functions: one normal and one inverted.
 * @author tgrbesa
 *
 */
public class UnaryButton extends JButton {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String normalText;
	private String inverseText;
	private ActionListener normalOperation;
	private ActionListener inverseOperation;


	public UnaryButton(String normalText, String inverseText, ActionListener normalOperation,
			ActionListener inverseOperation) {
		super(normalText);
		this.normalText = normalText;
		this.inverseText = inverseText;
		this.normalOperation = normalOperation;
		this.inverseOperation = inverseOperation;

		
		this.setBackground(Color.LIGHT_GRAY);
		
		this.addActionListener(normalOperation);

	}
	
	public void inverse() {
		if(this.getActionListeners()[0].equals(normalOperation)) {
			this.removeActionListener(normalOperation);
			this.addActionListener(inverseOperation);
			this.setText(inverseText);
		} else {
			this.removeActionListener(inverseOperation);
			this.addActionListener(normalOperation);
			this.setText(normalText);
		}
	}
	
}
