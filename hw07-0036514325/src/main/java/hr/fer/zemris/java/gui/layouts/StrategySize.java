package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Dimension;

/**
 * Interface for strategy used for
 * calculating of dimension of whole layout.
 * @author tgrbesa
 *
 */
public interface StrategySize {
	Dimension size(Component comp);
}
