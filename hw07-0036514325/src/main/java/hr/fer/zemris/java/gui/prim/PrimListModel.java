package hr.fer.zemris.java.gui.prim;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * Implementation of ListModel that
 * on call of method next generates new prim number.
 * @author tgrbesa
 *
 * @param <E>
 */
public class PrimListModel<E> implements ListModel<E> {
	private int n;
	private List<Integer> listOfPrimes = new ArrayList<>();
	private List<ListDataListener> observers = new ArrayList<>();
	
	public PrimListModel() {
		listOfPrimes.add(1);
		n = 2;
	}
	
	public int next() {
		for(int i = n;;i++) {
			if(isPrime(i)) {
				listOfPrimes.add(i);
				
				ListDataEvent e = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, listOfPrimes.size() - 1, listOfPrimes.size() - 1);
				for(var l : observers) {
					l.intervalAdded(e);
				}
				
				n++;
				return i;
			}
			n++;
		}
	}
	
	
	private static boolean isPrime(int number) {
		for(int i = 2; i < number; i++) {
			if(number % i == 0) {
				return false;
			}
		}
		return true;
	}
	
	public List<Integer> getLista() {
		return listOfPrimes;
	}

	@Override
	public int getSize() {
		return listOfPrimes.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public E getElementAt(int index) {
		return (E) listOfPrimes.get(index);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		observers.add(l);
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		observers.remove(l);
	}
}
