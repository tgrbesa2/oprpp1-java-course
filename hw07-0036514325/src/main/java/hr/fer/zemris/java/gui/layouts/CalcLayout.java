package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.HashMap;
import java.util.Map;


/**
 * Implementation of layout manager designed for
 * simple calculator.
 * @author tgrbesa
 *
 */
public class CalcLayout implements LayoutManager2 {
	private int spaceBetween;
	
	private Map<RCPosition, Component> mapComponents;
	

	
	/**
	 * Constructor that takes one argument:
	 * defined space between components.
	 * @param spaceBetween
	 */
	public CalcLayout(int spaceBetween) {
		this.spaceBetween = spaceBetween;
		mapComponents = new HashMap<>();
	}
	
	/**
	 * Basic constructor. Sets space between
	 * components to 0.
	 */
	public CalcLayout() {
		this(0);
	}
	@Override
	public void addLayoutComponent(String name, Component comp) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		RCPosition rcp = null;
		for(var entry : mapComponents.keySet()) {
			if(mapComponents.get(entry).equals(comp)) {
				rcp = entry;
				break;
			}
		}
		
		mapComponents.remove(rcp);
	}

	
	private Dimension getLayoutSize(StrategySize s, Container parent) {
		int width = 0;
		int height = 0;
		
		for(Map.Entry<RCPosition, Component> entry : mapComponents.entrySet()) {
			int compHeight = s.size(entry.getValue()).height;
			int compWidth = s.size(entry.getValue()).width;
			System.out.println(compHeight);
			System.out.println(compWidth);
		
		if(entry.getKey().getRow() == 1 && entry.getKey().getColumn() == 1) {
			compWidth = compWidth - 4*spaceBetween;
			compWidth = compWidth / 5;
			
		}
		
		if(compHeight > height) {
			height = compHeight;
		}
		if(compWidth > width) {
			width = compWidth;
		}
	}
	
	Insets inset = parent.getInsets();
	return new Dimension(width * 7 + 6 * spaceBetween + inset.right + inset.left,
			height * 5 + 4 * spaceBetween + inset.bottom + inset.top);
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		return getLayoutSize((c) -> c.getMinimumSize(), parent);
	}
	
	@Override
	public Dimension maximumLayoutSize(Container target) {
		return getLayoutSize((c) -> c.getMaximumSize(), target);
	}
	
	@Override
	public Dimension preferredLayoutSize(Container parent) {
		return getLayoutSize((c) -> c.getPreferredSize(), parent);
	}

	@Override
	public void layoutContainer(Container parent) {

		int height = (int) ((parent.getHeight() - 4 * spaceBetween) / 5 + 0.5);
		int width = (int) ((parent.getWidth() - 6 * spaceBetween) / 7 + 0.5);
		
		int numberHeight = (parent.getHeight() - (4 * spaceBetween + height * 5));
		int numberWidth = (parent.getWidth() - (6 * spaceBetween + width * 7));
		System.out.println(numberHeight);
		
		int alreadyAddedHeight = 0;
		int alreadyAddedWidth = 0;
		
		int counterHeight = numberHeight;
		int counterWidth = 0;
		
		int heightAddOne = 0;
		int widthAddOne = 0;
		
		
		for(int i = 1; i < 6; i++) {
			counterWidth = numberWidth;
			for(int j = 1; j < 8; j++) {
				if(i == 1 && j == 1) {
					Component tmp1 = mapComponents.get(new RCPosition(i,j));
					Component tmp2 = mapComponents.get(new RCPosition(1, 6));
					Component tmp3 = mapComponents.get(new RCPosition(1, 7));

					switch(numberWidth) {
					case 0:
						tmp1.setBounds(0, 0, 4 * spaceBetween + 5 * width, height);
						tmp2.setBounds(5 * (spaceBetween + width), 0, width, height);
						tmp3.setBounds(6 * (spaceBetween + width), 0, width, height);
						break;
					case 1:
						tmp1.setBounds(0, 0, 4 * spaceBetween + 5 * width, height);
						tmp2.setBounds(5 * (spaceBetween + width), 0, width, height);
						tmp3.setBounds(6 * (spaceBetween + width), 0, width + 1, height);
						break;
					case 2:
						tmp1.setBounds(0, 0, 4 * spaceBetween + 5 * width + 1, height);
						tmp2.setBounds(5 * (spaceBetween + width) + 1, 0, width, height);
						tmp3.setBounds(6 * (spaceBetween + width) + 1, 0, width + 1, height);
						break;
					case 3:
						tmp1.setBounds(0, 0, 4 * spaceBetween + 5 * width + 2, height);
						tmp2.setBounds(5 * (spaceBetween + width) + 2, 0, width, height);
						tmp3.setBounds(6 * (spaceBetween + width) + 2, 0, width + 1, height);
						break;
					case 4:
						tmp1.setBounds(0, 0, 4 * spaceBetween + 5 * width + 2, height);
						tmp2.setBounds(5 * (spaceBetween + width) + 2, 0, width + 1, height);
						tmp3.setBounds(6 * (spaceBetween + width) + 3, 0, width + 1, height);
						break;
					case 5:
						tmp1.setBounds(0, 0, 4 * spaceBetween + 5 * width + 3, height);
						tmp2.setBounds(5 * (spaceBetween + width) + 3, 0, width + 1, height);
						tmp3.setBounds(6 * (spaceBetween + width) + 4, 0, width + 1, height);
						break;
					case 6:
						tmp1.setBounds(0, 0, 4 * spaceBetween + 5 * width + 4, height);
						tmp2.setBounds(5 * (spaceBetween + width) + 4, 0, width + 1, height);
						tmp3.setBounds(6 * (spaceBetween + width) + 5, 0, width + 1, height);
						break;
					}
					break;
				}
				
				Component tmp = mapComponents.get(new RCPosition(i, j));
				heightAddOne = 0;
				widthAddOne = 0;
				
				if(counterHeight >= 5) {
					heightAddOne++;
				}
				
				if(counterWidth >= 7) {
					widthAddOne++;
					counterWidth = counterWidth % 7;
				}
				
				tmp.setBounds((j - 1) * (width + spaceBetween) + alreadyAddedWidth, (i - 1) * (height + spaceBetween) + alreadyAddedHeight,
						width + widthAddOne, height + heightAddOne);
				if(widthAddOne == 1) {
					alreadyAddedWidth++;
				}
				
				counterWidth += numberWidth;
			}
			if(heightAddOne == 1) {
				alreadyAddedHeight++;
			}
			if(counterHeight >= 5) {
				counterHeight = counterHeight % 5;
			}
			counterHeight += numberHeight;
			alreadyAddedWidth = 0;
		}

		
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		if(constraints == null) {
			throw new NullPointerException();
		}
	
		if(!(constraints instanceof String || constraints instanceof RCPosition)) {
			throw new IllegalArgumentException("Constraint is not String or RCPosition!");
		}
		
		RCPosition position = null;
		if(constraints instanceof String) {
			position = RCPosition.parse((String) constraints);
		}
		
		if(constraints instanceof RCPosition) {
			position = (RCPosition) constraints;
		}
		
		if(position.getRow() < 1 || position.getRow() > 5 || position.getColumn() < 1 || position.getColumn() > 7) {
			throw new CalcLayoutException("Error in defined row, column position!");
		}
		
		if(position.getRow() == 1 && position.getColumn() < 6 && position.getColumn() > 1) {
			throw new CalcLayoutException("Error in first row.");
		}
		
		if(mapComponents.containsKey(position)) {
			throw new CalcLayoutException("Element with same constraint already contained!");
		}
		
		mapComponents.put(position, comp);
	}



	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0;
	}

	@Override
	public void invalidateLayout(Container target) {		
	}

}
