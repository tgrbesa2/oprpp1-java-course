package hr.fer.zemris.math;

import static java.lang.Math.cos;
import static java.lang.Math.hypot;
import static java.lang.Math.sin;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



public class Complex {
	/**
	 * Private double variables and they represent complex number.
	 */
	private double real;
	private double imaginary;
	
	public static final Complex ZERO = new Complex(0,0);
	public static final Complex ONE = new Complex(1,0);
	public static final Complex ONE_NEG = new Complex(-1,0);
	public static final Complex IM = new Complex(0,1);
	public static final Complex IM_NEG = new Complex(0,-1);
	
	/**
	 * Constructor that accpets two arguments.
	 * @param realPart real part of complex number.
	 * @param imaginaryPart imaginary part of complex number.
	 */
	public Complex(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}
	
	/**
	 * Basic constructor that returns
	 * 0 + 0i complex number.
	 */
	public Complex() {
		this(0, 0);
	}
	
	public double getReal() {
		return real;
	}

	public double getImaginary() {
		return imaginary;
	}

	/**
	 * Returns module of complex number.
	 * @return
	 */
	public double module() {
		return hypot(real, imaginary);
	}
	
	/**
	 * Creates and returns complex number from magnitude and angle;
	 * @param magnitude
	 * @param angle
	 * @return
	 */
	public static Complex fromMagnitudeAndAngle(double magnitude, double angle) {
		return new Complex(cos(angle) * magnitude, sin(angle) * magnitude);
	}
	
	/**
	 * Method calculates sumation of two complex numbers.
	 * @param c
	 * @return new complex number as a result of sumation.
	 */
	public Complex add(Complex c) {
		return new Complex(this.real + c.real, this.imaginary + c.imaginary);
	}
	
	/**
	 * Method calculates subtraction of two complex numbers.
	 * @param c
	 * @return new complex number as a result of subtraction.
	 */
	public Complex sub(Complex c) {
		return new Complex(this.real - c.real, this.imaginary - c.imaginary);
	}
	
	/**
	 * Method calculates multiplication of two complex numbers.
	 * @param c
	 * @return new complex number as a result of multiplication.
	 */
	public Complex mul(Complex c) {
		return new Complex(this.real * c.real - this.imaginary * c.imaginary, this.real * c.imaginary + this.imaginary * c.real);
	}
	
	/**
	 * Method calculates division of two complex numbers.
	 * @param c
	 * @return new complex number as a result of division.
	 */
	public Complex div(Complex c) {
		Complex conjugate = new Complex(c.real, -1 * c.imaginary);
		Complex pom = this.mul(conjugate);

		return new Complex(pom.real / (c.module() * c.module()), pom.imaginary / (c.module() * c.module()));
	}
	
	/**
	 * Method calculates power of <code>n</code> on complex number.
	 * @param n
	 * @return new complex number as a result of operation power.
	 * @throws IllegalArgumentException If n is less than 0.
	 */
	public Complex power(int n) {
		if(n < 0) throw new IllegalArgumentException("n must be greater than or equal to 0!");
		
		return fromMagnitudeAndAngle(Math.pow(this.module(), n), this.getAngle() * n);
	}
	
	/**
	 * Method calculates root of <code>n</code> on complex number.
	 * @param n
	 * @return new complex number as a result of operation root.
	 * @throws IllegalArgumentException If n is less than or equal 0.
	 */
	public List<Complex> root(int n) {
		if(n <= 0) throw new IllegalArgumentException("n must be greater than 0!");
		
		Complex[] roots = new Complex[n];
		
		for(int i = 0; i < n; i++) {
			roots[i] = fromMagnitudeAndAngle(Math.pow(this.module(), 1.0D / n), (this.getAngle() + 2 * i * Math.PI) / n);
		}
		return Arrays.stream(roots).collect(Collectors.toList());
		
	}
	
	/**
	 * Method returns new Complex number as result
	 * of negating <code>this</code> Complex number.
	 * @return
	 */
	public Complex negate() {
		return new Complex(-this.real, -this.imaginary);
	}
	
	/**
	 * Returns angle of complex number in range from 0 to 2Pi.
	 * @return
	 */
	public double getAngle() {
		return Math.atan2(this.imaginary, this.real);

	}
	
	/**
	 * Method returns complex number parsed from string.
	 * @return
	 */
	public static Complex parse(String str) {
		if(str.isEmpty()) throw new IllegalArgumentException("Wrong input");
		
		double realPart = 0.0;
		double imaginaryPart = 0.0;
		
		if(str.startsWith("+")) str = str.substring(1, str.length());
		
		str = str.replaceAll(" ", "");
		
		if(str.contains("+")) {
			int idx = str.indexOf("+");
			
			if(str.substring(idx + 1, str.length()).equals("i")) {
				imaginaryPart = 1.0;
			} else {
				imaginaryPart = Double.parseDouble(str.substring(idx + 2, str.length()));
			}
			
			realPart = Double.parseDouble(str.substring(0, idx));
		} else if(str.lastIndexOf("-") > 0) {
			int idx = str.lastIndexOf("-");
			
			if(str.substring(idx + 1, str.length()).equals("i")) {
				imaginaryPart = -1.0;
			} else {
				imaginaryPart = -1.0 * Double.parseDouble(str.substring(idx + 2, str.length()));
			}
			
			realPart = Double.parseDouble(str.substring(0, idx));
		} else {
			if(str.contains("i")) {
				switch(str) {
				case "i":
					imaginaryPart = 1.0;
					break;
				case "-i":
					imaginaryPart = -1.0;
					break;
				default:
					imaginaryPart  = Double.parseDouble(str.replace("i", ""));
				}
			} else {
				realPart = Double.parseDouble(str);
			}
		}
		return new Complex(realPart, imaginaryPart);
	}
	
	

	
	@Override
	public String toString() {
		String tmp = "(";
		tmp += real;
		if(imaginary >= 0) {
			tmp += "+";
		} else {
			tmp += "-";
		}
		tmp += "i";
		tmp += Math.abs(imaginary);
		tmp += ")";
		return tmp;

	}
}
