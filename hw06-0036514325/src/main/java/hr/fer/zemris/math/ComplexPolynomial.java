package hr.fer.zemris.math;

public class ComplexPolynomial {
	private Complex[] factors;
	
	/**
	 * Constructor.
	 * @param factors
	 */
	public ComplexPolynomial(Complex ... factors) {
		this.factors = factors;
	}
	
	/**
	 * Returns order of the polynomial.
	 * @return
	 */
	public short order() {
		return (short)(factors.length - 1);
	}
	
	/**
	 * Method computes multiplication of
	 * this polynomial with <code>p</code>
	 * other polynomial.
	 * @param p
	 * @return
	 */
	public ComplexPolynomial multiply(ComplexPolynomial p) {
		Complex[] tmp = new Complex[this.order() + p.order() + 1];
		
		for(int i = 0; i < this.factors.length; i++) {
			for(int j = 0; j < p.factors.length; j++) {
				if(tmp[i + j] == null) {
					tmp[i + j] = this.factors[i].mul(p.factors[j]);
				} else {
					tmp[i + j] = tmp[i + j].add(this.factors[i].mul(p.factors[j]));
				}
			}
		}
		return new ComplexPolynomial(tmp);
	}
	
	/**
	 * Method computes first derivative of this polynomial.
	 * @return
	 */
	public ComplexPolynomial derive() {
		Complex[] tmp = new Complex[this.order()];
		
		for(int i = 0; i < tmp.length; i++) {
			tmp[i] = new Complex(factors[i].getReal() * (this.order() - i), factors[i].getImaginary() * (this.order() - i));
		}
		
		return new ComplexPolynomial(tmp);
	}
	
	/**
	 * Computes polynomial value at given point z.
	 * @param z
	 * @return
	 */
	public Complex apply(Complex z) {
		Complex tmp = new Complex();
		
		for(int i = 0; i < factors.length; i++) {
			tmp = tmp.add(factors[i].mul(z.power(this.order() - i)));
		}
		
		return tmp;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0; i < factors.length - 1; i++) {
			sb.append(String.format("%s%s%d%s", factors[i], "*z^", this.order() - i, "+"));
		}
		sb.append(factors[factors.length - 1]);
		return sb.toString();
	}
	

}
