package hr.fer.zemris.math;

public class ComplexRootedPolynomial {
	private Complex constant;
	private Complex[] roots;
	
	/**
	 * Constructor of rooted polynomial
	 * as z0*(z-z1)*(z-z2)... where z0 is constant
	 * and z1,z2... are roots.
	 * @param constant
	 * @param roots
	 */
	public ComplexRootedPolynomial(Complex constant, Complex ... roots) {
		this.constant = constant;
		this.roots = roots;
	}
	
	/**
	 * Computes polynomial value at given point z
	 * @param z
	 * @return
	 */
	public Complex apply(Complex z) {
		Complex tmp = constant.mul(z.sub(roots[0]));
		
		for(int i = 1; i < roots.length; i++) {
			tmp = tmp.mul(z.sub(roots[i]));
		}
		
		return tmp;
	}
	
	/**
	 * Method converts this representation of
	 * ComplexRootedPolynomial to ComplexPolynomial
	 * @return
	 */
	public ComplexPolynomial toComplexPolynom() {
		ComplexPolynomial[] tmpArray = new ComplexPolynomial[roots.length];
		ComplexPolynomial tmpValue = null;
		
		for(int i = 0; i < roots.length; i++) {
			tmpArray[i] = new ComplexPolynomial(Complex.ONE, roots[i]);
		}
		
		tmpValue = tmpArray[0];
		for(int i = 1; i < tmpArray.length; i++) {
			tmpValue = tmpValue.multiply(tmpArray[i]);
		}
		
		return tmpValue.multiply(new ComplexPolynomial(this.constant));
		
	}
	
	// finds index of closest root for given complex number z that is within
	// treshold; if there is no such root, returns -1
	// first root has index 0, second index 1, etc
	/**
	 * Finds index of closest root for given complex number z
	 * that is within treshold; if there is no such root, returns -1
	 * first root hasi index 0, second index 1, etc.
	 * @param z
	 * @param treshold
	 * @return
	 */
	public int indexOfClosestRoot(Complex z, double treshold) {
		int currentIndex = -1;
		double currentDistance = treshold;

		for(int i = 0; i < roots.length; i++) {
			if(z.sub(roots[i]).module() < currentDistance) {
				currentIndex = i;
			}
		}
		
		return currentIndex;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(constant.toString());
		
		for(int i = 0; i < roots.length; i++) {
			sb.append("*(z-");
			sb.append(roots[i].toString());
			sb.append(")");
		}
		
		return sb.toString();	
	}
}
