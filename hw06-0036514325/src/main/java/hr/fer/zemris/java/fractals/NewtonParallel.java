package hr.fer.zemris.java.fractals;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

public class NewtonParallel {
	private static ComplexRootedPolynomial crp;
	private static ComplexPolynomial cp;
	
	private static OptionalInt noOfThreads = OptionalInt.empty();
	private static OptionalInt noOfJobs = OptionalInt.empty();

	
	public static void main(String[] args) {
		if(args.length != 0) {
			boolean flag = false;
			for(int i = 0; i < args.length; i++) {
				if(flag) {
					flag = false;
					continue;
				}
				if(args[i].contains("--")) {
					if(args[i].substring(2).startsWith("workers") && noOfThreads.isEmpty()) {
						noOfThreads = OptionalInt.of(Integer.parseInt(args[i].substring(args[i].indexOf("=") + 1)));
					} else if(noOfJobs.isEmpty()) {
						noOfJobs = OptionalInt.of(Integer.parseInt(args[i].substring(args[i].indexOf("=") + 1)));
					}
					
					
				} else if(args[i].contains("-w") && noOfThreads.isEmpty()) {
					if((i + 1) < args.length) {
						noOfThreads = OptionalInt.of(Integer.parseInt(args[i + 1]));
						flag = true;
					}
					
					
				} else if(args[i].contains("-t") && noOfJobs.isEmpty()){
					if((i + 1) < args.length) {
						noOfJobs = OptionalInt.of(Integer.parseInt(args[i + 1]));
						flag = true;
					}
				} else {
					throw new RuntimeException("Error ocurred while reading parameters!");
				}
			}
			
		}
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.\n"
				+ "Please enter at least two roots, one root per line. Enter 'done' when done.");
		
		List<Complex> list = new ArrayList<Complex>();
		
		while(true) {
			
			System.out.print("Root " + (list.size() + 1) + "> ");
			String str = sc.nextLine();

			if(str.equals("done")) {
				if(list.size() < 2) {
					System.out.println("Please enter at least two roots!");
					continue;
				}
				Complex[] carr = new Complex[list.size()];
				list.toArray(carr);
				crp = new ComplexRootedPolynomial(Complex.ONE, carr);
				cp = crp.toComplexPolynom();
				
				break;
			}
			
			try {
				list.add(Complex.parse(str));
			} catch (Exception ex) {
				System.out.println("Wrong input, try again:)");
			}
		}
		
		sc.close();
		
		FractalViewer.show(new MojProducer());
	}
	
	public static class PosaoIzracuna implements Runnable {
		double reMin;
		double reMax;
		double imMin;
		double imMax;
		int width;
		int height;
		int yMin;
		int yMax;
		int m;
		short[] data;
		AtomicBoolean cancel;
		public static PosaoIzracuna NO_JOB = new PosaoIzracuna();
		
		private PosaoIzracuna() {
		}
		
		public PosaoIzracuna(double reMin, double reMax, double imMin,
				double imMax, int width, int height, int yMin, int yMax, 
				int m, short[] data, AtomicBoolean cancel) {
			super();
			this.reMin = reMin;
			this.reMax = reMax;
			this.imMin = imMin;
			this.imMax = imMax;
			this.width = width;
			this.height = height;
			this.yMin = yMin;
			this.yMax = yMax;
			this.m = m;
			this.data = data;
			this.cancel = cancel;
		}
		
		@Override
		public void run() {
			for(int y = yMin; y <= yMax; y++) {
				for(int x = 0; x < width; x++) {
					double cre = x / (width-1.0) * (reMax - reMin) + reMin;
					double cim = (height-1.0-y) / (height-1) * (imMax - imMin) + imMin;
					Complex zn = new Complex(cre, cim);
					Complex znold;
					int iter = 0;
					do {
						Complex first = cp.apply(zn);
						Complex second = cp.derive().apply(zn);
						znold = new Complex(zn.getReal(), zn.getImaginary());
						zn = zn.sub(first.div(second));
						iter++;
					} while((znold.sub(zn).module()) > 0.001 && iter < m);
					
					int index = crp.indexOfClosestRoot(zn, 0.002);
					data[y * height + x] = (short)(index+1);
				}
			}
		}
	}
	
	public static class MojProducer implements IFractalProducer {
		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax,
				int width, int height, long requestNo, IFractalResultObserver observer, AtomicBoolean cancel) {
			System.out.println("Zapocinjem izracun...");
			int m = 16*16*16;
			short[] data = new short[width * height];
			if(noOfThreads.isEmpty()) {
				noOfThreads = OptionalInt.of(Runtime.getRuntime().availableProcessors());
			}
			
			if(noOfJobs.isEmpty()) {
				noOfJobs = OptionalInt.of(4 * Runtime.getRuntime().availableProcessors());
			}
			
			if(noOfJobs.getAsInt() < 1) noOfJobs = OptionalInt.of(1);
			if(noOfJobs.getAsInt() > height) noOfJobs = OptionalInt.of(height);
			
			System.out.println("Broj dretvi: " + noOfThreads.getAsInt());
			System.out.println("Broj poslova: " + noOfJobs.getAsInt());
			
			int brojYPoTraci = height / noOfJobs.getAsInt();
			
			final BlockingQueue<PosaoIzracuna> queue = new LinkedBlockingQueue<>();

			Thread[] radnici = new Thread[noOfThreads.getAsInt()];
			for(int i = 0; i < radnici.length; i++) {
				radnici[i] = new Thread(new Runnable() {
					@Override
					public void run() {
						while(true) {
							PosaoIzracuna p = null;
							try {
								p = queue.take();
								if(p==PosaoIzracuna.NO_JOB) break;
							} catch (InterruptedException e) {
								continue;
							}
							p.run();
						}
					}
				});
			}
			for(int i = 0; i < radnici.length; i++) {
				radnici[i].start();
			}
			
			for(int i = 0; i < noOfJobs.getAsInt(); i++) {
				int yMin = i*brojYPoTraci;
				int yMax = (i+1)*brojYPoTraci-1;
				if(i== noOfJobs.getAsInt()-1) {
					yMax = height-1;
				}
				PosaoIzracuna posao = new PosaoIzracuna(reMin, reMax, imMin, imMax, width, height, yMin, yMax, m, data, cancel);
				while(true) {
					try {
						queue.put(posao);
						break;
					} catch (InterruptedException e) {
					}
				}
			}
			for(int i = 0; i < radnici.length; i++) {
				while(true) {
					try {
						queue.put(PosaoIzracuna.NO_JOB);
						break;
					} catch (InterruptedException e) {
					}
				}
			}
			
			for(int i = 0; i < radnici.length; i++) {
				while(true) {
					try {
						radnici[i].join();
						break;
					} catch (InterruptedException e) {
					}
				}
			}
			
			System.out.println("Racunanje gotovo. Idem obavijestiti promatraca tj. GUI!");
			observer.acceptResult(data, (short)(cp.order() + 1), requestNo);
		}
	}
}
