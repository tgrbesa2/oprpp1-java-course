package hr.fer.zemris.java.fractals;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

public class Newton {
	private static ComplexRootedPolynomial crp;
	private static ComplexPolynomial cp;

	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.\n"
				+ "Please enter at least two roots, one root per line. Enter 'done' when done.");
		
		List<Complex> list = new ArrayList<Complex>();
		
		while(true) {
			
			System.out.print("Root " + (list.size() + 1) + "> ");
			String str = sc.nextLine();

			if(str.equals("done")) {
				if(list.size() < 2) {
					System.out.println("Please enter at least two roots!");
					continue;
				}
				Complex[] carr = new Complex[list.size()];
				list.toArray(carr);
				crp = new ComplexRootedPolynomial(Complex.ONE, carr);
				cp = crp.toComplexPolynom();
				
				break;
			}
			
			try {
				list.add(Complex.parse(str));
			} catch (Exception ex) {
				System.out.println("Wrong input, try again:)");
			}
		}
		
		sc.close();
		
		FractalViewer.show(new MyProducer());
	}
	
	public static class MyProducer implements IFractalProducer {

		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax
				, int width, int height, long requestNo,
				IFractalResultObserver observer, AtomicBoolean cancel) {
			System.out.println("Zapocinjem izracun...");
			int m = 16*16*16;
			int offset = 0;
			short[] data = new short[width * height];
			
			for(int y = 0; y < height; y++) {
				for(int x = 0; x < width; x++) {
					double cre = x / (width-1.0) * (reMax - reMin) + reMin;
					double cim = (height-1.0-y) / (height-1) * (imMax - imMin) + imMin;
					Complex zn = new Complex(cre, cim);
					Complex znold;
					int iter = 0;
					do {
						Complex first = cp.apply(zn);
						Complex second = cp.derive().apply(zn);
						znold = new Complex(zn.getReal(), zn.getImaginary());
						zn = zn.sub(first.div(second));
						iter++;
					} while((znold.sub(zn).module()) > 0.001 && iter < m);
					int index = crp.indexOfClosestRoot(zn, 0.002);
					data[offset++] = (short)(index+1);
				}
			}
			System.out.println("Racunanje gotovo. Obavjescujem GUI");
			observer.acceptResult(data, (short)(cp.order() + 1), requestNo);
			
		}

	}
}
