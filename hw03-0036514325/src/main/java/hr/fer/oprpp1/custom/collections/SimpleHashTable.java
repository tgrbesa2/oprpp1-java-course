package hr.fer.oprpp1.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Class represents hashtable data structure. 
 * @author tgrbesa
 *
 * @param <K>
 * @param <V>
 */
public class SimpleHashTable<K, V> implements Iterable<SimpleHashTable.TableEntry<K, V>>{
	private static final int DEFAULT_NO_SLOTS = 16;
	
	/**
	 * Static nested class that represents hashtable entry.
	 * @author tgrbesa
	 *
	 * @param <K>
	 * @param <V>
	 */
	public static class TableEntry<K, V> {
		private K key;
		private V value;
		private TableEntry<K, V> next;
		
		public TableEntry(K key, V value) {
			if(key == null) throw new NullPointerException("Key must be non-null reference!");
			
			this.key = key;
			this.value = value;
			this.next = null;
		}
		
		public K getKey() {
			return this.key;
		}
		
		public V getValue() {
			return this.value;
		}
		
		public void setValue(V value) {
			this.value = value;
		}
		
		public String toString() {
			return key.toString() + "=" + value.toString();
		}
	}
	
	
	/**
	 * Hash table.
	 */
	private TableEntry<K, V>[] table;
	
	/**
	 * Number of currently stored elements.
	 */
	private int size;
	
	/**
	 * Default constructor. Creates hashtable of 16 slots.
	 */
	public SimpleHashTable() {
		this(DEFAULT_NO_SLOTS);
	}
	
	/**
	 * Long variable that counts collection modification.
	 * When structural modification occurs, <code>modificationCount</code>
	 * increments with 1.
	 */
	private long modificationCount = 0;
	
	 
	
	/**
	 * Constructor with one argument: number of slots in hashtable
	 * @throws IllegalArgumentException if <code>numberOfSlots</code> is less than 1.
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashTable(int numberOfSlots) {
		if(numberOfSlots < 1) throw new IllegalArgumentException("Arguments must be bigger than or equal to 1!");
		
		double tmp = Math.log((double) numberOfSlots) / Math.log(2.0);
		tmp = Math.floor(tmp);
		
		if(Math.pow(2, tmp) < numberOfSlots) {
			tmp++;
		}
		
		//this.table = (TableEntry<K, V>[]) new Object[(int) Math.pow(2, tmp)];
		this.table = new TableEntry[(int) Math.pow(2, tmp)];
		this.table = (TableEntry<K, V>[]) this.table;
	}
	
	/**
	 * Returns size of hash table.
	 * @return
	 */
	public int size() {
		return this.size;
	}
	
	/**
	 * Returns true if hashtable is empty, false otherwise.
	 * @return
	 */
	public boolean isEmpty() {
		return this.size == 0;
	}
	
	/**
	 * Method that calculates hash value of <code>key</code> and puts
	 * new table entry into hash table. Place of putting is determined
	 * by hash value.
	 * @param key
	 * @param value
	 * @throws NullPointerException if key is null.
	 */
	public V put(K key, V value) {
		if(this.size / table.length > 0.75) {
			this.decreaseLoadFactor();
		}
		
		return this.putValue(key, value);
	}
	
	/**
	 * Returns value that is stored under <code>key</code>.
	 * If key is not in table, null is returned.
	 * @param key
	 * @return
	 */
	public V get(Object key) {
		if(key == null) return null;
		
		int hash = Math.abs(key.hashCode()) % table.length;
		TableEntry<K, V> tmp = table[hash];
		V value = null;
		
		while(tmp != null) {
			if(tmp.key.equals(key)) {
				value = tmp.value;
				break;
			}
			tmp = tmp.next;
		}
		
		return value;
	}
	
	/**
	 * Returns true if table contains <code>key</code>, false otherwise.
	 * @param key
	 * @return
	 */
	public boolean containsKey(Object key) {
		if(key == null) return false;
		
		int hash = Math.abs(key.hashCode()) % table.length;
		TableEntry<K, V> tmp = table[hash];
		boolean flag = false;
		
		while(tmp != null) {
			if(tmp.key.equals(key)) {
				flag = true;
				break;
			}
			tmp = tmp.next;
		}
		
		return flag;
	}
	
	/**
	 * Returns true if hash table contains <code>value</code>, false otherwise.
	 * @param value
	 * @return
	 */
	public boolean containsValue(Object value) {
		boolean flag = false;
		TableEntry<K, V>[] tmp = this.toArray();
		
		for(int i = 0; i < tmp.length; i++) {
			if(tmp[i].value.equals(value)) {
				flag = true;
				break;
			}
		}
		tmp = null;
		
		return flag;
	}
	
	/**
	 * Removes value under specific <code>key</code>, and returns value
	 * that is removed. If key is not in table, null is returned.
	 * @param key
	 * @return
	 */
	public V remove(Object key) {
		if(key == null) return null;
		
		int hash = Math.abs(key.hashCode()) % table.length;
		TableEntry<K, V> tmp = table[hash];
		V value = null;
		
		if(tmp.key.equals(key)) {
			value = tmp.value;
			table[hash] = tmp.next;
			size--;
			modificationCount++;
			tmp = null;
		} else {
			while(tmp.next != null) {
				if(tmp.next.key.equals(key)) {
					value = tmp.next.value;
					tmp.next = tmp.next.next;
					size--;
					modificationCount++;
					break;
				}
				tmp = tmp.next;
			}
		}
		
		return value;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");

		
		for(int i = 0; i < table.length; i++) {
			TableEntry<K, V> tmp = table[i];
		
			while(tmp != null) {
				sb.append(tmp.toString() + ", ");
				tmp = tmp.next;
			}
		}
		
		if(this.size != 0) {
			sb.delete(sb.length() - 2, sb.length());
		}
		
		sb.append("]");
		
		return sb.toString();
	}
	
	/**
	 * Returns all key, value pairs as an array.
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public TableEntry<K, V>[] toArray() {
		TableEntry<K, V>[] tmp =(TableEntry<K,V>[]) new TableEntry[this.size];
		int counter = 0;
		
		for(int i = 0; i < table.length; i++) {
			TableEntry<K, V> tmp2 = table[i];
		
			while(tmp2 != null) {
				tmp[counter++] = tmp2;
				tmp2 = tmp2.next;
			}
		}
		
		return tmp;
	}

	/**
	 * This method clears all key,value pairs from hashtable.
	 */
	public void clear() {
		this.size = 0;
		for(int i = 0; i < table.length; i++) {
			table[i] = null;
		}
	}
	
	/**
	 * Decreases load factor of hash table by increasing table size by 2.
	 */
	@SuppressWarnings("unchecked")
	private void decreaseLoadFactor() {
		TableEntry<K, V>[] tmp = this.toArray();
		this.clear();
		
		
		table = (TableEntry<K, V>[]) new TableEntry[table.length * 2];
		
		for(int i = 0; i < tmp.length; i++) {
			this.putValue(tmp[i].key, tmp[i].value);
		}
		
	}
	
	/**
	 * Private method that represents put method.
	 * @param key
	 * @param value
	 * @return
	 */
	private V putValue(K key, V value) {
		int hash = Math.abs(key.hashCode()) % table.length;
		V value2 = null;
		
		if(table[hash] == null) {
			TableEntry<K, V> tmp = new TableEntry<>(key, value);
			table[hash] = tmp;
			size++;
			modificationCount++;
		} else {
			TableEntry<K, V> tmp2 = table[hash];
			boolean flag = true;
			
			while(tmp2.next != null || tmp2.key.equals(key)) {
				if(tmp2.key.equals(key)) {
					value2 = tmp2.value;
					tmp2.value = value;
					flag = false;
					break;
				}
				tmp2 = tmp2.next;
			}
			
			if(flag) {
				tmp2.next = new TableEntry<>(key, value);
				size++;
				modificationCount++;
			}	
		}
		
		return value2;
	}



	@Override
	public Iterator<SimpleHashTable.TableEntry<K, V>> iterator() {
		return new IteratorImpl(this, (TableEntry<K, V>[]) this.toArray());
	}
	
	private class IteratorImpl implements Iterator<SimpleHashTable.TableEntry<K, V>> {
		private TableEntry<K, V>[] tmp;
		private SimpleHashTable<K, V> reference;		
		private int counter;
		private boolean flag;
		private long savedModificationCount;
		
		public IteratorImpl(SimpleHashTable<K, V> reference, TableEntry<K, V>[] tmp) {
			this.tmp = tmp;
			this.reference = reference;
			this.flag = false;
			this.savedModificationCount = reference.modificationCount;
			this.counter = 0;
		}
		
		@Override
		public boolean hasNext() {
			if(savedModificationCount != reference.modificationCount) {
				throw new ConcurrentModificationException("Internal collection has been structuraly changeed!");
			}
			
			return counter < tmp.length;
		}
		@Override
		public SimpleHashTable.TableEntry<K, V> next() {
			if(savedModificationCount != reference.modificationCount) {
				throw new ConcurrentModificationException("Internal collection has been structuraly changeed!");
			}
			if(counter == tmp.length) throw new NoSuchElementException("Iterator has returned all values already!");
			
			flag = true;
			return tmp[counter++];
		}
		
		@Override
		public void remove() {
			if(!flag) throw new IllegalStateException("Method next hasn't been called yet!");
			
			savedModificationCount++;
			reference.remove(tmp[counter - 1].key);
			flag = false;
		}
	}
	
	
}
