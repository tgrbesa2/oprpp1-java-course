package hr.fer.oprpp1.custom.collections;

import java.util.Iterator;

public class Demo {
	public static void main(String[] args) {
		// create collection:
		SimpleHashTable<String,Integer> examMarks = new SimpleHashTable<>(2);
		// fill data:
		examMarks.put("Ivana", 2);
		examMarks.put("Ante", 2);
		examMarks.put("Jasna", 2);
		examMarks.put("Kristina", 5);
		examMarks.put("Ivana", 5); // overwrites old grade for Ivana
		for(SimpleHashTable.TableEntry<String,Integer> pair : examMarks) {
			System.out.printf("%s => %d%n", pair.getKey(), pair.getValue());
		}
		
		for(SimpleHashTable.TableEntry<String,Integer> pair1 : examMarks) {
			for(SimpleHashTable.TableEntry<String,Integer> pair2 : examMarks) {
			System.out.printf(
					"(%s => %d) - (%s => %d)%n",
					pair1.getKey(), pair1.getValue(),
					pair2.getKey(), pair2.getValue()
			);
			}
		}
		

		
		Iterator<SimpleHashTable.TableEntry<String,Integer>> iter = examMarks.iterator();
		while(iter.hasNext()) {
		SimpleHashTable.TableEntry<String,Integer> pair = iter.next();
		System.out.printf("%s => %d%n", pair.getKey(), pair.getValue());
		iter.remove();
		}
		System.out.printf("Veličina: %d%n", examMarks.size());
		
		System.out.println(examMarks.toString());
	}
}
