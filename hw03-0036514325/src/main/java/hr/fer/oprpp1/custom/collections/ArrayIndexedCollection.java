package hr.fer.oprpp1.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

/**
 * Collection of objects stored in array.
 * @author tgrbesa
 *
 */
public class ArrayIndexedCollection<T> implements List<T> {
	
	/**
	 * Number of elements currently stored in collection.
	 */
	private int size;
	
	/**
	 * Array which contains collection elements.
	 */
	private T[] elements;
	
	/**
	 * Long variable that counts collection modifications.
	 * When structural modification occurs, <code>modificationCount</code>
	 * increments with 1.
	 */
	private long modificationCount = 0;
	
	private static final int DEFAULT_NO_ELEMENTS = 16;
	
	/**
	 * Creates new ArrayIndexedCollection containing objects with array size of 16.
	 */
	public ArrayIndexedCollection() {
		this(DEFAULT_NO_ELEMENTS);
	}
	
	/**
	 * Creates new ArrayIndexedCollection with array capacity of <code>initialCapacity</code>
	 * @param initialCapacity
	 * @throws IllegalArgumentException If argument is less than 1.
	 */
	@SuppressWarnings("unchecked")
	public ArrayIndexedCollection(int initialCapacity) {
		if(initialCapacity < 1) throw new IllegalArgumentException("Initial capacity must be larger or equal 1");
		
		this.size = 0;
		this.elements =(T[]) new Object[initialCapacity];
	}
	
	/**
	 * Copy constructor for some other collection with capacity of 16.
	 * @param other
	 * @throws NullPointerException If argument other is null.
	 */
	public ArrayIndexedCollection(Collection<? extends T> other) {		
		this(other, DEFAULT_NO_ELEMENTS);
	}
	
	/**
	 * Copy constructor for some other collection with capacity <code>initialCapacity</code>
	 * @param other
	 * @param initialCapacity 
	 * @throws IllegalArgumentException If initial capacity is less than 1.
	 * @throws NullPointerException If argument other is null
	 */
	@SuppressWarnings("unchecked")
	public ArrayIndexedCollection(Collection<? extends T> other, int initialCapacity) {
		if(other == null) throw new NullPointerException("Argument other must be non-null reference!");
		if(initialCapacity < 1) throw new IllegalArgumentException("Initial capacity must be larger or equal 1");
	
		if(initialCapacity < other.size()) {
			this.elements =(T[]) new Object[other.size()];
		} else {
			this.elements =(T[]) new Object[initialCapacity];
		}
		
		this.addAll(other);	
	}
	
	@Override
	public int size() {
		return this.size;
	}
	
	/**
	 * Returns length of array used for storage of object elements.
	 * @return
	 */
	public int capacity() {
		return this.elements.length;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public void add(T value) {
		//Could have called method insert with argument index = size!!
		if(value == null) throw new NullPointerException("Argument value must be non-null reference!");
		
		if(this.size == this.elements.length) {
			T[] tmp =(T[]) new Object[this.capacity() * 2];
			modificationCount++;
			
			for(int i = 0; i < this.size; i++) {
				tmp[i] = this.elements[i];
			}
			this.elements = tmp;
		}
		
		this.elements[size++] = value;
	}
	
	/**
	 * Returns the object stored in collection array at position <code>index</code>.
	 * Valid <code>index</code> is between 0 and size-1.
	 * Average complexity is O(1).
	 * @param index Position of object
	 * @return Object at position <code>index</code> in this collection.
	 * @throws IndexOutOfBoundsException If index is invalid
	 */
	public T get(int index) {
		if(index < 0 || index > this.size - 1) throw new IndexOutOfBoundsException("Index must be between 0 and (size - 1)");
		
		return this.elements[index];
	}
	
	@Override
	public void clear() {
		for(int i = 0; i < this.size; i++) {
			this.elements[i] = null;
		}
		this.size = 0;
		modificationCount++;
	}
	
	/**
	 * Inserts <code>value</code> at specific <code>position</code> in collection. 
	 * @param value Value that is inserted into collection.
	 * @param position Position in collection where value is inserted.
	 * @throws IndexOutOfBoundsException If <code>index</code> is invalid.
	 */
	@SuppressWarnings("unchecked")
	public void insert(T value, int position) {
		if(position < 0 || position > this.size) throw new IndexOutOfBoundsException("Index must be between 0 and size(both included)");
		
		if(this.size == this.elements.length) {
			T[] tmp =(T[]) new Object[this.capacity() * 2];
			
			for(int i = 0; i < this.size; i++) {
				tmp[i] = this.elements[i];
			}
			this.elements = tmp;
		}
		
		T tmp = null;
		for(int i = position; i < this.size - 1; i++) {
			 tmp = this.elements[position + 1];
			 this.elements[position + 1] = this.elements[position];
		}
		this.elements[size++] = tmp;
		
		this.elements[position] = value;
		modificationCount++;
	}
	/**
	 * Searches the collection and returns the index of the first occurence of the given value.
	 * Average complexity is O(n/2).
	 * @param value Value which is looked up for in collection.
	 * @return index of argument <code>value</code> or -1 if the value is not found.
	 */
	public int indexOf(Object value) {
		if(value != null) {
			for(int i = 0; i < this.size; i++) {
				if(this.elements[i].equals(value)) return i;
			}
		}
		
		return -1;
	}
	
	/**
	 * Removes element at specified index from collection.
	 * <code>index</code> must be between 0 and (size - 1), otherwise is invalid.
	 * @param index Index of ellement to be removed.
	 * @throws IndexOutOfBoundsException If <code>index</code> is invalid.
	 */
	public void remove(int index) {
		if(index < 0 || index > this.size - 1) throw new IndexOutOfBoundsException("Index must be between 0 and (size - 1)");
		
		for(int i = index; i < this.size - 1; i++) {
			this.elements[i] = this.elements[i + 1];
		}
		
		this.elements[size--] = null;
		modificationCount++;
	}
	
	@Override
	public boolean contains(Object value) {
		return this.indexOf(value) != -1;
	}
	
	@Override
	public Object[] toArray() {
		Object[] tmp = new Object[this.size];
		
		for(int i = 0; i < this.size; i++) {
			tmp[i] = this.elements[i];
		}
		
		return tmp;
	}
	
	

	@Override
	public boolean remove(Object value) {
		int idx = this.indexOf(value);
		if(idx == -1) return false;
		
		this.remove(idx);
		modificationCount++;
		
		return true;
 
	}

	@Override
	public ElementsGetter<T> createElementsGetter() {
		return new ElementsGetterArray<T>(this);
	}
	
	/**
	 * Private static class that implements ElementsGetter(iterator)
	 * specified for ArrayIndexedCollection.
	 * @author tgrbesa
	 *
	 */
	private static class ElementsGetterArray<T> implements ElementsGetter<T> {
		private int idx = 0;
		private long savedModificationCount;
		private ArrayIndexedCollection<T> internalCollection;
		
		public ElementsGetterArray(ArrayIndexedCollection<T> collection) {
			this.internalCollection = collection;
			savedModificationCount = collection.modificationCount;
		}
		
		@Override
		public boolean hasNextElement() {
			if(savedModificationCount != internalCollection.modificationCount) {
				throw new ConcurrentModificationException("Internal collection has been structuraly changed!");
			}
			return idx < internalCollection.size;
		}

		@Override
		public T getNextElement() {
			if(savedModificationCount != internalCollection.modificationCount) {
				throw new ConcurrentModificationException("Internal collection has been structuraly changed!");
			}
			
			if(idx == internalCollection.size) throw new NoSuchElementException();
			
			
			return internalCollection.elements[idx++];
		}
		
	}

}
