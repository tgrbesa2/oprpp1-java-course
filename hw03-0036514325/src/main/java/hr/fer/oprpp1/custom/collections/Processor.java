package hr.fer.oprpp1.custom.collections;

/**
 * 
 * 
 * @author tgrbesa
 *
 */
public interface Processor<T>{
	/**
	 * Method that processes some object
	 * 
	 * @param value Object to be processed.
	 */
	public void process(T value);
	
}
