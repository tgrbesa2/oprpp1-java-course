package hr.fer.oprpp1.math;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class Vector2DTester {
	
	@Test
	public void testConstructorAndGetters() {
		Vector2D vector = new Vector2D(1, 1);
		
		assertEquals(vector.getX(), 1);
		assertEquals(vector.getY(), 1);
	}
	
	@Test
	public void testAdd() {
		Vector2D vector = new Vector2D(1, 1);
		
		vector.add(new Vector2D(2, 3));
		
		assertEquals(vector.getX(), 3);
		assertEquals(vector.getY(), 4);
	}
	
	@Test
	public void testAdded() {
		Vector2D vector = new Vector2D(1, 1);
		
		Vector2D vector2 = vector.added(new Vector2D(2, 3));
		
		assertEquals(vector2.getX(), 3);
		assertEquals(vector2.getY(), 4);
	}
	
	@Test
	public void testRotate() {
		Vector2D vector = new Vector2D(1, -1);
		
		vector.rotate(Math.PI / 2);
	
		assertTrue((vector.getX() - 1) < 10e-5);
		assertTrue((vector.getY() - 1) < 10e-5);
	}
	
	@Test
	public void testRotated() {
		Vector2D vector = new Vector2D(1, 1);
		
		Vector2D vector2 = vector.rotated(Math.PI);
		
		assertTrue((vector2.getX() + 1) < 10e-5);
		assertTrue((vector2.getY() + 1) < 10e-5);
	}
	
	@Test
	public void testScaler() {
		Vector2D vector = new Vector2D(2, 2);
		
		vector.scale(4);
		
		assertEquals(vector.getX(), 8);
		assertEquals(vector.getY(), 8);
		
	}
	
	@Test
	public void testScaled() {
		Vector2D vector = new Vector2D(2, 2);
		
		Vector2D vector2 = vector.scaled(4);
		
		assertEquals(vector2.getX(), 8);
		assertEquals(vector2.getY(), 8);
		
	}
}
