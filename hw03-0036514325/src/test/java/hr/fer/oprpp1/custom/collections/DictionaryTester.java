package hr.fer.oprpp1.custom.collections;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class DictionaryTester {
	
	@Test
	public void testKeyNull() {
		Dictionary<Integer, String> data = new Dictionary<>();
	
		assertThrows(NullPointerException.class, () -> data.put(null, "Hehe"));
	}
	
	@Test
	public void testValueNull() {
		Dictionary<Integer, String> data = new Dictionary<>();
		
		data.put(1, null);
		data.put(2, "Hehe");
		
		assertEquals(null, data.get(1));
	}
	
	@Test
	public void testSize() {
		Dictionary<Integer, String> data = new Dictionary<>();
		
		data.put(1, null);
		data.put(2, "Hehe");
		
		assertEquals(2, data.size());
	}
	
	@Test
	public void testIsEmpty() {
		Dictionary<Integer, String> data = new Dictionary<>();
		
		data.put(1, null);
		data.put(2, "Hehe");
		
		assertEquals(false, data.isEmpty());
	}
	
	@Test
	public void testClear() {
		Dictionary<Integer, String> data = new Dictionary<>();
		
		data.put(1, null);
		data.put(2, "Hehe");
		
		assertEquals(2, data.size());
		
		data.clear();
		
		assertEquals(0, data.size());
	}
	
	@Test
	public void testAddingTwoSameKeys() {
		Dictionary<Integer, String> data = new Dictionary<>();
		
		data.put(1, null);
		data.put(2, "Hehe");
		
		assertEquals(data.get(2), "Hehe");
		
		data.put(2, "Laku noc");
		
		assertEquals(data.get(2), "Laku noc");
	}
	
	@Test
	public void testgetNonExistingKey() {
		Dictionary<Integer, String> data = new Dictionary<>();
		
		data.put(1, null);
		data.put(2, "Hehe");
		
		assertEquals(null, data.get(50));
	}
	
	@Test
	public void testRemove() {
		Dictionary<Integer, String> data = new Dictionary<>();
		
		data.put(1, null);
		data.put(2, "Hehe");
		
		data.remove(2);
		
		assertEquals(data.get(2), null);
		
	}
	
	@Test
	public void testRemoveNonExistingValue() {
		Dictionary<Integer, String> data = new Dictionary<>();
		
		data.put(1, null);
		data.put(2, "Hehe");
		
		assertEquals(data.remove(3), null);
	}
	
	
	
	
	
	
	
}
