package hr.fer.oprpp1.custom.collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ArrayIndexedCollectionTest {
	
	@Test
	public void testDefaultConstructor() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection();
		Assertions.assertEquals(16, collection.capacity());
	}
	
	@Test
	public void testConstructorWithInitialCapacity() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection(20);
		Assertions.assertEquals(20, collection.capacity());
	}
	
	@Test
	public void testConstructorInitialCapacityLessThanShouldThrow() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection(-50));
	}
	
	@Test
	public void testCopyConstructor() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection();
		for(int i = 0; i < 10; i++) {
			collection.add(i);
		}
		
		ArrayIndexedCollection collection2 = new ArrayIndexedCollection(collection);
		Assertions.assertEquals(16, collection2.capacity());
		Assertions.assertArrayEquals(collection.toArray(), collection2.toArray());

	}
	
	@Test
	public void testCopyConstructorNullReferenceShouldThrow() {
		Assertions.assertThrows(NullPointerException.class, () -> new ArrayIndexedCollection(null));
		
	}
	
	@Test
	public void testCopyConstructorWithInitialCapacity() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection(10);
		for(int i = 0; i < 10; i++) {
			collection.add(i);
		}
		
		ArrayIndexedCollection collection2 = new ArrayIndexedCollection(collection, 20);
		ArrayIndexedCollection collection3 = new ArrayIndexedCollection(collection, 5);
		Assertions.assertEquals(10, collection3.capacity());
		Assertions.assertArrayEquals(collection.toArray(), collection2.toArray());
		Assertions.assertEquals(20, collection2.capacity());
		

	}
	
	@Test
	public void testCopyConstructorWithInitialCapacityLessThanShouldThrow() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection();
		Assertions.assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection(collection, 0));
	}
	
	@Test
	public void testAdd() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection();
		collection.add(18);collection.add("Tomo");
		
		Object[] tmp = {18, "Tomo"};
		
		Assertions.assertArrayEquals(tmp, collection.toArray());
	}
	
	@Test
	public void testAddNullShouldThrow() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection();
		Assertions.assertThrows(NullPointerException.class, () -> collection.add(null));
	}
	
	@Test
	public void testGet() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection();
		collection.add(18);collection.add("Tomo");
		
		Assertions.assertEquals(18, collection.get(0));
	}
	
	@Test
	public void testGetInvalidIndexShouldThrow() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection();
		collection.add(18);collection.add("Tomo");
		
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.get(2));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.get(-5));
	}
	
	@Test
	public void testClear() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection(8);
		collection.add(18);collection.add("Tomo");
		
		collection.clear();
		
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.get(0));
		Assertions.assertEquals(8, collection.capacity());
	}
	
	@Test
	public void testInsert() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection();
		collection.add(18);collection.add("Tomo"); collection.add(68);
		
		collection.insert(2, 1);
		
		Assertions.assertEquals(2, collection.get(1));
		Assertions.assertEquals(68, collection.get(3));
	}
	
	@Test
	public void testInsertInvalidArgumentShouldThrow() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection();
		collection.add(18);collection.add("Tomo");
		
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.insert("Ne ide", 10));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.insert("Ne ide", -7));

	}
	
	@Test
	public void testIndexOf() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection();
		collection.add(18);collection.add("Tomo");
		
		Assertions.assertEquals(1, collection.indexOf("Tomo"));
		Assertions.assertEquals(-1, collection.indexOf("Nije ovde"));
	}
	
	@Test
	public void testRemove() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection();
		collection.add(18);collection.add("Tomo");collection.add(2);
		
		collection.remove(0);
		
		Assertions.assertEquals("Tomo", collection.get(0));
		Assertions.assertEquals(2, collection.get(1));
	}
	
	@Test void testRemoveInvalidArgumentShouldThrow() {
		ArrayIndexedCollection collection = new ArrayIndexedCollection();
		collection.add(18);collection.add("Tomo");collection.add(2);
		
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.remove(3));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.remove(-3));

	}
	
	

}
