package hr.fer.oprpp1.custom.collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LinkedListIndexedCollectionTest {
	
	@Test
	public void testDefaultConstructor() {
		LinkedListIndexedCollection collection = new LinkedListIndexedCollection();
		Assertions.assertEquals(0, collection.size());
	}
	
	@Test
	public void testCopyConstructor() {
		LinkedListIndexedCollection collection = new LinkedListIndexedCollection();
		collection.add(2); collection.add("Tomo");
		
		LinkedListIndexedCollection collection2 = new LinkedListIndexedCollection(collection);
		Assertions.assertArrayEquals(collection.toArray(), collection2.toArray());
	}
	
	@Test
	public void testAdd() {
		LinkedListIndexedCollection collection = new LinkedListIndexedCollection();
		collection.add(2); collection.add("Tomo"); collection.add(1);
		
		Assertions.assertEquals(1, collection.get(2));
		Assertions.assertEquals(2, collection.get(0));
		
	}
	
	@Test
	public void testAddNullShouldThrow() {
		LinkedListIndexedCollection collection = new LinkedListIndexedCollection();
		Assertions.assertThrows(NullPointerException.class, () -> collection.add(null));
	}
	
	@Test
	public void testGet() {
		LinkedListIndexedCollection collection = new LinkedListIndexedCollection();
		collection.add(2); collection.add("Tomo"); collection.add(1);
		
		Assertions.assertEquals(1, collection.get(2));
		Assertions.assertEquals(2, collection.get(0));
	}
	
	@Test
	public void testGetInvalidArgumentShouldThrow() {
		LinkedListIndexedCollection collection = new LinkedListIndexedCollection();
		collection.add(2); collection.add("Tomo"); collection.add(1);
		
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.get(5));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.get(-5));

	}
	
	@Test
	public void testClear() {
		LinkedListIndexedCollection collection = new LinkedListIndexedCollection();
		collection.add(2); collection.add("Tomo"); collection.add(1);
		
		collection.clear();
		
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.get(0));
		Assertions.assertEquals(0, collection.size());
	}
	
	@Test
	public void testInsert() {
		LinkedListIndexedCollection collection = new LinkedListIndexedCollection();
		collection.add(2); collection.add("Tomo"); collection.add(1);
		
		collection.insert(5, 1);
		
		Assertions.assertEquals(5, collection.get(1));
		Assertions.assertEquals("Tomo", collection.get(2));
		Assertions.assertEquals(1, collection.get(3));

	}
	
	@Test
	public void testInsertIllegalArgumentShouldThrow() {
		LinkedListIndexedCollection collection = new LinkedListIndexedCollection();
		collection.add(2); collection.add("Tomo"); collection.add(1);
		
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.insert(5, 4));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.insert(5, -5));
	}
	
	@Test
	public void testIndexOf() {
		LinkedListIndexedCollection collection = new LinkedListIndexedCollection();
		collection.add(2); collection.add("Tomo"); collection.add(1);
		
		Assertions.assertEquals(1, collection.indexOf("Tomo"));
		Assertions.assertEquals(-1, collection.indexOf("JEJEJEJJE"));
		Assertions.assertEquals(-1, collection.indexOf(null));

	}
	
	@Test
	public void testRemove() {
		LinkedListIndexedCollection collection = new LinkedListIndexedCollection();
		collection.add(2); collection.add("Tomo"); collection.add(1);
		
		collection.remove(0);
		
		Assertions.assertEquals(0, collection.indexOf("Tomo"));
		Assertions.assertEquals(1, collection.indexOf(1));

	}
	
	@Test
	public void testRemoveIllegalArgumentShouldThrow() {
		LinkedListIndexedCollection collection = new LinkedListIndexedCollection();
		collection.add(2); collection.add("Tomo"); collection.add(1);
		
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.remove(3));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> collection.remove(-5));

	}
}
