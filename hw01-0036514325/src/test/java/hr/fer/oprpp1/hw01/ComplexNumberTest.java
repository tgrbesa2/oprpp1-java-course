package hr.fer.oprpp1.hw01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ComplexNumberTest {
	
	@Test
	public void testConstructorAndGetters() {
		ComplexNumber c = new ComplexNumber(1, 1);
		Assertions.assertEquals(1, c.getReal());
		Assertions.assertEquals(1, c.getImaginary());
		Assertions.assertEquals(Math.PI / 4, c.getAngle());
		Assertions.assertEquals(Math.sqrt(2), c.getMagnitude());
		
		ComplexNumber c2 = new ComplexNumber(0, -1);
		Assertions.assertEquals(Math.PI * 3 / 2, c2.getAngle());
	}
	
	@Test
	public void testFromMethods() {
		ComplexNumber c1 = ComplexNumber.fromReal(2.0);
		ComplexNumber c2 = ComplexNumber.fromImaginary(3.0);
		ComplexNumber c3 = ComplexNumber.fromMagnitudeAndAngle(2, Math.PI);
		
		Assertions.assertEquals(0.0, c1.getImaginary());
		Assertions.assertEquals(2.0, c1.getReal());
		Assertions.assertEquals(0.0, c2.getReal());
		Assertions.assertEquals(3.0, c2.getImaginary());
		Assertions.assertEquals(-2.0, c3.getReal());
		Assertions.assertTrue(0.0 - c3.getImaginary() < 1e-5);
	}
	
	@Test
	public void testParseAndToString() {
		String[] testStrings = {"i", "-i", "+2+3i", "-2.5-i"};
		String[] correctStrings = {"0.0+1.0i", "0.0-1.0i", "2.0+3.0i", "-2.5-1.0i"};
		ComplexNumber[] complexArray = new ComplexNumber[4];
		String[] parsedStrings = new String[4];
		
		for(int i = 0; i < testStrings.length; i++) {
			complexArray[i] = ComplexNumber.parse(testStrings[i]);
		}
		for(int i = 0; i < parsedStrings.length; i++) {
			parsedStrings[i] = complexArray[i].toString();
		}
		
		Assertions.assertArrayEquals(correctStrings, parsedStrings);
	}
	
	@Test
	public void testAdd() {
		ComplexNumber c1 = new ComplexNumber(1, 1);
		ComplexNumber c2 = new ComplexNumber(-5, 2);
		ComplexNumber c3 = c1.add(c2);
		
		Assertions.assertEquals("-4.0+3.0i", c3.toString());
	}
	
	@Test
	public void testSub() {
		ComplexNumber c1 = new ComplexNumber(1, 1);
		ComplexNumber c2 = new ComplexNumber(-5, 2);
		ComplexNumber c3 = c1.sub(c2);
		
		Assertions.assertEquals("6.0-1.0i", c3.toString());
	}
	
	@Test
	public void testMul() {
		ComplexNumber c1 = new ComplexNumber(1, 1);
		ComplexNumber c2 = new ComplexNumber(-5, 2);
		ComplexNumber c3 = c1.mul(c2);
		
		Assertions.assertEquals("-7.0-3.0i", c3.toString());
	}
	
	@Test
	public void testDiv() {
		ComplexNumber c1 = new ComplexNumber(5, 5);
		ComplexNumber c2 = new ComplexNumber(3, 4);
		ComplexNumber c3 = c1.div(c2);
		
		Assertions.assertEquals("7.0-1.0i", c3.toString());
	}
	
	@Test
	public void testPower() {
		ComplexNumber c1 = new ComplexNumber(3, 4);
		ComplexNumber c2 = c1.power(2);
		
		//Assertions.assertEquals("-7.0+24.0i", c2.toString());
		Assertions.assertThrows(IllegalArgumentException.class, () -> c1.power(-5));
	}		
	
	@Test
	public void testRoot() {
		ComplexNumber c1 = ComplexNumber.fromMagnitudeAndAngle(25.0, Math.PI / 4);
		ComplexNumber[] c2 = c1.root(2);
		
		Assertions.assertEquals(5, c2[0].getMagnitude());
		Assertions.assertTrue(Math.PI / 8 - c2[0].getAngle() < 1e-5);
		Assertions.assertTrue(Math.PI * 9 / 8 - c2[1].getAngle() < 1e-5);
		
		Assertions.assertThrows(IllegalArgumentException.class, () -> c1.root(0));
	}
	
	
}
