package hr.fer.oprpp1.custom.collections;

/**
 * Class represents general collection of objects
 * @author tgrbesa
 *
 */
public class Collection {
	
	/**
	 * Checks if collection is empty.
	 * @return True if collection contains no object, false otherwise.
	 */
	public boolean isEmpty() {
		return this.size() == 0;
	}
	
	/**
	 * Returns the number of currently stored objects in this collection.
	 * @return Size of the collection.
	 */
	public int size() {
		return 0;
	}
	
	/**
	 *  Adds the given object into this colelction.
	 * @param value Object to be added to collection.
	 */
	public void add(Object value) {
	}
	
	 /**
	  * Checks if collection contains given <code>value</code>.
	  * @param value
	  * @return True if collection contains <code>value</code>, otherwise false.
	  */
	public boolean contains(Object value) {
		return false;
	}
	
	/**
	 * Removes one occurence of given <code>value</code>, if the collection contains given value. 
	 * @param value Object to be removed.
	 * @return True if object is contained in collection and removed, false otherwise.
	 */
	public boolean remove(Object value) {
		return false;
	}
	
	/**
	 * Allocates new array with equal size of this collection, fills it with collection content and returns the array.
	 * @return Copy of this collection as array.
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Method calls <code>Processor.process()</code> for each element of this collection.
	 * @param processor Instance of Processor class.
	 */
	public void forEach(Processor processor) {
	}
	
	/**
	 * 
	 * @param other
	 */
	public void addAll(Collection other) {
		/**
		 * This class extends Processor class and is used for adding values in this collection.
		 * @author tgrbesa
		 *
		 */
		class addAllProcessor extends Processor {
			
			@Override
			public void process(Object value) {
				add(value);
			}
		}
		other.forEach(new addAllProcessor());
	}
	
	/**
	 * Removes all elements from this collection.
	 */
	public void clear() {
	}
} 
