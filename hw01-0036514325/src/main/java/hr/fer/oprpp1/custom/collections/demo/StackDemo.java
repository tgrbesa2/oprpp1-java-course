package hr.fer.oprpp1.custom.collections.demo;


import hr.fer.oprpp1.custom.collections.ObjectStack;


/**
 * Command-line application which accepts single command-line argument.
 * @author tgrbesa
 *
 */
public class StackDemo {
	public static void main(String[] args) {
		ObjectStack stack = new ObjectStack();
		String[] evaluation = args[0].split(" ");
		
		int tmp = 0;
		int firstArg = 0;
		int secondArg = 0;
		for(String s : evaluation) {
			try {
				tmp = Integer.parseInt(s);
				stack.push(tmp);
				
			} catch(NumberFormatException ex1) {
				secondArg = (int) stack.pop();
				firstArg = (int) stack.pop();
				switch(s) {
					case "+":
						stack.push(firstArg + secondArg);
						break;
					case "-":
						stack.push(firstArg - secondArg);
						break;
					case "/":
						stack.push(firstArg / secondArg);
						break;
					case "*":
						stack.push(firstArg * secondArg);
						break;
					case "%":
						stack.push(firstArg % secondArg);
						break;
					default:
						throw new IllegalArgumentException("Wrong argument" + s);
					
				}
				
			} catch(Exception ex2) {
				System.out.println("Error");
				System.exit(-1);
			}
		}
		
		if(stack.size() != 1) {
			System.out.println("Error");
			System.exit(-1);
		}
		
		System.out.println(stack.pop());
	}
}
