package hr.fer.oprpp1.custom.collections;

public class ObjectStack {
	
	/**
	 * ArrayIndexedCollection where the data will be stored.
	 */
	private ArrayIndexedCollection data;
	
	/**
	 * Constructor creates new instance of Array collection.
	 */
	public ObjectStack() {
		data = new ArrayIndexedCollection();
	}
	/**
	 * Checks if stack is empty.
	 * @return True if stack contains no object, false otherwise.
	 */
	public boolean isEmpty() {
		return data.isEmpty();
	}
	
	/**
	 * Returns the number of currently stored objects in this stack.
	 * @return Size of the stack.
	 */
	public int size() {
		return data.size();
	}
	
	/**
	 * Pushes given <code>value</code> on the stack.
	 * Null value is not allowed.
	 * @param value Value to be pushed on stack.
	 * @throws NullPointerException If value is null reference.
	 */
	public void push(Object value) {
		data.add(value);
	}
	
	/**
	 * Removes last value pushed on stack and returns it.
	 * @return Object on top of the stack.
	 * @throws EmptyStackException If stack is empty.
	 */
	public Object pop() {
		if(this.size() == 0) throw new EmptyStackException("Stack is empty.");
		
		Object tmp = data.get(this.size() - 1);
		data.remove(this.size() - 1);
		return tmp;
	}
	
	/**
	 * Returns the last element placed on stack, but does not delete it from stack.
	 * @return Object at the top of stack.
	 * @throws EmptyStackException If stack is empty.
	 */
	public Object peek() {
		if(this.size() == 0) throw new EmptyStackException("Stack is empty");
		
		return data.get(this.size() - 1);
	}
	
	/**
	 * Removes all elements from this stack.
	 */
	public void clear() {
		data.clear();
	}
	
	
}
