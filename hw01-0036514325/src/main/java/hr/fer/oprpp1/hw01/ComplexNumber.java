package hr.fer.oprpp1.hw01;

import static java.lang.Math.hypot;
import static java.lang.Math.acos;
import static java.lang.Math.*;

/**
 * Class represents an unmodifiable complex number.
 * @author tgrbesa
 *
 */
public class ComplexNumber {
	
	/**
	 * Private double variables and they represent complex number.
	 */
	private double real;
	private double imaginary;
	
	/**
	 * Constructor that accpets two arguments.
	 * @param realPart real part of complex number.
	 * @param imaginaryPart imaginary part of complex number.
	 */
	public ComplexNumber(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}
	
	/**
	 * Returns imaginary part of complex number.
	 * @return
	 */
	public double getImaginary() {
		return this.imaginary;
	}
	
	/**
	 * Returns real part of complex number.
	 * @return
	 */
	public double getReal() {
		return this.real;
	}
	
	/**
	 * Returns magnitude of complex number.
	 * @return
	 */
	public double getMagnitude() {
		return hypot(real, imaginary);
	}
	
	/**
	 * Returns angle of complex number in range from 0 to 2Pi.
	 * @return
	 */
	public double getAngle() {
		if((this.real < 0 && this.imaginary < 0) || (this.real >= 0 && this.imaginary < 0)) {
			return 2 * Math.PI + Math.atan2(this.imaginary, this.real);
		} else {
			return Math.atan2(this.imaginary, this.real);
		}
	}
	
	/**
	 * Creates and returns complex number only with real part.
	 * @param real
	 * @return
	 */
	public static ComplexNumber fromReal(double real) {
		return new ComplexNumber(real, 0.0);
	}
	
	/**
	 * Creates and returns complex number only with imaginary part.
	 * @param imaginary
	 * @return
	 */
	public static ComplexNumber fromImaginary(double imaginary) {
		return new ComplexNumber(0.0, imaginary);
	}
	
	/**
	 * Creates and returns complex number from magnitude and angle;
	 * @param magnitude
	 * @param angle
	 * @return
	 */
	public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
		return new ComplexNumber(cos(angle) * magnitude, sin(angle) * magnitude);
	}
	
	/**
	 * Creates and returns complex number from parsed string.
	 * Examples that can be parssed(-3.5, -i, -4i, i, -2+3i, 2-3i) etc.
	 * @param s String for parsing.
	 * @return ComplexNumber parsed from input string.
	 * @throws IllegalArgumentException If imaginary unit is not written after magnitude.
	 * @throws IllegalArgumentException If multiple signs are found in string.
	 */
	public static ComplexNumber parse(String s) {
		double realPart = 0.0;
		double imaginaryPart = 0.0;
		
		if(s.lastIndexOf("i") != s.length() - 1 && s.lastIndexOf("i") != - 1) {
			throw new IllegalArgumentException("Imaginary unit must be written after magnitude of imaginary part");
		}
		
		if(s.contains("++") || s.contains("+-") || s.contains("-+") || s.contains("--")) {
			throw new IllegalArgumentException("Multiple signs are not supported");
		}
		
		
		if(s.substring(0, 1).equals("+")) s = s.substring(1, s.length());
		
		if(s.contains("+")) {
			int idx = s.indexOf("+");
			
			if(s.substring(idx + 1, s.length()).equals("i")) {
				imaginaryPart = 1.0;
			} else {
				imaginaryPart = Double.parseDouble(s.substring(idx, s.length() - 1));
			}
			
			realPart = Double.parseDouble(s.substring(0, idx));
			
		} else if(s.lastIndexOf("-") > 0) {
			int idx = s.lastIndexOf("-");
			
			if(s.substring(idx + 1, s.length()).equals("i"))  {
				imaginaryPart = -1.0;
			} else {
				imaginaryPart = Double.parseDouble(s.substring(idx, s.length() - 1));
			}
			
			realPart = Double.parseDouble(s.substring(0, idx));
			
		} else {
			if(s.contains("i")) {
				switch(s) {
					case "i":
						imaginaryPart = 1.0;
						break;
					case "-i":
						imaginaryPart = -1.0;
						break;
					default:
						imaginaryPart = Double.parseDouble(s.substring(0, s.length() - 1));
				}
			} else {
				realPart = Double.parseDouble(s);
			}
		}
		return new ComplexNumber(realPart, imaginaryPart);
	}
	
	/**
	 * Method calculates sumation of two complex numbers.
	 * @param c
	 * @return new complex number as a result of sumation.
	 */
	public ComplexNumber add(ComplexNumber c) {
		return new ComplexNumber(this.real + c.real, this.imaginary + c.imaginary);
	}
	
	/**
	 * Method calculates subtraction of two complex numbers.
	 * @param c
	 * @return new complex number as a result of subtraction.
	 */
	public ComplexNumber sub(ComplexNumber c) {
		return new ComplexNumber(this.real - c.real, this.imaginary - c.imaginary);
	}
	
	/**
	 * Method calculates multiplication of two complex numbers.
	 * @param c
	 * @return new complex number as a result of multiplication.
	 */
	public ComplexNumber mul(ComplexNumber c) {
		return new ComplexNumber(this.real * c.real - this.imaginary * c.imaginary, this.real * c.imaginary + this.imaginary * c.real);
	}
	
	/**
	 * Method calculates division of two complex numbers.
	 * @param c
	 * @return new complex number as a result of division.
	 */
	public ComplexNumber div(ComplexNumber c) {
		ComplexNumber conjugate = new ComplexNumber(c.real, -1 * c.imaginary);
		ComplexNumber pom = this.mul(conjugate);

		return new ComplexNumber(pom.real / c.getMagnitude(), pom.imaginary / c.getMagnitude());
	}
	
	/**
	 * Method calculates power of <code>n</code> on complex number.
	 * @param n
	 * @return new complex number as a result of operation power.
	 * @throws IllegalArgumentException If n is less than 0.
	 */
	public ComplexNumber power(int n) {
		if(n < 0) throw new IllegalArgumentException("n must be greater than or equal to 0!");
		
		return fromMagnitudeAndAngle(Math.pow(this.getMagnitude(), n), this.getAngle() * n);
	}
	
	/**
	 * Method calculates root of <code>n</code> on complex number.
	 * @param n
	 * @return new complex number as a result of operation root.
	 * @throws IllegalArgumentException If n is less than or equal 0.
	 */
	public ComplexNumber[] root(int n) {
		if(n <= 0) throw new IllegalArgumentException("n must be greater than 0!");
		
		ComplexNumber[] roots = new ComplexNumber[n];
		
		for(int i = 0; i < n; i++) {
			roots[i] = fromMagnitudeAndAngle(Math.pow(this.getMagnitude(), 1.0D / n), (this.getAngle() + 2 * i * Math.PI) / n);
		}
		return roots;
	}
	
	@Override
	public String toString() {
		String tmp = "";
		tmp += real;
		if(imaginary >= 0) {
			tmp += "+";
		}
		tmp += imaginary;
		tmp += "i";
		return tmp;

	}

}