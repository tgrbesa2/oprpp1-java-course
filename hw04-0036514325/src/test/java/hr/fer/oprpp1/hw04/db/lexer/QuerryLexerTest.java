package hr.fer.oprpp1.hw04.db.lexer;


import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class QuerryLexerTest {
	@Test
	public void testNormalQuery() {
		String a = "querry jmbag=\"0036514325\"";
		
		QuerryLexer ql = new QuerryLexer(a);
		
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.OPERATOR);
		assertEquals(ql.nextToken().getType(), TokenType.STRING);
		assertEquals(ql.nextToken().getType(), TokenType.EOF);


	}
	
	@Test
	public void testMultipleQuerry() {
		String a = "querry jmbag=\"*036514325\" AND ime<\"majmun\"";
		
		QuerryLexer ql = new QuerryLexer(a);
		
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.OPERATOR);
		assertEquals(ql.nextToken().getType(), TokenType.STRING);
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.OPERATOR);
		assertEquals(ql.nextToken().getType(), TokenType.STRING);

		assertEquals(ql.nextToken().getType(), TokenType.EOF);
	}
	
	@Test
	public void testLongQuerry() {
		String a ="query firstName!=\"A\" and firstName<\"C\" and lastName LIKE \"B*ć\" and jmbag>\"0000000002\"";
		
		QuerryLexer ql = new QuerryLexer(a);
		
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.OPERATOR); 
		System.out.println(ql.getToken().getValue().toLowerCase());
		assertEquals(ql.nextToken().getType(), TokenType.OPERATOR);
		assertEquals(ql.nextToken().getType(), TokenType.STRING);
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.OPERATOR);
		assertEquals(ql.nextToken().getType(), TokenType.STRING);
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.STRING);
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.KEYWORD);
		assertEquals(ql.nextToken().getType(), TokenType.OPERATOR);
		assertEquals(ql.nextToken().getType(), TokenType.STRING);



		assertEquals(ql.nextToken().getType(), TokenType.EOF);
		
	}
}
