package hr.fer.oprpp1.hw04.db;


import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
public class QueryFilterTest {
	String a = "0036514325   Grbeša  Tomislav  2";
	String b = "0011111111   Vuica Alka 4";
	String c = "0000000001   Zarka Uica 5";
	
	String[] arrays = {a, b, c};
	
	@Test
	public void testQueryFilter() {
		StudentDatabase database = new StudentDatabase(arrays);
		
		QueryParser parser = new QueryParser(" jmbag = \"0036514325\"");
		QueryFilter filter = new QueryFilter(parser.getQuery());
		
		assertEquals(2, database.filter(filter).get(0).getFinalGrade());
		
		parser = new QueryParser(" lastName > \"M\"");
		filter = new QueryFilter(parser.getQuery());
		
		assertEquals(4, database.filter(filter).get(0).getFinalGrade());
		assertEquals(5, database.filter(filter).get(1).getFinalGrade());
	}
}
