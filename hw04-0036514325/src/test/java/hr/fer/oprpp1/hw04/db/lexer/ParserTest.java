package hr.fer.oprpp1.hw04.db.lexer;


import org.junit.jupiter.api.*;

import hr.fer.oprpp1.hw04.db.QueryParser;

import static org.junit.jupiter.api.Assertions.*;

public class ParserTest {

	@Test
	public void directQueryTest() {
		String a = "jmbag = \"123091\"";
		
		QueryParser parser = new QueryParser(a);
		
		assertEquals(1, parser.getQuery().size());
	}
	
	
	@Test
	public void twoQueryTest() {
		String a = "jmbag = \"123091\" AND lastname LIKE \"hehe\" AND firstname<\"beee\"";
		
		QueryParser parser = new QueryParser(a);
		
		assertEquals(3, parser.getQuery().size());
		assertEquals(false, parser.isDirectQuery());
	}
}
