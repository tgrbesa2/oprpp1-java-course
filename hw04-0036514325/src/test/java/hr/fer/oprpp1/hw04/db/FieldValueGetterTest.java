package hr.fer.oprpp1.hw04.db;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class FieldValueGetterTest {
	
	@Test
	public void testFieldValueGetter() {
		IFieldValueGetter firstname = FieldValueGetter.FIRST_NAME;
		IFieldValueGetter lastnam = FieldValueGetter.LAST_NAME;
		IFieldValueGetter jmbag = FieldValueGetter.JMBAG;

		StudentRecord record = new StudentRecord("0036514325", "Grbeša", "Tomislav", 5);
		
		assertEquals("0036514325", jmbag.get(record));
		assertEquals("Grbeša", lastnam.get(record));
		assertEquals("Tomislav", firstname.get(record));

	}
}
