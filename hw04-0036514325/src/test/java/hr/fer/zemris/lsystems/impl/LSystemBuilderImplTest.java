package hr.fer.zemris.lsystems.impl;

import org.junit.jupiter.api.*;

import hr.fer.zemris.lsystems.LSystem;

public class LSystemBuilderImplTest {
	
	@Test
	public void LSystemBuilderImplGenerateTest() {
		LSystemBuilderImpl lsbi = new LSystemBuilderImpl();
		
		LSystem ls = lsbi.registerCommand('F', "draw 1")
		.registerCommand('+', "rotate 60")
		.registerCommand('-', "rotate -60")
		.setOrigin(0.05, 0.4)
		.setAngle(0)
		.setUnitLength(0.9)
		.setUnitLengthDegreeScaler(1.0/3.0)
		.registerProduction('F', "F+F--F+F")
		.setAxiom("F")
		.build();
		
		Assertions.assertEquals("F", ls.generate(0));
		Assertions.assertEquals("F+F--F+F", ls.generate(1));
		Assertions.assertEquals("F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F", ls.generate(2));
		
		
	}
	
	
	
	
}
