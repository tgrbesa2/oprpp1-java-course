package demo;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.*;


import hr.fer.oprpp1.hw04.db.StudentDatabase;
import hr.fer.oprpp1.hw04.db.StudentRecord;
public class StudentDatabaseTest {
	@Test
	public void TestAcceptAllFilter() throws IOException {
		String docBody = new String(
				Files.readAllBytes(Paths.get("src/test/resources/database.txt")),
				StandardCharsets.UTF_8);
		String[] split = docBody.split("\n");
		

		StudentDatabase database = new StudentDatabase(split);
		
		List<StudentRecord> fullList = database.filter(r -> true);
		List<StudentRecord> emptyList = database.filter(r -> false);
		
		assertEquals(fullList, database.getListOfStudents());
		assertEquals(emptyList.size(), 0);
	}
}
