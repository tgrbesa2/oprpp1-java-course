package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.oprpp1.math.Vector2D;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.*;

/**
 * Draws line on GUI.
 * @author tgrbesa
 *
 */
public class DrawCommand implements Command {
	/**
	 * Scaler of length.
	 */
	private double step;
	
	public DrawCommand(double step) {
		this.step = step;
	}
	
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState currentState = ctx.getCurrentState();
		Vector2D currentPos = currentState.getCurrentPosition();
		Vector2D currentDir = currentState.getCurrentDirection();
		
		Vector2D nextPos = currentPos.added(currentDir.scaled(currentState.getCurrentMovingLength() * step));
		
		
		painter.drawLine(currentPos.getX(),
						currentPos.getY(),
						nextPos.getX(),
						nextPos.getY(),
						ctx.getCurrentState().getColor(),
						1f);
		currentState.setCurrentPosition(nextPos);
	}
}
