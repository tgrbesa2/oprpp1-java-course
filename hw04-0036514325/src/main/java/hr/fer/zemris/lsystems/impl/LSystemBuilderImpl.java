package hr.fer.zemris.lsystems.impl;

import java.awt.Color;

import hr.fer.oprpp1.custom.collections.Dictionary;
import hr.fer.oprpp1.math.Vector2D;
import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.commands.*;

/**
 * Implementation class of LSystemBuilder.
 * @return
 */
public class LSystemBuilderImpl implements  LSystemBuilder {
	
	/**
	 * Dictionary that stores productions.
	 */
	private Dictionary<Character, String> productions;
	
	/**
	 * Dictionary that stores commands
	 */
	private Dictionary<Character, Command> commands;
	
	
	private double angle = 0;
	
	private String axiom = "";
	
	private double unitLengthDegreeScaler = 1;
	
	private Vector2D origin = new Vector2D(0, 0);
	
	private double unitLength = 0.1;
	
	/**
	 * Basic constructor
	 */
	public LSystemBuilderImpl() {
		this.productions = new Dictionary<Character, String>();
		this.commands = new Dictionary<Character, Command>();
	}
	
	@Override
	public LSystem build() {
		LSystem lsystem = new LSystem() {

			@Override
			public void draw(int arg0, Painter arg1) {
				Context ctx = new Context();
				ctx.pushState(new TurtleState(origin, new Vector2D(1, 0).rotated(angle), Color.BLACK, 
						unitLength * Math.pow(unitLengthDegreeScaler, arg0)));
				
				for(char c : generate(arg0).toCharArray()) {
					if(commands.get(c) != null) 
						commands.get(c).execute(ctx, arg1);
				}
				
			}

			@Override
			public String generate(int arg0) {
				String generated = axiom;
				StringBuilder sb = new StringBuilder();
				
				for(int i = 0; i < arg0; i++) {
					
					for(char c : generated.toCharArray()) {
						String tmp = productions.get(c);
						
						if(tmp != null) {
							sb.append(tmp);
						} else {
							sb.append(c);
						}
					}
					generated = sb.toString();
					sb.delete(0, sb.length());
				}
				
				System.out.println(generated);
				return generated;
			}
			
		};
		
		
		
		return lsystem;
	}

	@Override
	public LSystemBuilder configureFromText(String[] arg0) {
		for(String line : arg0) {
			if(line.equals("")) {
				continue;
			}
			
			String[] properties = line.split("\\s+");
			
			switch(properties[0]) {
				case "origin":
					setOrigin(Double.parseDouble(properties[1]),
							Double.parseDouble(properties[2]));
					break;
				case "angle":
					setAngle(Double.parseDouble(properties[1]));
					break;
				case "unitLength":
					setUnitLength(Double.parseDouble(properties[1]));
					break;
				case "command":
					if(properties[2].equals("push") || properties[2].equals("pop")) {
						registerCommand(properties[1].charAt(0), properties[2]);
					} else {
						registerCommand(properties[1].charAt(0), properties[2] + " " + properties[3]);
					}
					break;
				case "unitLengthDegreeScaler":
					if(properties.length == 2) {
						String[] tmp = properties[1].split("/");
						setUnitLengthDegreeScaler(Double.parseDouble(tmp[0]) / Double.parseDouble(tmp[1]));
					}
					if(properties.length == 3) {
						if(properties[1].contains("/")) {
							setUnitLengthDegreeScaler(
									Double.parseDouble(properties[1].substring(0, properties[1].length() - 1)) / 
									Double.parseDouble(properties[2]));
						} else {
							setUnitLengthDegreeScaler(
									Double.parseDouble(properties[1]) /
									Double.parseDouble(properties[2].substring(1, properties[2].length())));
						}
					}
					
					if(properties.length == 4) {
						setUnitLengthDegreeScaler(Double.parseDouble(properties[1]) / 
												Double.parseDouble(properties[3]));
						
					}
					break;
				case "axiom":
					setAxiom(properties[1]);
					break;
				case "production":
					registerProduction(properties[1].charAt(0), properties[2]);
					break;
			}
		}
		
		
		return this;
	}

	@Override
	public LSystemBuilder registerCommand(char arg0, String arg1) {
		String[] command = arg1.split("\\s+"); 
		
		switch(command[0]) {
			case "draw":
				commands.put(arg0, new DrawCommand(Double.parseDouble(command[1])));
				break;
			case "rotate":
				Double tmp = Double.parseDouble(command[1]);
				
				commands.put(arg0, new RotateCommand(tmp * Math.PI / 180));
				break;
			case "skip":
				commands.put(arg0, new SkipCommand(Double.parseDouble(command[1])));
				break;
			case "scale":
				commands.put(arg0, new ScaleCommand(Double.parseDouble(command[1])));
				break;
			case "push":
				commands.put(arg0, new PushCommand());
				break;
			case "pop":
				commands.put(arg0, new PopCommand());
				break;
			case "color":
				commands.put(arg0, new ColorCommand(Color.decode("#" + command[1])));
				break;
		}
		
		return this;
	}

	@Override
	public LSystemBuilder registerProduction(char arg0, String arg1) {
		productions.put(arg0, arg1);
		
		return this;
	}

	@Override
	public LSystemBuilder setAngle(double arg0) {
		angle = arg0 * Math.PI / 180;
		
		return this;
	}

	@Override
	public LSystemBuilder setAxiom(String arg0) {
		axiom = arg0;
		
		return this;
	}

	@Override
	public LSystemBuilder setOrigin(double arg0, double arg1) {
		origin = new Vector2D(arg0, arg1);
		
		return this;
	}

	@Override
	public LSystemBuilder setUnitLength(double arg0) {
		unitLength = arg0;
		
		return this;
	}

	@Override
	public LSystemBuilder setUnitLengthDegreeScaler(double arg0) {
		unitLengthDegreeScaler = arg0;
		
		return this;
	}

}
