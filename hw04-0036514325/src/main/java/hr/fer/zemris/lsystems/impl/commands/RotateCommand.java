package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.*;

/**
 * Rotates current direction of turtle.
 * @author tgrbesa
 *
 */
public class RotateCommand implements Command{
	/**
	 * Angle of rotation for turtle.
	 */
	private double angle;
	
	/**
	 * Basic constructor.
	 * @param angle
	 */
	public RotateCommand(double angle) {
		this.angle = angle;
	}
	
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState()
			.getCurrentDirection()
			.rotate(this.angle);
	}
}
