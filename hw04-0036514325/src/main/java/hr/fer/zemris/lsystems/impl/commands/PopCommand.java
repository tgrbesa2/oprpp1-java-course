package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.*;

/**
 * Class that implements command and pops one state from stack.
 * @author tgrbesa
 *
 */
public class PopCommand implements Command {
	
	
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.popState();
	}
}
