package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.oprpp1.math.Vector2D;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.*;

/**
 * Scales current context length.
 * @author tgrbesa
 *
 */
public class ScaleCommand implements Command {
	private double factor;
	
	public ScaleCommand(double factor) {
		this.factor = factor;
	}
	
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().setCurrentMovingLength(
				ctx.getCurrentState().getCurrentMovingLength() * factor);
	}
}
