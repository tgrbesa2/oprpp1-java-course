package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.lsystems.Painter;

/**
 * Command interface.
 * @author tgrbesa
 *
 */
public interface Command {
	void execute(Context ctx, Painter painter);
}	
