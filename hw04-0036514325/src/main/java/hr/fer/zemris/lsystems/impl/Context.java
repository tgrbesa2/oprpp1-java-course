package hr.fer.zemris.lsystems.impl;

import hr.fer.oprpp1.custom.collections.*;

/**
 * Instances of this class are capable of showing fractals-
 * @author tgrbesa
 *
 */
public class Context {

	/**
	 * Stack of turtle states.
	 */
	ObjectStack<TurtleState> stack;
	
	/**
	 * Basic constructor(initializes stack).
	 */
	public Context() {
		stack = new ObjectStack<TurtleState>();
	}
	
	/**
	 * Returns current state(doesn't remove it)
	 * @return
	 */
	public TurtleState getCurrentState() {
		return stack.peek();
	}
	
	/**
	 * Pushes on top <code>state</code>
	 * @param state
	 */
	public void pushState(TurtleState state) {
		stack.push(state);
	}
	
	/**
	 * Deletes state from top.
	 */
	public void popState() {
		stack.pop();
	}
}
