package hr.fer.zemris.lsystems.impl;

import java.awt.Color;

import hr.fer.oprpp1.math.*;

/**
 * Class models turtle position and direction of it's head.
 * @author tgrbesa
 *
 */
public class TurtleState {
	/**
	 * Current turtle position.
	 */
	private Vector2D currentPosition;
	
	/**
	 * Turtle direction.
	 */
	private Vector2D currentDirection;
	
	/**
	 * Color which turlte draws with.
	 */
	private Color color;
	
	/**
	 * Current effective move length.
	 */
	private double currentMovingLength;
	
	/**
	 * Basic constructor.
	 * @param currentPosition
	 * @param currentDirection
	 * @param color
	 * @param currentMovingLength
	 */
	public TurtleState(Vector2D currentPosition, Vector2D currentDirection, Color color, double currentMovingLength) {
		super();
		this.currentPosition = currentPosition;
		this.currentDirection = currentDirection;
		this.color = color;
		this.currentMovingLength = currentMovingLength;
	}



	/**
	 * Method imitates copy constructor, returns new TurtleState
	 * as copy of this TurtleState.
	 * @return
	 */
	public TurtleState copy() {
		return new TurtleState(currentPosition.copy(), currentDirection.copy(), new Color(color.getRGB()), currentMovingLength);
	}

	public Vector2D getCurrentDirection() {
		return currentDirection;
	}
	
	public Vector2D getCurrentPosition() {
		return currentPosition;
	}



	public Color getColor() {
		return color;
	}



	public void setColor(Color color) {
		this.color = color;
	}



	public double getCurrentMovingLength() {
		return currentMovingLength;
	}



	public void setCurrentMovingLength(double currentMovingLength) {
		this.currentMovingLength = currentMovingLength;
	}



	public void setCurrentPosition(Vector2D currentPosition) {
		this.currentPosition = currentPosition;
	}



	public void setCurrentDirection(Vector2D currentDirection) {
		this.currentDirection = currentDirection;
	}

	
	
}
