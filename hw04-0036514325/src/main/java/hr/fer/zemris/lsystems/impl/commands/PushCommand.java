package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.*;

/**
 * Pushes last state from stack again.
 * @author tgrbesa
 *
 */
public class PushCommand implements Command {
	
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState tmp = ctx.getCurrentState().copy();
		ctx.pushState(tmp);
	}

}
