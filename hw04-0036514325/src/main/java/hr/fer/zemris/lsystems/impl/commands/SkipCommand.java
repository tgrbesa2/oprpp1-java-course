package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.oprpp1.math.Vector2D;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.*;

/**
 * Skips line in GUI.
 * @author tgrbesa
 *
 */
public class SkipCommand implements Command {
	/**
	 * Scaler of length.
	 */
	private double step;
	
	
	public SkipCommand(double step) {
		this.step = step;
	}
	
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState currentState = ctx.getCurrentState();
		
		currentState.getCurrentPosition().add(
				currentState.getCurrentDirection()
				.scaled(step * currentState.getCurrentMovingLength()));
	}
}
