package hr.fer.oprpp1.math;

/**
 * Class represents two dimensional vector.
 * @author tgrbesa
 *
 */
public class Vector2D {
	/**
	 * X coordinate of vector.
	 */
	private double x;
	
	/**
	 * Y coordinate of vector.
	 */
	private double y;
	
	
	public Vector2D(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	/**
	 * Add's <code>offset</code> vector to this vector.
	 * @param offset
	 */
	public void add(Vector2D offset) {
		this.x += offset.x;
		this.y += offset.y;
	}
	
	/**
	 * Add's <code>offset</code> vector to this vector and returns new vector as result.
	 * @param offset
	 * @return
	 */
	public Vector2D added(Vector2D offset) {
		return new Vector2D(this.x + offset.x, this.y + offset.y);
	}
	
	/**
	 * Rotates this vector for value of <code>angle</code>.
	 * @param angle
	 */
	public void rotate(double angle) {
		double r = Math.hypot(x, y);
		double currentAngle = Math.atan2(y, x);
		
		currentAngle += angle;
		this.x = Math.cos(currentAngle) * r;
		this.y = Math.sin(currentAngle) * r;
	}
	
	/**
	 * Rotates this vector for value of <code>angle</code> and returns new rotated.
	 * @param angle
	 */
	public Vector2D rotated(double angle) {
		double r = Math.hypot(x, y);
		double currentAngle = Math.atan2(y, x);
		
		currentAngle += angle;
		
		return new Vector2D(Math.cos(currentAngle) * r, Math.sin(currentAngle) * r);
	}
	
	/**
	 * Scales this vector for value of <code>scaler</code>
	 * @param scaler
	 */
	public void scale(double scaler) {
		this.x *= scaler;
		this.y *= scaler;
	}
	
	/**
	 * Scales this vector for value of <code>scaler</code> and returns new one scaled.
	 * @param scaler
	 * @return
	 */
	public Vector2D scaled(double scaler) {
		return new Vector2D(this.x * scaler, this.y * scaler );
	}
	
	/**
	 * Returns copy of this vector.
	 * @return
	 */
	public Vector2D copy() {
		return new Vector2D(this.x, this.y);
	}
	
}
