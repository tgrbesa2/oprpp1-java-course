package hr.fer.oprpp1.hw04.db;

import java.util.function.Predicate;

/**
 * Functional interface.
 * Accepts student record given condition.
 * @author tgrbesa
 *
 */
public interface IFilter {
	boolean accepts(StudentRecord record);
}
