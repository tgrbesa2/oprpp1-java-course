package hr.fer.oprpp1.hw04.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Demo {

	public static void main(String[] args) throws IOException {
		String docBody = new String(
				Files.readAllBytes(Paths.get("src/test/resources/database.txt")),
				StandardCharsets.UTF_8);
		String[] split = docBody.split("\n");
		

		StudentDatabase database = new StudentDatabase(split);
		
		Scanner sc = new Scanner(System.in);
		String line;
		
		while(true) {
			System.out.printf("%s", "> ");
			line = sc.nextLine();
			
			if(line.trim().equals("exit")) {
				System.out.println("Goodbye! :)");
				System.exit(0);
			}
			
			if(!line.startsWith("query")) {
				System.out.println("Unknown command");
				continue;
			}
			
			QueryParser parser;
			try {
				parser = new QueryParser(line.substring(5, line.length()));
			} catch (Exception ex) {
				System.out.println("Invalid input!");
				continue;
			}
				
			Query(database, parser);
			
		}

	}
	
	private static void Query(StudentDatabase database, QueryParser parser) {
		List<StudentRecord> records = null;
		int lastnameLength = 0;
		int firstnameLength = 0;
		StringBuilder sb = new StringBuilder();
		
		if(parser.isDirectQuery()) {
			records = new ArrayList<StudentRecord>();
			records.add(database.forJMBAG(parser.getQueriedJMBAG()));
			
			firstnameLength = records.get(0).getFirstName().length();
			lastnameLength = records.get(0).getLastName().length();
		} else {
			records = database.filter(new QueryFilter(parser.getQuery()));
			
			for(var rec : records) {
				if(rec.getLastName().length() > lastnameLength) {
					lastnameLength = rec.getLastName().length();
				}
				if(rec.getFirstName().length() > firstnameLength) {
					firstnameLength = rec.getFirstName().length();
				}
			}
			
		}
		if(records.isEmpty()) {
			System.out.println("Records selected: 0");
			return;
		}
		
		String firstLastRow = "+";
		
		for(int i = 0; i < 12; i++) {
			firstLastRow += "=";
		}
		firstLastRow += "+";
		
		for(int i = 0; i < lastnameLength + 2; i++) {
			firstLastRow += "=";
		}
		
		firstLastRow += "+";
		
		for(int i = 0; i < firstnameLength + 2; i++) {
			firstLastRow += "=";
		}
		
		firstLastRow += "+===+";
		
		sb.append(firstLastRow + '\n');
		
		for(var rec : records) {
			sb.append("| ");
			sb.append(rec.getJmbag());
			sb.append(" | ");
			sb.append(rec.getLastName());
			for(int i = 0; i < (lastnameLength - rec.getLastName().length()); i++) {
				sb.append(" ");
			}
			sb.append(" | ");
			sb.append(rec.getFirstName());
			for(int i = 0; i < (firstnameLength - rec.getFirstName().length()); i++) {
				sb.append(" ");
			}
			sb.append(" | ");
			sb.append(rec.getFinalGrade());
			sb.append(" |\n");
		}
		
		sb.append(firstLastRow + '\n');
		sb.append("Records selected: " + records.size());
		System.out.println(sb.toString());
	}

}
