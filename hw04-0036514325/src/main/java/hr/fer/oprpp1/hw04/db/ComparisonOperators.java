package hr.fer.oprpp1.hw04.db;

/**
 * Class contains various instances of <code>IComparisonOperator</code>.
 * @author tgrbesa
 *
 */
public class ComparisonOperators {
	public static final IComparisonOperator LESS = (s1, s2) -> s1.compareTo(s2) < 0;
	public static final IComparisonOperator LESS_OR_EQUAL = (s1, s2) -> s1.compareTo(s2) <= 0;
	public static final IComparisonOperator GREATER = (s1, s2) -> s1.compareTo(s2) > 0;
	public static final IComparisonOperator GREATER_OR_EQUAL = (s1, s2) -> s1.compareTo(s2) >= 0;
	public static final IComparisonOperator EQUALS = (s1, s2) -> s1.compareTo(s2) == 0;
	public static final IComparisonOperator NOT_EQUALS = (s1, s2) -> s1.compareTo(s2) != 0;
	public static final IComparisonOperator LIKE = (s1, s2) -> {
			int counter = 0;
			for(char c : s2.toCharArray()) {
				if(Character.compare(c, '*') == 0) {
					counter++;
					
				}
			}
			if(counter != 1) throw new IllegalArgumentException("Like operator cannot contain multiple * chars");
			
			if(s1.length() + 1 < s2.length()) return false;
			
			if(s2.startsWith("*")) {
				return s1.endsWith(s2.substring(1, s2.length()));
			}
			if(s2.endsWith("*")) {
				return s1.startsWith(s2.substring(0, s2.length() - 1));
			}
			
			return s1.startsWith(s2.substring(0, s2.indexOf("*"))) && s1.endsWith(s2.substring(s2.indexOf("*") + 1, s2.length()));
			
	};
	
	

}
