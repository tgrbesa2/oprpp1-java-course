package hr.fer.oprpp1.hw04.db.lexer;

/**
 * Lexer of DB querries.
 * @author tgrbesa
 *
 */
public class QuerryLexer {
	/**
	 * Entry text stored in array of chars.
	 */
	private char[] data;
	
	/**
	 * Current token
	 */
	private Token token;
	
	/**
	 * Index of first unimproved char.
	 */
	private int currentIndex;
	
	public QuerryLexer(String text) {
		if(text == null) throw new NullPointerException("Input must be non null reference!");
		
		data = text.toCharArray();
		currentIndex = 0;
	}
	
	
	/**
	 * Generates and returns next token.
	 * @return
	 */
	public Token nextToken() {
		if(currentIndex > data.length) throw new RuntimeException("All tokens already generated");
		String currentToken = "";
		TokenType currentType = null;
		
		while(true) {
			//Reached end of string
			if(currentIndex == data.length) {
				if(currentType != null) {
					token = new Token(currentType, currentToken);
					break;
				}
				token = new Token(TokenType.EOF, null);
				currentIndex++;
				break;
			}
			
			if(currentToken.isEmpty() && data[currentIndex] == ' ') {
				currentIndex++;
				continue;
			}
			
			if(data[currentIndex] == ' ' && currentType != TokenType.STRING) {
				currentIndex++;
				token = new Token(currentType, currentToken);
				break;
			}
			
			if(data[currentIndex] == '\"') {
				if(currentType != TokenType.STRING && currentType != null) {
					token = new Token(currentType, currentToken);
					break;
				}
				if(currentType == TokenType.STRING) {
					token = new Token(currentType, currentToken);
					currentIndex++;
					break;
				}
				
				currentType = TokenType.STRING;
				currentIndex++;
				continue;
			}
			
			if(Character.isLetter(data[currentIndex]) || Character.isDigit(data[currentIndex])) {
				if(currentType == null) {
					currentType = TokenType.KEYWORD;
				}
				
				currentToken += data[currentIndex++];
				continue;
			}
			
			if(data[currentIndex] == '*') {
				if(currentType != TokenType.STRING) throw new RuntimeException("Wrong usage of * char");
				currentToken += data[currentIndex++];
				continue;
			}
			
			if(currentToken.isEmpty()) {
				currentToken += data[currentIndex++];
				token = new Token(TokenType.OPERATOR, currentToken);
			} else {
				token = new Token(currentType, currentToken);
				
			}
			break;
			
			
			
		}
		
		return token;
	}
	
	public Token getToken() {
		return token;
	}
}
