package hr.fer.oprpp1.hw04.db.lexer;

public enum TokenType {
	EOF,
	STRING,
	OPERATOR,
	KEYWORD
}
