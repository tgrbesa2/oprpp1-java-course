package hr.fer.oprpp1.hw04.db;

import java.util.List;

/**
 * Class implements Ifilter and is used when testing queries.
 * @author tgrbesa
 *
 */
public class QueryFilter implements IFilter{
	private List<ConditionalExpression> list;
	
	public QueryFilter(List<ConditionalExpression> list) {
		this.list = list;
	}
	
	public boolean accepts(StudentRecord record) {
		for(ConditionalExpression ce : list) {
			if(!ce.getOperator().satisfied(ce.getValue().get(record), ce.getLiteral())) {
				return false;
			}
		}
		
		return true;
	}
}
