package hr.fer.oprpp1.hw04.db;

/**
 * Contains various instances of <code>IFieldValueGetter</code>
 * that represents getter of specific atribute.
 * @author tgrbesa
 *
 */
public class FieldValueGetter {
	public static final IFieldValueGetter FIRST_NAME = r -> r.getFirstName();
	public static final IFieldValueGetter LAST_NAME = r -> r.getLastName();
	public static final IFieldValueGetter JMBAG = r -> r.getJmbag();

}
