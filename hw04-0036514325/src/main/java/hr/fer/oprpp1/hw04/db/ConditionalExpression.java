package hr.fer.oprpp1.hw04.db;
/**
 * Conditional expression used in extraction specific data
 * from student record.
 * @author tgrbesa
 *
 */
public class ConditionalExpression {
	private IFieldValueGetter value;
	private IComparisonOperator operator;
	private String literal;
	
	
	public ConditionalExpression(IFieldValueGetter value, IComparisonOperator operator, String literal) {
		super();
		this.value = value;
		this.operator = operator;
		this.literal = literal;
	}
	public IFieldValueGetter getValue() {
		return value;
	}
	public IComparisonOperator getOperator() {
		return operator;
	}
	public String getLiteral() {
		return literal;
	}
	
	
}
