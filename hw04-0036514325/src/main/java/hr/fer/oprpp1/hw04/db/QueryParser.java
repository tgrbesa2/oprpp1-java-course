package hr.fer.oprpp1.hw04.db;

import java.util.ArrayList;
import java.util.List;

import hr.fer.oprpp1.hw04.db.lexer.*;

public class QueryParser {
	/**
	 * Lexer of string query.
	 */
	private QuerryLexer lexer;
	
	/**
	 * Flag of query.
	 */
	private boolean direct;
	
	/**
	 * If query is direct, stores query's jmbag
	 */
	private String directJmbag = null;
	
	/**
	 * list of conditions parsed from querry
	 */
	private List<ConditionalExpression> listOfConditions;
	
	public QueryParser(String text) {
		lexer = new QuerryLexer(text);
		direct = false;
		
		parse();
	}
	
	/**
	 * Method that parses query from tokens.
	 */
	private void parse() {
		Token token = lexer.nextToken();
		Token sequencedToken;
		listOfConditions = new ArrayList<ConditionalExpression>();
		
		while(token.getValue() != null) {
			if(token.getType() != TokenType.KEYWORD) throw new RuntimeException("Query written wrongly!"); 				
			
			String atribute = token.getValue().toLowerCase();
			IFieldValueGetter valueGetter;
			IComparisonOperator operatorGetter;
			String literal;
			
				
			switch(atribute) {
				case "firstname":
					valueGetter = FieldValueGetter.FIRST_NAME;
					break;
				case "lastname":
					valueGetter = FieldValueGetter.LAST_NAME;
					break;
				case "jmbag":
					valueGetter = FieldValueGetter.JMBAG;
					break;
				default:
					throw new RuntimeException("Wrongly used keyword");
			}
			
			token = lexer.nextToken();
			
			if(token.getType() != TokenType.OPERATOR) {
				if(!token.getValue().toLowerCase().equals("like"))
				throw new RuntimeException("Expected operator, but got " + token.getValue());
			}
			
			atribute = token.getValue().toLowerCase();
			
			token = lexer.nextToken();
			if(token.getType() == TokenType.OPERATOR) {
				atribute += token.getValue();
			}
			
			switch(atribute) {
				case "<":
					operatorGetter = ComparisonOperators.LESS;
					break;
				case ">":
					operatorGetter = ComparisonOperators.GREATER;
					break;
				case "=":
					operatorGetter = ComparisonOperators.EQUALS;
					break;
				case "!=":	
					operatorGetter = ComparisonOperators.NOT_EQUALS;
					break;
				case "like":
					operatorGetter = ComparisonOperators.LIKE;
					break;
				case "<=":
					operatorGetter = ComparisonOperators.LESS_OR_EQUAL;
					break;
				case ">=":
					operatorGetter = ComparisonOperators.GREATER_OR_EQUAL;
				default:
					throw new RuntimeException("Wrong operator inserted!");
			}
			
		
			if(token.getType() != TokenType.STRING) {
				throw new RuntimeException("Expected literal but recieved" + token.getValue());
			}
			
			literal = token.getValue();
			
			listOfConditions.add(new ConditionalExpression(valueGetter, operatorGetter, literal));
			
			token = lexer.nextToken();
				
			if(listOfConditions.size() == 1 && token.getType() == TokenType.EOF) {
				if(valueGetter == FieldValueGetter.JMBAG && operatorGetter == ComparisonOperators.EQUALS) {
					direct = true;
					directJmbag = literal;
				}
			}
			if(token.getValue() == null) break;
			
			if(token.getValue().toLowerCase().equals("and")) {
				token = lexer.nextToken();
			}
			
			
		}
	}
	
	public boolean isDirectQuery() {
		return direct;
	}
	
	public String getQueriedJMBAG() {
		if(directJmbag == null) throw new RuntimeException("Query is not direct!");
		return directJmbag;
	}
	
	public List<ConditionalExpression> getQuery() {
		return listOfConditions;
	}
}
