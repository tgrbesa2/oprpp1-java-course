package hr.fer.oprpp1.hw04.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class represents simplified database of students.
 * @author tgrbesa
 *
 */
public class StudentDatabase {
	/**
	 * List contains all students in database.
	 */
	private List<StudentRecord> listOfStudents;
	
	/**
	 * Map contains all students in database.
	 * Used for O(1) search.
	 */
	private Map<String, StudentRecord> mapOfStudents;
	
	/**
	 * Initializes database, duplicates cannot be inserted.
	 * @param records
	 */
	public StudentDatabase(String[] records) {
		listOfStudents = new ArrayList<>();
		mapOfStudents = new HashMap<>();
		
		for(String tmp : records) {
			String[] tmpRecord = tmp.split("\\s+");
			if(tmpRecord.length == 4) {
				if(mapOfStudents.get(tmpRecord[0]) == null) {
					int grade = Integer.parseInt(tmpRecord[3]);
					if(grade < 1 || grade > 5) throw new IllegalArgumentException("Grade cannot be " + grade);
					
					StudentRecord sr = new StudentRecord(tmpRecord[0], tmpRecord[1],
										tmpRecord[2], grade);
					
					listOfStudents.add(sr);
					mapOfStudents.put(sr.getJmbag(), sr);
				}
				
			} else if(tmpRecord.length  == 5) {
				if(mapOfStudents.get(tmpRecord[0]) == null) {
					int grade = Integer.parseInt(tmpRecord[4]);
					if(grade < 1 || grade > 5 ) throw new IllegalArgumentException("Grade cannot be " + grade);
					
					StudentRecord sr = new StudentRecord(tmpRecord[0], tmpRecord[1] + " " + tmpRecord[2],
														tmpRecord[3], grade);
					
					listOfStudents.add(sr);
					mapOfStudents.put(sr.getJmbag(), sr);
				}
			} else {
				throw new RuntimeException();
			}
			
		}
	}
	
	/**
	 * Returns in O(1) student record for given <code>jmbag</code>
	 * @param jmbag
	 * @return
	 */
	public StudentRecord forJMBAG(String jmbag) {
		return mapOfStudents.get(jmbag);
	}
	
	public List<StudentRecord> filter(IFilter filter) {
		List<StudentRecord> tmp = new ArrayList<>();
		for(StudentRecord s : this.listOfStudents) {
			if(filter.accepts(s) ) {
				tmp.add(s);
			}
		}
		return tmp;
	}
	
	public List<StudentRecord> getListOfStudents() {
		return this.listOfStudents;
	}
	
	public Map<String, StudentRecord> getMapOfStudents() {
		return this.mapOfStudents;
	}

	
}
