package hr.fer.oprpp1.hw04.db;

/**
 * Strategy interface that is used when parsing query from text.
 * @author tgrbesa
 *
 */
public interface IComparisonOperator {
	boolean satisfied(String value1, String value2);
}
