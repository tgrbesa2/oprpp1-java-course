package hr.fer.oprpp1.custom.collections;

/**
 * Dictionary data structure(In java world maps)
 * @author tgrbesa
 *
 * @param <T>
 */
public class Dictionary<K, V> {
	
	/**
	 * Private class that represents one mapping of Key and Value
	 * @author tgrbesa
	 *
	 * @param <K>
	 * @param <V>
	 */
	private class MapNode<T, S> {
		private T key;
		private S value;
		
		public MapNode(T key, S value) {
			if(key == null) throw new NullPointerException("Key must be non-null reference!");
			this.key = key;
			this.value = value;
		}
		
	}
	
	/**
	 * Array structure used internally for dictionary.
	 */
	private ArrayIndexedCollection<MapNode<K, V>> data;
	
	/**
	 * Constructor.
	 */
	public Dictionary() {
		data = new ArrayIndexedCollection<MapNode<K, V>>();
	}
	
	/**
	 * Returns true if dictionary is not empty, false otherwise.
	 * @return
	 */
	public boolean isEmpty() {
		return this.size() == 0;
	}
	
	/**
	 * Returns size of dictionary.
	 * @return
	 */
	public int size() {
		return data.size();
	}
	
	/**
	 * Clears all items from dictionary.
	 */
	public void clear() {
		data.clear();
	}
	
	/**
	 * Puts selected key and value into dictionary.
	 * @param key
	 * @param value
	 */
	public V put(K key, V value) {
		ElementsGetter<MapNode<K, V>> iterator = data.createElementsGetter();
		boolean flag = false;
		MapNode<K, V> tmp = null;
		V tmp2 = null;
		
		while(iterator.hasNextElement()) {
			tmp = iterator.getNextElement();
			if(tmp.key == key) {
				flag = true;
				break;
			}
		}
		
		if(flag) {
			tmp2 = tmp.value;
			tmp.value = value;
		} else {
			data.add(new MapNode<K, V>(key, value));
		}
		
		return tmp2;
	}
	
	
	/**
	 * Returns value for selected key.
	 * If key doesnt have its value stored in dictionary, null is returned.
	 * @param key
	 * @param value
	 * @return
	 */
	public V get(K key) {
		ElementsGetter<MapNode<K, V>> iterator = data.createElementsGetter();
		V tmp = null;
		MapNode<K, V> tmp2;
		
		while(iterator.hasNextElement()) {
			tmp2 = iterator.getNextElement();
			if(tmp2.key == key) {
				tmp = tmp2.value;
				break;
			}
		}
		
		return tmp;
	}
	
	/**
	 * Removes value for selected key.
	 * If key is not stored in dictionary, null is returned.
	 * @param key
	 * @return
	 */
	public V remove(K key) {
		ElementsGetter<MapNode<K, V>> iterator = data.createElementsGetter();
		V tmp = null;
		MapNode<K, V> tmp2 = null;
		
		while(iterator.hasNextElement()) {
			tmp2 = iterator.getNextElement();
			if(tmp2.key == key) {
				tmp = tmp2.value;
				break;
			}
		}
		
		if(tmp != null) {
			data.remove(tmp2);
		}
		
		return tmp;
	}
 	
	
}
