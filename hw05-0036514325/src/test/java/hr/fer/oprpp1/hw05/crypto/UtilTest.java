package hr.fer.oprpp1.hw05.crypto;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;
public class UtilTest {
	
	
	@Test
	public void testSimpleHexToByte() {
		String bla = "01aE22";
		
		byte[] ha = Util.hextobyte(bla);
		assertEquals(ha[0], 1);
		assertEquals(ha[1], -82);
		assertEquals(ha[2], 34);
	}
	
	@Test
	public void testEmptyHexToByte() {
		String bla = "";
		byte[] ha = Util.hextobyte(bla);
		
		assertEquals(ha.length, 0);
	}
	
	@Test
	public void testInvalidSizeHexToByte() {
		String a = "01345";
		
		assertThrows(IllegalArgumentException.class, () -> Util.hextobyte(a));
	}
	
	@Test
	public void testIllegalHexToByte() {
		String a = "AeFžža";

		assertThrows(IllegalArgumentException.class, () -> Util.hextobyte(a));
	}
	
	@Test
	public void testSimpleByteToHex() {
		byte[] aray = {1, -82, 34};
		
		assertEquals("01ae22", Util.bytetohex(aray));
	}
	
	
}
