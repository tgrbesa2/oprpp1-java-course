package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

public class CatShellCommand implements ShellCommand {
	
	private List<String> commandDescription;
	
	public CatShellCommand() {
		commandDescription = new ArrayList<String>();
		commandDescription.add("Command cat takes one or two arguments.");
		commandDescription.add("The first is path to some file and is mandatory.");
		commandDescription.add("The second is charset name that should be used");
		commandDescription.add("to interpret chars from bytes. If not provided,");
		commandDescription.add("a default platform charset should be used.");
		commandDescription.add("This command opens given file and writes its content");
		commandDescription.add("to console");
		
		commandDescription = Collections.unmodifiableList(commandDescription);
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String firstArg = "";
		String secondArg = "";
		
		if(arguments.startsWith("\"")) {
			firstArg = arguments.substring(1, arguments.lastIndexOf('\"'));
			firstArg = firstArg.replace("\\\"", "\"");
			firstArg = firstArg.replace("\\\\", "\\");
			
			if(!(arguments.length() == arguments.lastIndexOf('\"') + 1)) {
				secondArg = arguments.substring(arguments.lastIndexOf('\"') + 2);
			}
		} else {
			if(arguments.contains(" ")) {
				firstArg = arguments.split(" ")[0];
				secondArg = arguments.split(" ")[1];
			} else {
				firstArg = arguments;
			}
		}
		
		Path p = Paths.get(firstArg);
		
		if(!Files.exists(p)) {
			env.writeln("File" + p.getFileName() + " does not exist!");
			return ShellStatus.CONTINUE;
		}
		
		if(Files.isDirectory(p)) {
			env.writeln("Given file" + p.getFileName() + " is directory, not file.");
			return ShellStatus.CONTINUE;
		}
		
		StringBuilder sb = new StringBuilder();
		if(secondArg.equals("")) {
			secondArg = "UTF-8";
		}
		
		try(InputStream is = Files.newInputStream(p)) {
			byte[] buff = new byte[4096];
			while(true) {
				int r = is.read(buff);
				if(r < 1) break;
				sb.append(new String(Arrays.copyOf(buff, r), secondArg));
			}
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading!");
			return ShellStatus.CONTINUE;
		}
		
		env.write(sb.toString());
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "cat";
	}

	@Override
	public List<String> getCommandDescription() {
		return commandDescription;
	}

}
