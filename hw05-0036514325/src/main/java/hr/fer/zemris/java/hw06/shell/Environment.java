package hr.fer.zemris.java.hw06.shell;

import java.util.SortedMap;

/**
 * Abstraction that is used in MyShell and is passed
 * to each defined command. Each implemented command
 * communicates with user through this interface.
 * @author tgrbesa
 *
 */
public interface Environment {
	/**
	 * Reads line from standard input.
	 * @return Returns string read from input.
	 * @throws ShellIOException
	 */
	String readLine() throws ShellIOException;
	
	/**
	 * Writes <code>text</code> on standard output.
	 * @param text
	 * @throws ShellIOException
	 */
	void write(String text) throws ShellIOException;
	
	/**
	 * Writes line <code>text</code> on standard output.
	 * @param text
	 * @throws ShellIOException
	 */
	void writeln(String text) throws ShellIOException;
	
	/**
	 * Method returns all defined methods
	 * as unmodifiable sorted map.
	 * @return
	 */
	SortedMap<String, ShellCommand> commands();
	
	Character getMultilineSymbol();
	
	void setMultilineSymbol(Character symbol);
	
	Character getPromptSymbol();
	
	void setPromptSymbol(Character symbol);
	
	Character getMorelinesSymbol();
	
	void setMorelinesSymbol(Character symbol);
}
