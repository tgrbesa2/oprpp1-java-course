package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

public class HelpShellCommand implements ShellCommand {
	
	private List<String> commandDescription;
	
	public HelpShellCommand() {
		commandDescription = new ArrayList<>();
		commandDescription.add("Command help can be called alone or with one");
		commandDescription.add("extra argument. If started with no argument");
		commandDescription.add("it lists names of all supported commands.");
		commandDescription.add("If started with argument it prints name and");
		commandDescription.add("the description of selected command.");

		commandDescription = Collections.unmodifiableList(commandDescription);
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(arguments.length() == 0) {
			env.commands().forEach((k, v) -> env.writeln(v.getCommandName()));
		} else {
			ShellCommand sc = env.commands().get(arguments);
			if(sc == null) {
				env.writeln("Unknown command, please try again!");
				return ShellStatus.CONTINUE;
			}
			sc.getCommandDescription().stream().forEach(i -> env.writeln(i));
		}
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "help";
	}

	@Override
	public List<String> getCommandDescription() {
		return commandDescription;
	}

}
