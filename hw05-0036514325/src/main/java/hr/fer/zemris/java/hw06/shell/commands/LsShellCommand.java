package hr.fer.zemris.java.hw06.shell.commands;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

public class LsShellCommand implements ShellCommand {
	
	private List<String> commandDescription;
	
	public LsShellCommand() {
		commandDescription = new ArrayList<>();
		commandDescription.add("Command ls takes a single argument - directory");
		commandDescription.add("and writes a directory listing.");
		
		commandDescription = Collections.unmodifiableList(commandDescription);
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		arguments = this.oneArgumentParse(arguments);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		File file = new File(arguments);
		
		if(!(file.exists())) {
			System.out.println("File does not exist!");
			return ShellStatus.CONTINUE;
		}
		
		if(!(file.isDirectory())) {
			System.out.println("File is not directory!");
			return ShellStatus.CONTINUE;
		}
		
		File[] children = file.listFiles();
		if(children.length == 0) {
			System.out.println("Directory is empty!");
			return ShellStatus.CONTINUE;
		}
		
		for(File f : children) {
			BasicFileAttributeView faView = Files.getFileAttributeView(Paths.get(f.getAbsolutePath()), BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS);
			BasicFileAttributes attributes;
			
			try {
				attributes = faView.readAttributes();
			} catch (IOException e) {
				System.out.println("Could not read directory " + arguments);
				return ShellStatus.CONTINUE;
			}
			
			FileTime fileTime = attributes.creationTime();
			String formattedDateTime = sdf.format(new Date(fileTime.toMillis()));
			
			String result = String.format("%s%s", f.isDirectory() ? "d" : "-", f.canRead() ? "r" : "-");
			result += String.format("%s%s", f.canWrite() ? "w" : "-", f.canExecute() ? "x" : "-");
			result += String.format("%14d %s %s\n", f.length(), formattedDateTime, f.getName());
			
			env.write(result);
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "ls";
	}

	@Override
	public List<String> getCommandDescription() {
		return commandDescription;
	}
}
	



