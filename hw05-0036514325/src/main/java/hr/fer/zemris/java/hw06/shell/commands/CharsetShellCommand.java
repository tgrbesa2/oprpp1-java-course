package hr.fer.zemris.java.hw06.shell.commands;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

public class CharsetShellCommand implements ShellCommand {

	private List<String> commandDescription;
	
	public CharsetShellCommand() {
		commandDescription = new ArrayList<>();
		commandDescription.add("Command charset takes no argument and lists names");
		commandDescription.add("of supported charsets in MyShell.");

		commandDescription = Collections.unmodifiableList(commandDescription);
	}
	
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		Charset.availableCharsets().forEach((k, v) -> env.writeln(k));
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "charsets";
	}

	@Override
	public List<String> getCommandDescription() {
		return commandDescription;
	}
	

}
