package hr.fer.zemris.java.hw06.shell;


/**
 * Exceptions used for MyShell.
 * @author tgrbesa
 *
 */
public final class ShellIOException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ShellIOException() {
		super();
	}
	
	public ShellIOException(String message) {
		super(message);
	}
}
