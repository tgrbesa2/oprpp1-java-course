package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

public class HexdumpShellCommand implements ShellCommand {
	
	private List<String> commandDescription;
	
	public HexdumpShellCommand() {
		commandDescription = new ArrayList<>();
		commandDescription.add("Command hexdump takes one single argument: file name,");
		commandDescription.add("and produces hex-output. It prints code for standard");
		commandDescription.add("characters, for all other characters '.' is printed.");

		commandDescription = Collections.unmodifiableList(commandDescription);
	}
	
	
	@Override
	public ShellStatus executeCommand(Environment env,  String arguments) {
		arguments = this.oneArgumentParse(arguments);
		
		Path path = Paths.get(arguments);
		
		if(!Files.exists(path)) {
			System.out.println("File doesn't exist!");
			return ShellStatus.CONTINUE;
		}
		
		try(InputStream is = Files.newInputStream(path)) {
			byte[] buff = new byte[16];
			int counter = 0;
			String[] tmp = new String[4];
			int r;
			
			while(true) {
				r = is.read(buff);
				if(r < 1) break;
				tmp[0] = String.format("%07X:", counter);
				counter += 16;
				tmp[1] = "";
				tmp[2] = "";
				tmp[3] = " ";
				
				for(int i = 0; i < r; i++) {
					if(i < 8) {
						tmp[1] += String.format(" %02X", buff[i]);

					} else {
						tmp[2] += String.format("%02X ", buff[i]);
					}
					if((int) buff[i] >= 32 && (int) buff[i] <= 127) {
							tmp[3] += String.format("%s",(char) buff[i]);
					} else {
						tmp[3] += String.format("%s", ".");
					}
				}
				if(tmp[1].length() < 24) {
					tmp[1] += " ".repeat(24 - tmp[1].length());
				}
				if(tmp[2].length() < 24) {
					tmp[2] += " ".repeat(24 - tmp[2].length());
				}
				
				env.writeln(Arrays.stream(tmp).collect(Collectors.joining("|", "", "")));
			}
			
			
		} catch (IOException ex) {
			System.out.println("Error while reading file!");
			
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "hexdump";
	}

	@Override
	public List<String> getCommandDescription() {
		return commandDescription;
	}
	


}
