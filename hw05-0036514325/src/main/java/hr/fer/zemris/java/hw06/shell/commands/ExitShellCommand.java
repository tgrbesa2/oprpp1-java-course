package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

public class ExitShellCommand implements ShellCommand {
	private List<String> commandDescription;
	
	public ExitShellCommand() {
		commandDescription = new ArrayList<>();
		commandDescription.add("Command exit exits from MyShell.");


		commandDescription = Collections.unmodifiableList(commandDescription);
	}
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		return ShellStatus.TERMINATE;
	}

	@Override
	public String getCommandName() {
		return "exit";
	}

	@Override
	public List<String> getCommandDescription() {
		return commandDescription;
	}

}
