package hr.fer.zemris.java.hw06.shell.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

public class TreeShellCommand implements ShellCommand{
	
	private List<String> commandDescription;
	
	public TreeShellCommand() {
		commandDescription = new ArrayList<>();
		commandDescription.add("Command tree takes a single argument - directory");
		commandDescription.add("and prints a tree.");
		
		commandDescription = Collections.unmodifiableList(commandDescription);
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		arguments = this.oneArgumentParse(arguments);
		File file = new File(arguments);
		
		if(!file.exists()) {
			System.out.println("File does not exist!");
			return ShellStatus.CONTINUE;
		}
		
		if(file.isFile()) {
			System.out.println("Argument must be directory!");
			return ShellStatus.CONTINUE;
		}
	
		WriteTree wt = new WriteTree();
		goAround(file, wt);
		
		env.write(wt.getTree());
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "tree";
	}

	@Override
	public List<String> getCommandDescription() {
		return commandDescription;
	}
	
	
	private static class WriteTree implements Job {
		int level;
		StringBuilder sb;
		
		public WriteTree() {
			sb = new StringBuilder();
		}

		@Override
		public void findFile(File path) {
			write(path);
		}

		@Override
		public void enteringDirectory(File path) {
			write(path);
			level++;	
		}

		@Override
		public void exitingDirectory(File path) {
			level--;
		}
		
		public String getTree() {
			return sb.toString();
		}
		
		public void write(File path) {
			sb.append(String.format("%s%s%n", " ".repeat(level * 2), path.getName()));
		}
	}
	
	private static void goAround(File path, Job job) {
		if(path.isFile()) {
			return;
		}
		
		if(!path.isDirectory()) {
			return;
		}
	
		File[] children = path.listFiles();
		
		for(File f : children) {
			job.enteringDirectory(f);
			goAround(f, job);
			job.exitingDirectory(f);
			
		}
	}
	
}
