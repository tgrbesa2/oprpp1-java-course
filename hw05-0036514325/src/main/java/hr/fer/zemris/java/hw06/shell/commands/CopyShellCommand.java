package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

public class CopyShellCommand implements ShellCommand{
	
	
	private List<String> commandDescription;
	
	public CopyShellCommand() {
		commandDescription = new ArrayList<>();
		commandDescription.add("Command copy expects two arguments: source file ");
		commandDescription.add("name and destination file name(i.e. paths and names).");
		commandDescription.add("If destination file exists, you should ask user");
		commandDescription.add("is it allowed to overwrite it. Copy command works");
		commandDescription.add("only with files(no directories.");
		
		commandDescription = Collections.unmodifiableList(commandDescription);
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String firstArg = "";
		String secondArg = "";
		
		if(arguments.contains("\"")) {
			if(arguments.indexOf('\"') == 0 && arguments.endsWith("\"")) {
				String tmp = arguments.substring(1, arguments.length());
				firstArg = tmp.substring(0, tmp.indexOf("\" "));
				firstArg = firstArg.replace("\\\"", "\"");
				firstArg = firstArg.replace("\\\\", "\\");
				secondArg = tmp.substring(tmp.indexOf("\" ") + 3, tmp.length() - 1);
				secondArg = secondArg.replace("\\\"", "\"");
				secondArg = secondArg.replace("\\\\", "\\");
			}
			
			if(arguments.indexOf('\"') == 0) {
				firstArg = arguments.substring(1, arguments.lastIndexOf('\"'));
				firstArg = firstArg.replace("\\\"", "\"");
				firstArg = firstArg.replace("\\\\", "\\");
				secondArg = arguments.substring(arguments.lastIndexOf('\"') + 2, arguments.length());
			} else {
				firstArg = arguments.substring(0, arguments.indexOf('\"') - 1);
				secondArg = arguments.substring(arguments.indexOf('\"') + 1, arguments.length() - 1);
				secondArg = secondArg.replace("\\\"", "\"");
				secondArg = secondArg.replace("\\\\", "\\");
			}
		} else {
			firstArg = arguments.split(" ")[0];
			secondArg = arguments.split(" ")[1];
		}
		
		Path path1 = Paths.get(firstArg);
		Path path2 = Paths.get(secondArg);
		
		if(!Files.exists(path1)) {
			System.out.println("File does not exist!");
			return ShellStatus.CONTINUE;
		}
		
		if(Files.isDirectory(path1)) {
			System.out.println("Directories cannot be coppied!");
			return ShellStatus.CONTINUE;
		}
		
		if(Files.exists(path2) && !Files.isDirectory(path2)) {
			env.writeln("File already exists. Overwrite file[Y/N] ?");
			String str1 = env.readLine();
			if(str1.equals("N")) return ShellStatus.CONTINUE;
		} else if (Files.exists(path2)) {
			if(firstArg.contains("/")) {
				secondArg += firstArg.substring(firstArg.lastIndexOf('/'), firstArg.length() -1);
			} else {
				secondArg += "/";
				secondArg += firstArg;
			}
			path2 = Paths.get(secondArg);
			
			try {	
				Files.createFile(path2);
			} catch (IOException e) {
				env.writeln("Error while creating file! Check if folder structure exists or if file already exists.");
				return ShellStatus.CONTINUE;
			}
		} else if ((secondArg.contains("/")) && !secondArg.contains(".")){
			env.writeln("File path doesnt exist. Try again!");
			return ShellStatus.CONTINUE;
		}
		
		try(InputStream is = Files.newInputStream(path1);
			OutputStream os = Files.newOutputStream(path2)) {
			
			byte[] buff = new byte[4096];
			while(true) {
				int r = is.read(buff);
				if(r < 1) break;
				os.write(buff, 0, r);
			}
			
		} catch (IOException ex) {
			System.out.println("Error while reading/writing");
			return ShellStatus.CONTINUE;
		}
		
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "copy";
	}

	@Override
	public List<String> getCommandDescription() {
		return commandDescription;
	}
	
}
