package hr.fer.zemris.java.hw06.shell;

import java.util.List;

/**
 * Abstraction of command. Each command implements this interface.
 * @author tgrbesa
 *
 */
public interface ShellCommand {
	/**
	 * Command takes <code>env</code> which is used
	 * for communication with user. It also takes
	 * <code>arguments</code> that are defined for
	 * each command. After parsing of arguments, it
	 * executes defined command. If command cannot be
	 * executed, appropriate message is written and
	 * user can write new command again.
	 * @param env
	 * @param arguments
	 * @return
	 */
	ShellStatus executeCommand(Environment env, String arguments);
	
	String getCommandName();
	
	List<String> getCommandDescription();
	
	/**
	 * Default method used for one argument commands.
	 * It helps parse strings if they have quotes.
	 * @param arguments
	 * @return
	 */
	default String oneArgumentParse(String arguments) {
		if(arguments.startsWith("\"")) {
			arguments = arguments.substring(1, arguments.length() - 1);
			arguments = arguments.replace("\\\"", "\"");
			arguments = arguments.replace("\\\\", "\\");
		}
		return arguments;
	}
}
