package hr.fer.zemris.java.hw06.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

public class MkdirShellCommand implements ShellCommand {
	
	private List<String> commandDescription;
	
	public MkdirShellCommand() {
		commandDescription = new ArrayList<>();
		commandDescription.add("Command mkdir takes a single argument: directory name,");
		commandDescription.add("and creates appropriate directory structure.");

		commandDescription = Collections.unmodifiableList(commandDescription);
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		arguments = this.oneArgumentParse(arguments);
		
		Path path = Paths.get(arguments);
		if(Files.exists(path)) {
			System.out.println("File already exists!");
			return ShellStatus.CONTINUE;
		}
		
		try {
			Files.createDirectories(path);
		} catch (IOException e) {
			System.out.println("Directory couldn't be made!");
			return ShellStatus.CONTINUE;
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "mkdir";
	}

	@Override
	public List<String> getCommandDescription() {
		return commandDescription;
	}

}
