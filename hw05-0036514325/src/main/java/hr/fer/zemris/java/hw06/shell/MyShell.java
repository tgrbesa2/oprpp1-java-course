package hr.fer.zemris.java.hw06.shell;

import java.util.Collections;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.zemris.java.hw06.shell.commands.*;


/**
 * Myshell is command-line program with built-in commands:
 * charsets, cat, ls, tree, copy, mkdir, hexdump.
 * @author tgrbesa
 *
 */
public class MyShell {
	
	static Environment env = new Environment(){
		private Character promptSymbol = '>';
		private Character morelineSymbol = '\\';
		private Character multilineSymbol = '|';
		private SortedMap<String, ShellCommand> shellCommands = null;
		private Scanner sc = new Scanner(System.in);
		
		@Override
		public String readLine() throws ShellIOException {
			System.out.print(this.getPromptSymbol());
			String s;
			try {
				s = sc.nextLine();
				while(s.lastIndexOf(this.getMorelinesSymbol()) == s.length() - 1) {
					System.out.print(this.getMultilineSymbol());
					s = s.substring(0, s.length() -1);
					s += sc.nextLine();
				}
				return s;
			} catch (Exception ex) {
				throw new ShellIOException();
			}	
		}

		@Override
		public void write(String text) throws ShellIOException {
			try {
				System.out.print(text);
			} catch (Exception ex) {
				throw new ShellIOException();
			}
			
		}

		@Override
		public void writeln(String text) throws ShellIOException {
			try {
				System.out.println(text);
			} catch (Exception ex) {
				throw new ShellIOException();
			}
		} 

		@Override
		public SortedMap<String, ShellCommand> commands() {
			if(shellCommands == null) {
				shellCommands = new TreeMap<String, ShellCommand>();
				shellCommands.put("cat", new CatShellCommand());
				shellCommands.put("charset", new CharsetShellCommand());
				shellCommands.put("copy", new CopyShellCommand());
				shellCommands.put("hexdump", new HexdumpShellCommand());
				shellCommands.put("ls", new LsShellCommand());
				shellCommands.put("mkdir", new MkdirShellCommand());
				shellCommands.put("tree", new TreeShellCommand());
				shellCommands.put("symbol", new SettingsShellCommand());
				shellCommands.put("help", new HelpShellCommand());
				shellCommands.put("exit", new ExitShellCommand());
				
				shellCommands = Collections.unmodifiableSortedMap(shellCommands);
			}
			
			return shellCommands;
		}

		@Override
		public Character getMultilineSymbol() {
			return multilineSymbol;
		}

		@Override
		public void setMultilineSymbol(Character symbol) {
			this.multilineSymbol = symbol;
		}

		@Override
		public Character getPromptSymbol() {
			return this.promptSymbol;
		}

		@Override
		public void setPromptSymbol(Character symbol) {
			this.promptSymbol = symbol;
		}

		@Override
		public Character getMorelinesSymbol() {
			return this.morelineSymbol;
		}

		@Override
		public void setMorelinesSymbol(Character symbol) {
			this.morelineSymbol = symbol;
			
		}
			
	};
	public static void main(String[] args) {
		System.out.println("Welcome to MyShell v 1.0");
		ShellStatus status = ShellStatus.CONTINUE;
		
		do {
			String l = env.readLine();
			String commandName;
			String arguments;
			
			if(l.contains(" ")) {
				commandName = l.substring(0, l.indexOf(' '));
				arguments = l.substring(l.indexOf(' ') + 1);
			} else {
				commandName = l;
				arguments = "";
			}
			
			ShellCommand sc = env.commands().get(commandName);
			if(sc == null) {
				env.writeln("Unknown command. Please try again!");
				continue;
			}
			status = sc.executeCommand(env, arguments);
		} while (status != ShellStatus.TERMINATE);
		
		
		System.out.println("Goodbye :)");
	}
	
	

}
