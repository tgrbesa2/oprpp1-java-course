package hr.fer.zemris.java.hw06.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw06.shell.Environment;
import hr.fer.zemris.java.hw06.shell.ShellCommand;
import hr.fer.zemris.java.hw06.shell.ShellStatus;

public class SettingsShellCommand implements ShellCommand {
	private List<String> commandDescription;
	
	public SettingsShellCommand() {
		commandDescription = new ArrayList<>();
		commandDescription.add("Command symbol takes one single argument: symbol name,");
		commandDescription.add("and writes that symbol in shell. It can take one extra");
		commandDescription.add("argument: symbol(one character), and it will change");
		commandDescription.add("symbol into given character.");

		commandDescription = Collections.unmodifiableList(commandDescription);
	}
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String symbolName;
		Character symbol = '|';
		Character helper;
		boolean flag = false;
		
		if(arguments.contains(" ")) {
			symbolName = arguments.split(" ")[0];
			if(arguments.split(" ")[1].length() != 1) {
				env.writeln("Symbol must be character, not string or null!");
				return ShellStatus.CONTINUE;
			}
			symbol = arguments.split(" ")[1].charAt(0);
			flag = true;
		} else {
			symbolName = arguments;
		}
		
		switch(symbolName) { 
		case "PROMPT":
			helper = env.getPromptSymbol();
			if(flag) {
				env.setPromptSymbol(symbol);
			}
			break;
		case "MORELINES":
			helper = env.getMorelinesSymbol();
			if(flag) {
				env.setMorelinesSymbol(symbol);
			}
			break;
		case "MULTILINE":
			helper = env.getMultilineSymbol();
			if(flag) {
				env.setMultilineSymbol(symbol);
			}
			break;
		default:
			env.writeln("Unknown symbol, please try again");
			return ShellStatus.CONTINUE;
		}
		
		if(flag) {
			env.writeln("Symbol for " + symbolName + " changed from '" + helper + "' to '" + symbol + "'");
		} else {
			env.writeln("Symbol for " + symbolName + " is '" + helper  + "'");
		}

		return ShellStatus.CONTINUE;	
		
		
	}

	@Override
	public String getCommandName() {
		return "symbol";
	}

	@Override
	public List<String> getCommandDescription() {
		return commandDescription;
	}

}
