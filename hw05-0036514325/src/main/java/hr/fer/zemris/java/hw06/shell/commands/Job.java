package hr.fer.zemris.java.hw06.shell.commands;

import java.io.File;

/**
 * Interface used for TreeShellCommand as part of design pattern strategy.
 * @author tgrbesa
 *
 */
public interface Job {
	void findFile(File staza);
	
	void enteringDirectory(File staza);

	void exitingDirectory(File staza);

}
