package hr.fer.oprpp1.hw05.crypto;

/**
 * Class helper that has static methods for turning
 * hex data to byte and vice versa.
 * @author tgrbesa
 *
 */
public class Util {
	
	/**
	 * Method takes string written with hex code
	 * and transforms it in array of bytes.
	 * @param keytext
	 * @return
	 */
	public static byte[] hextobyte(String keytext) {
		if(keytext.length() % 2 != 0) throw new IllegalArgumentException("Argument mustn't be odd-sized!");
		if(keytext.length() == 0) return new byte[0];
		
		keytext = keytext.toUpperCase();
		if(!keytext.matches("[0-9A-F]+")) {
			throw new IllegalArgumentException("Input must have only hex chars!");
		}
		
		byte[] val = new byte[keytext.length() / 2];
		
		
		
		for(int i = 0; i < val.length; i++) {
			int index = i * 2;
			char first = keytext.charAt(index);
			char second = keytext.charAt(index + 1);
			
			val[i] = (byte) ((Character.digit(first, 16) << 4) + Character.digit(second, 16));
		}
		return val;
	}
	
	/**
	 * Method takes byte array and turns it
	 * into hex coded string.
	 * @param bytearray
	 * @return
	 */
	public static String bytetohex(byte[] bytearray) {
		StringBuilder sb = new StringBuilder();
		for(byte b : bytearray) {
			sb.append(String.format("%02X", b));
		}
		return sb.toString().toLowerCase();
	}
}
