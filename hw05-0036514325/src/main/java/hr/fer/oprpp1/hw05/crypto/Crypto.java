package hr.fer.oprpp1.hw05.crypto;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Class crypto allows user to encrypt/decrypt
 * given file using AES crypto-algorithm
 * and to calculate and check SHA-256 digest.
 * @author tgrbesa
 *
 */
public class Crypto {
	public static void main(String[] args) throws NoSuchAlgorithmException {
		if(!(args.length == 2 || args.length == 3)) {
			throw new IllegalArgumentException("Number of argument must be 2 or 3!");
		}
		Scanner sc = new Scanner(System.in);
		
		switch(args[0]) {
			case "checksha":
				System.out.println("Please provide expected sha-256 digest for " + args[1] + ":");
				System.out.printf("%s", "> ");
				
				String tmp1 = sc.nextLine();
				boolean result = checkSHA(Paths.get(args[1]), tmp1);
				
				if(result) {
					System.out.println("Digesting completed. Digest of hw05test.bin matches expected digest.");
				} else {
					byte[] hash = calculateSHA(Paths.get(args[1]));
					System.out.println("Digesting completed. Digest of hw05test.bin does not match the expected digest.");
					System.out.println("Digest was: " + Util.bytetohex(hash));
					
				}
				break;
				
			case "calculatesha":
				byte[] hash = calculateSHA(Paths.get(args[1]));
				System.out.println("Digesting completed. Digest of hw05test.bin is: \n" + Util.bytetohex(hash));
				break;
				
			case "encrypt":				
			case "decrypt":
				System.out.println("Please provide password as hex-encoded text (16 bytes, i.e 32 hex-digits):");
				System.out.printf("%s", "> ");
				String tmp4 = sc.nextLine();
				
				System.out.println("Please provide initialization vector as hex-encoded text (32 hex-digits):");
				System.out.printf("%s", "> ");
				String tmp5 = sc.nextLine();
				
				boolean func = args[0].equals("encrypt");
				
				try {
					crypt(tmp4, tmp5, Paths.get(args[1]), Paths.get(args[2]), func);
					System.out.printf("%s%s%s%s%s", args[0], "ion completed. Generated file ", args[2], " based on file ", args[1]);
				} catch (Exception ex) {
					System.out.println("Something went wrong. Please try again!");
					System.exit(-1);
				}
				break;
			default:
				sc.close();
				throw new IllegalArgumentException("Wrong argument has been inserted!");
		}
		
		
		sc.close();
		
	}
	
	/**
	 * Static method that compares two check sums.
	 * Return boolean value depending
	 * if check sums are equal.
	 * @param p path of file to check SHA.
	 * @param argumentSHA Digest to compare with.
	 * @throws NoSuchAlgorithmException thrown if instance of
	 * message digester is not called properly.
	 */
	public static boolean checkSHA(Path p, String argumentSHA) throws NoSuchAlgorithmException{
		byte[] hash = calculateSHA(p);
		
		int result = argumentSHA.compareTo(Util.bytetohex(hash));
		
		return result == 0;	
	}
	
	/**
	 * Static method that calculates SHA-256 digest.
	 * Returns digest as byte array.
	 * @param p Path of file to calculate SHA-256 digest.
	 * @throws NoSuchAlgorithmException thrown if instance of
	 * message digester is not called properly.
	 */
	public static byte[] calculateSHA(Path p) throws NoSuchAlgorithmException {
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		InputStream is = null;
		
		try {
			is = Files.newInputStream(p);
			byte[] buff = new byte[4096];
			while(true) {
				int r = is.read(buff);
				if(r<1) break;
				sha.update(buff, 0, r);
			}
		} catch(IOException ex) {
			System.out.println("InputStream error ocurred!");
			System.exit(-1);
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return sha.digest();
	}
	
	
	/**
	 * Method encrypts file given in <code>p</code>
	 * using given password and initialization vector.
	 * @param keyText
	 * @param ivText
	 * @param inputPath
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public static void crypt(String keyText, String ivText, Path inputPath, Path outputPath, boolean encrypt) throws IllegalBlockSizeException, BadPaddingException {
		Cipher aesCipher = null;
		
		try {
			aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Wrong algorithm used!");
			System.exit(-1);
		} catch (NoSuchPaddingException e) {
			System.out.println("Wrong padding used!");
			System.exit(-1);
		}
		
		SecretKeySpec keySpec = new SecretKeySpec(Util.hextobyte(keyText), "AES");
		AlgorithmParameterSpec paramSpec = new IvParameterSpec(Util.hextobyte(ivText));
		
		try {
			aesCipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);
		} catch (InvalidKeyException e) {
			System.out.println("Wrong key used!");
			System.exit(-1);
		} catch (InvalidAlgorithmParameterException e) {
			System.out.println("Wrong alrorithm used!");
			System.exit(-1);
		} 
		
		try (InputStream is = Files.newInputStream(inputPath); OutputStream os = Files.newOutputStream(outputPath)){
			byte[] buff = new byte[4096];
			
			while(true) {
				int r = is.read(buff);
				if(r < 1) break;
				os.write(aesCipher.update(buff, 0, r));
			}
			
			os.write(aesCipher.doFinal());
		} catch (IOException ex) {
			System.out.println("InputStream error ocurred!");
			System.exit(-1);
		} 
			
	}
}
