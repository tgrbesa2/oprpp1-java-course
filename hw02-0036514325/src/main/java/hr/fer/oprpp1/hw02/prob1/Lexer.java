package hr.fer.oprpp1.hw02.prob1;

/**
 * Class represents lexical analyzer.
 * @author tgrbesa
 *
 */
public class Lexer {
	
	/**
	 * Entry text stored in array of chars.
	 */
	private char[] data;
	
	/**
	 * Current token.
	 */
	private Token token;
	
	/**
	 * Index of first unimproved char.
	 */
	private int currentIndex;
	
	/**
	 * Current state of Lexer.
	 */
	private LexerState state;
	
	/**
	 * Constructor that transforms String <code>text</code> into array of chars.
	 * @param text
	 */
	public Lexer(String text) {
		if(text == null) throw new NullPointerException("Input string must be non-null reference!");
		text = text.replaceAll("\r", "");
		text = text.replaceAll("\n", "");
		text = text.replaceAll("\t", "");
		state = LexerState.BASIC;
		
		data = text.toCharArray();
		currentIndex = 0;
	}
	
	/**
	 * Generates and returns next token.
	 * @return
	 * @throws LexerException
	 */
	public Token nextToken() {
		if(currentIndex > data.length) throw new LexerException("All tokens already generated");
		String currentToken = "";
		TokenType currentType = null;
		
		while(state == LexerState.BASIC) {
			//End of input string
			if(currentIndex == data.length) {
				if(currentType != null) {
					if(currentType == TokenType.NUMBER) {
						long tmp = helpParseLong(currentToken);
						token = new Token(currentType, tmp);
					} else {
						token = new Token(currentType, currentToken);
					}
					break;
				}
				token = new Token(TokenType.EOF, null);
				currentIndex++;
				break;
			}
			//Escape blank spaces
			if(currentToken.isEmpty() && data[currentIndex] == ' ') {
				currentIndex++;
				continue;
			}
			//Current token is not empty, and next char is blank space.
			if(data[currentIndex] == ' ') {
				currentIndex++;
				if(currentType == TokenType.NUMBER) {
					long tmp = helpParseLong(currentToken);
					token = new Token(currentType, tmp);
				} else {
					token = new Token(currentType, currentToken);
				}
				break;
			}
			//Solver for escape chars
			if(data[currentIndex] == '\\') {
				if(currentIndex == data.length - 1) throw new LexerException();
				if(Character.isLetter(data[currentIndex + 1])) throw new LexerException("Cannot escape letters");
				
				if(currentType == TokenType.NUMBER) {
					long tmp = helpParseLong(currentToken);
					token = new Token(currentType, tmp);
					break;
				} else {
					currentIndex++;
					currentType = TokenType.WORD;
					currentToken += data[currentIndex++];
					continue;
				}
			}
			
			//Rest is regular flow of turning strings into tokens
			if(Character.isLetter(data[currentIndex])) {
				if(!currentToken.isEmpty()) {
					if(currentType == TokenType.NUMBER) {
						long tmp = helpParseLong(currentToken);
						token = new Token(currentType, tmp);
						break;
					}
				}
				currentType = TokenType.WORD;
				currentToken += data[currentIndex++];
				continue;
			}
			
			if(Character.isDigit(data[currentIndex])) {
				if(!currentToken.isEmpty()) {
					if(currentType != TokenType.NUMBER) {
						token = new Token(currentType, currentToken);
						break;
					}
				}
				currentType = TokenType.NUMBER;
				currentToken += data[currentIndex++];
				continue;
			}
			
			if(currentToken.isEmpty()) {			
				token = new Token(TokenType.SYMBOL, data[currentIndex++]);
			} else {
				if(currentType == TokenType.NUMBER) {
					long tmp = helpParseLong(currentToken);
					token = new Token(currentType, tmp);
				} else {
					token = new Token(currentType, currentToken);
				}
			}
			break;
		}
		
		while(state == LexerState.EXTENDED) {
			if(currentIndex == data.length) {
				if(currentType != null) {
					token = new Token(TokenType.WORD, currentToken);
					break;
				}
				token = new Token(TokenType.EOF, null);
				currentIndex++;
				break;
			}
			if(currentToken.isEmpty() && data[currentIndex] == ' ') {
				currentIndex++;
				continue;
			}
			if(data[currentIndex] == ' ') {
				currentIndex++;
				token = new Token(TokenType.WORD, currentToken);
				break;
			}
			
			if(data[currentIndex] == '#') {
				if(currentToken.isEmpty()) {
					currentType = TokenType.SYMBOL;
					token = new Token(currentType, data[currentIndex++]);
				} else {
					token = new Token(TokenType.WORD, currentToken);
				}
				break;
			}
			
			currentToken += data[currentIndex++];
			
		}
		
		if(currentType == TokenType.SYMBOL) {
			if(data[currentIndex - 1] == '#') {
				if(state == LexerState.BASIC) {
					this.setState(LexerState.EXTENDED);
				} else {
					this.setState(LexerState.BASIC);
				}
			}
		}
		
		return token;
	}
	
	/**
	 * Returns last generated token.
	 * Can be called multiple times.
	 * Doesn't generate next token.
	 * @return
	 */
	public Token getToken() {
		return token;
	}
	
	/**
	 * Helper function for parsing long.
	 * @param s
	 * @return
	 */
	private long helpParseLong(String s) {
		long tmp;
		try {
			tmp = Long.parseLong(s);
		} catch (Exception ex) {
			throw new LexerException("Current token can't be parsed");
		}
		
		return tmp;
	}
	
	/**
	 * Changes current state of lexer.
	 * @param state
	 */
	public void setState(LexerState state) {
		if(state == null) throw new NullPointerException("State must be non-null reference!");
		this.state = state;
	}
	
	
}
