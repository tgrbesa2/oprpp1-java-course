package hr.fer.oprpp1.hw02.prob1;

/**
 * Class represent exceptions connected with class Lexer.
 * @author tgrbesa
 *
 */
public class LexerException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LexerException() {
		super();
	}
	
	public LexerException(String message) {
		super(message);
	}
}
