package hr.fer.oprpp1.hw02;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.oprpp1.custom.scripting.nodes.DocumentNode;
import hr.fer.oprpp1.custom.scripting.parser.SmartScriptParser;

public class SmartScriptTester {

	public static void main(String[] args) throws IOException {
		String docBody = new String(
				Files.readAllBytes(Paths.get("src/test/resources/doc1.txt")),
				StandardCharsets.UTF_8);
		
		SmartScriptParser  parser = new SmartScriptParser(docBody);
		DocumentNode document = parser.getDocument();
		String originalDocumentBody = document.toString();
		
		SmartScriptParser parser2 = new SmartScriptParser(originalDocumentBody);
		DocumentNode document2 = parser2.getDocument();
		boolean same = document.equals(document2);
		
		System.out.println(same);
	}

}
