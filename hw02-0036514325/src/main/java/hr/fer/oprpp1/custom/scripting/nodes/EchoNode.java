package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.scripting.elems.Element;

/**
 * Inherited from base Node class.
 * Represents variable/string for print output.
 * @author tgrbesa
 *
 */
public class EchoNode extends Node {
	private Element[] elements = null;
	
	public EchoNode(Element[] elements) {
		this.elements = elements;
	}
	
	public Element[] getElements() {
		return elements;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{$=");
		for(int i = 0; i < elements.length; i++) {
			sb.append(" " + elements[i].asText());
		}
		sb.append("$}");
		return sb.toString();
	}
}
