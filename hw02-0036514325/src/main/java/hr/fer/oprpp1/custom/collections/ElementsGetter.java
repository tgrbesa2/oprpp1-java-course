package hr.fer.oprpp1.custom.collections;

import java.util.ConcurrentModificationException;

/**
 * Interface which can return elements of collection on demand.
 * Classes that implement this interface have to implement
 * two methods: getNextElement() and hasNextElement()
 *
 * @author tgrbesa
 *
 */
public interface ElementsGetter {
	
	/**
	 * Method returns if this object(iterator) has element to return.
	 * @return True if this object(iterator) has not returned all elements of collection, False otherwise.
	 * @throws NoSuchElementException If this object(iterator) already returned all elements from collection.
	 * @throws ConcurrentModificationException If collection has been structuraly changed.
	 */
	boolean hasNextElement();
	
	/**
	 * Method returns next element from collection and changes index
	 * to next element.
	 * @return Object that is next element.
	 * @throws NoSuchElementException If this object(iterator) already returned all elements from collection.
	 * @throws ConcurrentModificationException If collection has been structuraly changed.
	 */
	Object getNextElement();
	
	default void processRemaining(Processor p) {
		while(this.hasNextElement()) {
			p.process(this.getNextElement());
		}
	}
}
