package hr.fer.oprpp1.custom.scripting.lexer;

public class SmartToken {
	private SmartTokenType type;
	private Object value;
	
	public SmartToken(SmartTokenType type, Object value) {
		this.type = type;
		this.value = value;
	}
	
	public Object getValue() {
		return value;
	}
	
	public SmartTokenType getType() {
		return type;
	}
}
