package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.scripting.elems.ElementString;

/**
 * Inherited from base Node class.
 * Class represents a piece of textual data.
 * @author tgrbesa
 *
 */
public class TextNode extends Node {
	/**
	 * Read-only text.
	 */
	private ElementString text;
	
	public TextNode(String text) {
		this.text = new ElementString(text);
	}
	
	public String getText() {
		return text.getValue();
	}
	
	@Override
	public String toString() {
		return text.asText();
	}
}
