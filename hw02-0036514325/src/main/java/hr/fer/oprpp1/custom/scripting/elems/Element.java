package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Base class of elements
 * @author tgrbesa
 *
 */
public class Element {
	/**
	 * Return element parsed into string.
	 * @return
	 */
	public String asText() {
		return "";
	}
}
