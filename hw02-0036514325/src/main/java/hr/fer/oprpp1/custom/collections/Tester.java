package hr.fer.oprpp1.custom.collections;

/**
 * Classes that implement this interface create instances
 * that get some object and determine if that object is acceptable or not.
 * @author tgrbesa
 *
 */
public interface Tester {
	
	/**
	 * Determine if <code>obj</code> is acceptable.
	 * @param obj Object that is tested.
	 * @return True if <code>obj</code> is accpetable, false otherwise.
	 */
	boolean test(Object obj);
}
