package hr.fer.oprpp1.custom.collections;

public interface List<T> extends Collection<T> {
	
	/**
	 * Returns element at specified <code>index</code>
	 */
	T get(int index);
	
	/**
	 * Inserts Object <code>value</code> at specified <code>position</code>
	 * @param value
	 * @param position
	 */
	void insert(Object value, int position);
	
	/**
	 * Returns index of Object <code>value</code> in this List.
	 * @param value 
	 * @return Index of Object <code>value</code>, -1 otherwise.
	 */
	int indexOf(Object value);
	
	/**
	 * Removes element at specified index.
	 * @param index
	 */
	void remove(int index);

}
