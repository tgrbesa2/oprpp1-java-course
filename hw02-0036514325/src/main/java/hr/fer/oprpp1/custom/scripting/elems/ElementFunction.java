package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Inherited class from base Element class.
 * Stores function name as string.
 * @author tgrbesa
 *
 */
public class ElementFunction extends Element {
	private String name;
	
	public ElementFunction(String name) {
		this.name = name;
	}
	
	@Override
	public String asText() {
		return name;
	}

	public String getName() {
		return name;
	}
	
}
