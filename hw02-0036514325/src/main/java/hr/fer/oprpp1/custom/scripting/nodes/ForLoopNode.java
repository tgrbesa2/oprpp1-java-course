package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.scripting.elems.*;
/**
 * Inherited from base Node class.
 * Class represents for loop.
 * @author tgrbesa
 *
 */
public class ForLoopNode extends Node {
	private ElementVariable variable;
	private Element startExpression;
	private Element endExpression;
	private Element stepExpression;
	
	
	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression,
			Element stepExpression) {
		super();
		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
		this.stepExpression = stepExpression;
	}


	public ElementVariable getVariable() {
		return variable;
	}


	public Element getStartExpression() {
		return startExpression;
	}


	public Element getEndExpression() {
		return endExpression;
	}


	public Element getStepExpression() {
		return stepExpression;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{$FOR");
		sb.append(variable.asText());sb.append(" " + startExpression.asText());
		sb.append(" " + endExpression.asText());
		if(stepExpression != null) {
			sb.append(" "+ stepExpression.asText());
		}
		sb.append("$}");
		for(int i = 0; i < this.numberOfChildren(); i++) {
			sb.append(this.getChild(i).toString());
		}
		sb.append("{$END$}");
	
		return sb.toString();
	}
}
