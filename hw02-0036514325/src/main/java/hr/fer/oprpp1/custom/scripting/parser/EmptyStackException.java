package hr.fer.oprpp1.custom.scripting.parser;

/**
 * 
 * @author tgrbesa
 *
 */
public class EmptyStackException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Empty constructor.
	 */
	public EmptyStackException() {	
	}
	
	/**
	 * Constructor with message sent when exception is thrown.
	 * @param message Message to show.
	 */
	public EmptyStackException(String message) {
		super(message);
	}
	
}
