package hr.fer.oprpp1.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

/**
 * Collection of objects stored in linked list.
 * @author tgrbesa
 *
 */
public class LinkedListIndexedCollection implements List{
	
	/**
	 * Private class representing LinkedList nodes.
	 * @author tgrbesa
	 *
	 */
	private static class ListNode {
		private ListNode previous = null;
		private ListNode next = null;
		private Object element = null;
	}
	
	/**
	 * Current size of the collection.
	 */
	private int size;
	
	/**
	 * Reference to the first node of the linked list.
	 */
	private ListNode first;
	
	/**
	 * Reference to the last node of the linked list.
	 */
	private ListNode last;
	
	/**
	 * Long variable that counts collection modifications.
	 * When structural modification occurs, <code>modificationCount</code>
	 * increments with 1.
	 */
	private long modificationCount = 0;
	
	/**
	 * Default constructor which creates an empty collection.
	 */
	public LinkedListIndexedCollection() {
		this.first = null;
		this.last = null;
		this.size = 0;
	}
	/**
	 * Copy constructor which copies elements of some <code>other</code> collection into
	 * this newly constructed.
	 * @param other Elements from other collection that are copied into this collection.
	 */
	public LinkedListIndexedCollection(Collection other) {
		this.first = this.last = null;
		
		this.addAll(other);
	}
	
	@Override
	/**
	 * 
	 * @throws NullPointerException If <code>value</code> is null.
	 */
	public void add(Object value) {
		if(value == null) throw new NullPointerException("Value must be non-null reference");
		
		this.size++;
		ListNode ln = new ListNode();
		ln.element = value;
		
		if(this.first == null && this.last == null)  {
			this.first = this.last = ln;
		} else {
			this.last.next = ln;
			ln.previous = this.last;
			this.last = ln;
		}
		modificationCount++;
	}
	
	/**
	 * Returns the object at position <code>index</code>.
	 * Valid index is between 0 and (size - 1).
	 * @param index 
	 * @return Object stored in linked list at poisiton <code>index</code>
	 * @throws IndexOutOfBoundsException If <code>index</code> is invalid.
	 */
	public Object get(int index) {
		if(index < 0 || index > this.size - 1) throw new IndexOutOfBoundsException("Argument must be between 0 and (size - 1)");
		
		ListNode tmp = null;
		
		if(index > this.size / 2) {
			tmp = this.last;
			for(int i = this.size - 1; i > index; i--) {
				tmp = tmp.previous;
			}
		} else {
			tmp = this.first;
			for(int i = 0; i < index; i++) {
				tmp = tmp.next;
			}
		}
		
		return tmp.element;
	}
	
	@Override
	public void clear() {
		this.first = this.last = null;
		this.size = 0;
		modificationCount++;
	}
	
	/**
	 * Method inserts the given <code>value</code> at the given <code>position</code> in linked-list.
	 * Average complexity is O(n / 2).
	 * @param value 
	 * @param position
	 * @throws IndexOutOfBoundsException If position is not between 0 and size.
	 * @throws NullPointerException If value is null.
	 */
	public void insert(Object value, int position) {
		if(value == null) throw new NullPointerException("Value must be non-null reference");
		if(position < 0 || position > this.size) throw new IndexOutOfBoundsException("Position must be between 0 and size");
		
		this.size++;
		ListNode ln = new ListNode();
		ln.element = value;
		
		if(position == 0) {
			ln.next = this.first;
			this.first = ln;
			
		} else if(position == this.size) {
			ln.previous = this.last;
			this.last = ln;
			
		} else {		
			ListNode tmp = this.first;
		
			for(int i = 0; i < position - 1; i++) {
				tmp = tmp.next;
			}
			
			ln.next = tmp.next;
			ln.previous = tmp;
			tmp.next.previous = ln;
			tmp.next = ln;
		}
		modificationCount++;
	}
	
	/**
	 * Searches the collection and returns the index of the first occurence of the given <code>value</code>
	 * Average complexity is O(n/2).
	 * @param value Value which is searched for in linked-list.
	 * @return index of first occurence of the given <code>value</code>
	 */
	public int indexOf(Object value) {
		int counter = 0;
		for(ListNode ln = this.first; ln != null; ln = ln.next) {
			if(ln.element.equals(value)) return counter;
			counter++;
		}
		
		return -1;
	}
	
	/**
	 * Removes element at the specified <code>index</code> from collection.
	 * Valid <code>index</code> is between 0 and (size - 1).
	 * @param index Position of element to be removed.
	 * @throws IndexOutOfBoundsException If <code>index</code> is invalid.
	 */
	public void remove(int index) {
		if(index < 0 || index > size - 1) throw new IndexOutOfBoundsException("Argument index must be between 0 and (size - 1)");
		
		ListNode tmp = first;
		for(int i = 0; i < index; i++) {
			tmp = tmp.next;
		}
		if(tmp == this.first && tmp == this.last) {
			this.first = null;
			this.last = null;
		}
		if(tmp != this.first && tmp != this.last) {
			tmp.previous.next = tmp.next;
			tmp.next.previous = tmp.previous;
		}
		if(tmp == this.first) {
			this.first = tmp.next;
			tmp.next.previous = null;
		} 
		if(tmp == this.last) {
			this.last = tmp.previous;
			tmp.previous.next = null;
		}

		this.size--;
		tmp = null;
		
		modificationCount++;
	}
	
	@Override
	public int size() {
		return this.size;
	}
	
	@Override
	public boolean contains(Object value) {
		return this.indexOf(value) != -1;
 	}
	
	@Override
	public Object[] toArray() {
		Object[] toReturn = new Object[this.size];
		
		ListNode ln = this.first;
		for(int i = 0; i < this.size; i++) {
			toReturn[i] = ln.element;
			ln = ln.next;
		}
		
		return toReturn;
	}
	
	
	@Override
	public boolean remove(Object value) {
		int idx = this.indexOf(value);
		if(idx == -1) return false;
		
		this.remove(idx);
		modificationCount++;
		
		return true;
		
	}
	@Override
	public ElementsGetter createElementsGetter() {
		return new ElementsGetterLinkedList(first, this);
	}
	
	private static class ElementsGetterLinkedList implements ElementsGetter {
		private ListNode ln;
		private long savedModificationCount;
		private LinkedListIndexedCollection internalCollection;
		
		public ElementsGetterLinkedList(ListNode ln, LinkedListIndexedCollection collection) {
			this.ln = ln;
			this.internalCollection = collection;
			this.savedModificationCount = collection.modificationCount;
		}
		@Override
		public boolean hasNextElement() {
			if(savedModificationCount != internalCollection.modificationCount) {
				throw new ConcurrentModificationException("Internal collection has been structuraly changed!");
			}
			
			
			return ln != null;
		}

		@Override
		public Object getNextElement() {
			if(savedModificationCount != internalCollection.modificationCount) {
				throw new ConcurrentModificationException("Internal collection has been structuraly changed!");
			}
			
			if(ln == null) throw new NoSuchElementException();
			
			Object tmp = ln.element;
			this.ln = ln.next;
			return tmp;
		}
		
	}
}
