package hr.fer.oprpp1.custom.scripting.nodes;

/**
 * Inherited from base Node class.
 * Node at top of tree.
 * @author tgrbesa
 *
 */
public class DocumentNode extends Node {
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < this.numberOfChildren(); i++) {
			sb.append(this.getChild(i).toString());
		}
		
		return sb.toString();
	}
	
	public boolean equals(DocumentNode other) {
		return this.toString().equals(other.toString());
	}

}
