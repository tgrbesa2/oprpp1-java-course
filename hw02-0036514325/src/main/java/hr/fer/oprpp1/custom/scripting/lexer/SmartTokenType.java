package hr.fer.oprpp1.custom.scripting.lexer;

public enum SmartTokenType {
	EOF,
	BEGIN_TAG,
	CLOSE_TAG,
	KEYWORD,
	FUNCTION,
	STRING,
	TEXT,
	OPERATOR,
	CONSTANT,
	IDENT
}
