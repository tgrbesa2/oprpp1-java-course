package hr.fer.oprpp1.custom.collections;

/**
 * Class represents general collection of objects
 * @author tgrbesa
 *
 */
public interface Collection<T> {
	
	/**
	 * Checks if collection is empty.
	 * @return True if collection contains no object, false otherwise.
	 */
	default boolean isEmpty() {
		return this.size() == 0;
	}
	
	/**
	 * Returns the number of currently stored objects in this collection.
	 * @return Size of the collection.
	 */
	int size();
	
	/**
	 *  Adds the given object into this colelction.
	 * @param value Object to be added to collection.
	 */
	void add(Object value);
	
	 /**
	  * Checks if collection contains given <code>value</code>.
	  * @param value
	  * @return True if collection contains <code>value</code>, otherwise false.
	  */
	boolean contains(Object value);
	
	/**
	 * Removes one occurence of given <code>value</code>, if the collection contains given value. 
	 * @param value Object to be removed.
	 * @return True if object is contained in collection and removed, false otherwise.
	 */
	boolean remove(Object value);
	
	/**
	 * Allocates new array with equal size of this collection, fills it with collection content and returns the array.
	 * @return Copy of this collection as array.
	 */
	Object[] toArray();
	
	/**
	 * Method calls <code>Processor.process()</code> for each element of this collection.
	 * @param processor Instance of Processor class.
	 */
	default void forEach(Processor processor) {
		ElementsGetter getter = this.createElementsGetter();
		while(getter.hasNextElement()) {
			processor.process(getter.getNextElement());
		}
	}
	
	/**
	 * 
	 * @param other
	 */
	default void addAll(Collection other) {
		/**
		 * This class extends Processor class and is used for adding values in this collection.
		 * @author tgrbesa
		 *
		 */
		class addAllProcessor implements Processor {
			
			@Override
			public void process(Object value) {
				add(value);
			}
		}
		other.forEach(new addAllProcessor());
	}
	
	/**
	 * Removes all elements from this collection.
	 */
	void clear();
	
	/**
	 * Creates object that iterates over collection on demand.
	 */
	ElementsGetter createElementsGetter();
	
	/**
	 * Method adds all elements from collection <code>col</code>
	 * that <code>tester</code> accepts.
	 * @param col
	 * @param tester
	 */
	default void addAllSatisfying(Collection col, Tester tester) {
		ElementsGetter getter = col.createElementsGetter();
		
		while(getter.hasNextElement()) {
			Object tmp = getter.getNextElement();
			if(tester.test(tmp)) this.add(tmp);
		}
	}
} 
