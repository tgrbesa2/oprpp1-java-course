package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.collections.ArrayIndexedCollection;

/**
 * Base class of nodes.
 * @author tgrbesa
 *
 */
public class Node {
	/**
	 * Internally managed collection.
	 */
	ArrayIndexedCollection internalCollection;
	
	/**
	 * Add child node to current node.
	 * @param child
	 */
	public void addChildNode(Node child) {
		if(internalCollection == null) {
			internalCollection = new ArrayIndexedCollection();
		}
		internalCollection.add(child);
	}
	
	/**
	 * Return number of direct children.
	 * @return
	 */
	public int numberOfChildren() {
		return internalCollection.size();
	}
	
	/**
	 * Returns child node at <code>index</code>
	 * @param index
	 * @return
	 */
	public Node getChild(int index) {
		return (Node) internalCollection.get(index);
	}
	
	public boolean equals(Node other) {
		return false;
	}
}
