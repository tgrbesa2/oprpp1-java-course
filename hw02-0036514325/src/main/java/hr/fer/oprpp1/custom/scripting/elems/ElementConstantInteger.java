package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Inherited class from base Element class.
 * Stores element as integer.
 * @author tgrbesa
 *
 */
public class ElementConstantInteger extends Element {
	private int value;
	
	public ElementConstantInteger(int value) {
		this.value = value;
	}
	
	@Override
	public String asText() {
		return String.format("%d", value);
	}

	public int getValue() {
		return value;
	}
	
}
