package hr.fer.oprpp1.custom.scripting.parser;

import hr.fer.oprpp1.custom.scripting.lexer.*;
import hr.fer.oprpp1.custom.scripting.nodes.*;
import hr.fer.oprpp1.hw02.prob1.LexerException;
import hr.fer.oprpp1.custom.collections.ArrayIndexedCollection;
import hr.fer.oprpp1.custom.scripting.elems.*;

/**
 * Parser of language in hw02.
 * @author tgrbesa
 *
 */
public class SmartScriptParser {
	private static final int NO_FORLOOP_ARGUMENTS = 4;
	/**
	 * Lexer that parser use for lexical analysis.
	 */
	private SmartScriptLexer ssl;
	
	/**
	 * Top node of our document.
	 */
	private DocumentNode document;
	
	/**
	 * Stack helping in parser function.
	 */
	private ObjectStack stack;
	
	public SmartScriptParser(String docBody) {
		ssl = new SmartScriptLexer(docBody);
		stack = new ObjectStack();
		document = new DocumentNode();
		stack.push(document);
		try {
			this.Parse();
		} catch (LexerException ex) {
			throw new SmartScriptParserException();
		}
	}
	
	/**
	 * Private method that parses specified document.
	 */
	private void Parse() {
		SmartToken token = ssl.nextToken();
		
		while(token.getValue() != null) {
			//Case text
			if(token.getType() == SmartTokenType.TEXT) {
				Node tmp = (Node) stack.peek();
				tmp.addChildNode(new TextNode(token.getValue().toString()));
				token = ssl.nextToken();
				continue;
			}
			if(token.getType() == SmartTokenType.BEGIN_TAG) {
				token = ssl.nextToken();
				
				//Case For tag
				if("FOR".compareTo(token.getValue().toString()) == 0) {
					Node tmp = (Node) stack.peek();
					Element[] tmpArray = new Element[NO_FORLOOP_ARGUMENTS];
					token = ssl.nextToken();
					
					for(int i = 0; i < NO_FORLOOP_ARGUMENTS; i++) {
						if(i == 0) {
							if(token.getType() != SmartTokenType.IDENT) {
								throw new SmartScriptParserException();
							}
						}
						if(i == 3) {
							if(token.getType() == SmartTokenType.CLOSE_TAG) break;
						}
						Element tmp2;
						switch(token.getType()) {

							case IDENT:
								tmp2 = new ElementVariable(token.getValue().toString());
								break;
							case CONSTANT:
								if(token.getValue().toString().contains(".")) {
									tmp2 = new ElementConstantDouble(Double.parseDouble(token.getValue().toString()));	
								} else {
									tmp2 = new ElementConstantInteger(Integer.parseInt(token.getValue().toString()));	
								}
								break;
							case STRING:
								tmp2 = new ElementString(token.getValue().toString());
							default:
								throw new SmartScriptParserException();
						}
						
						tmpArray[i] = tmp2;
						token = ssl.nextToken();
 					}
					
					ForLoopNode fln = new ForLoopNode((ElementVariable)tmpArray[0], tmpArray[1], tmpArray[2], tmpArray[3]);
					tmp.addChildNode(fln);
					stack.push(fln);
					if(token.getType() == SmartTokenType.CLOSE_TAG) {
						token = ssl.nextToken();
						continue;
					} else {
						throw new SmartScriptParserException();
					}
				}
				
				//Case echo tag
				if("=".compareTo(token.getValue().toString()) == 0) {
					token = ssl.nextToken();
					Node tmp = (Node) stack.peek();
					ArrayIndexedCollection helpArray = new ArrayIndexedCollection();
					
					while(token.getType() != SmartTokenType.CLOSE_TAG) {
						switch(token.getType()) {
							case IDENT:
								helpArray.add(new ElementVariable(token.getValue().toString()));
								break;
							case STRING:
								helpArray.add(new ElementString(token.getValue().toString()));
								break;
							case OPERATOR:
								helpArray.add(new ElementOperator(token.getValue().toString()));
								break;
							case FUNCTION:
								helpArray.add(new ElementFunction(token.getValue().toString()));
								break;
							case CONSTANT:
								if(token.getValue().toString().contains(".")) {
									helpArray.add(new ElementConstantDouble(Double.parseDouble(token.getValue().toString())));
								} else {
									helpArray.add(new ElementConstantInteger(Integer.parseInt(token.getValue().toString())));
								}
								break;
							default:
								throw new SmartScriptParserException();
						}
						token = ssl.nextToken();
							
					}
					Element[] echoElements = new Element[helpArray.size()];
					for(int i = 0; i < helpArray.size(); i++) {
						echoElements[i] = (Element) helpArray.get(i);
					}
					
					
					tmp.addChildNode(new EchoNode(echoElements));
					token = ssl.nextToken();
					continue;
 				}
				
				//Case End tag
				if("END".compareTo(token.getValue().toString()) == 0) {
					stack.pop();
					token = ssl.nextToken();
					if(token.getType() == SmartTokenType.CLOSE_TAG) {
						token = ssl.nextToken();
					} else {
						throw new SmartScriptParserException();
					}
				}
				
			}
		}
		if(stack.size() != 1) throw new SmartScriptParserException();
	}
	
	/**
	 * Returns tree structure of parsed input.
	 * At top of tree is DocumentNode.
	 * @return
	 */
	public DocumentNode getDocument() {
		return document;
	}
}
