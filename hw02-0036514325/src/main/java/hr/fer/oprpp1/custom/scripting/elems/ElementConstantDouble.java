package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Inherited class from base Element class.
 * Stores element as double.
 * @author tgrbesa
 *
 */
public class ElementConstantDouble extends Element {
	private double value;
	
	public ElementConstantDouble(double value) {
		this.value = value;
	}
	
	@Override
	public String asText() {
		return String.format("%f", value);
	}

	public double getValue() {
		return value;
	}
	
}
