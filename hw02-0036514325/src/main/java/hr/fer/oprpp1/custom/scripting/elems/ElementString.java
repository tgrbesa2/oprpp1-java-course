package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Inherited class from base Element class.
 * Stores element as string.
 * @author tgrbesa
 *
 */
public class ElementString extends Element {
	private String value;
	
	public ElementString(String value) {
		this.value = value;
	}
	
	@Override
	public String asText() {
		return value;
	}

	public String getValue() {
		return value;
	}
	
}
