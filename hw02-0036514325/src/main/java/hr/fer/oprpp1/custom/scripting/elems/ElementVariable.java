package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Inherited class from base Element class.
 * Stores element variable name as string.
 * @author tgrbesa
 *
 */
public class ElementVariable extends Element {
	private String name;
	
	public ElementVariable(String name) {
		this.name = name;
	}
	
	@Override
	public String asText() {
		return this.name;
	}

	public String getName() {
		return name;
	}
	
}
