package hr.fer.oprpp1.custom.scripting.lexer;

import hr.fer.oprpp1.hw02.prob1.LexerException;

/**
 * Lexer of language in hw02.
 * @author tgrbesa
 *
 */
public class SmartScriptLexer {
	/**
	 * Entry text stored in array of chars.
	 */
	private char[] data;
	
	/**
	 * Current token.
	 */
	private SmartToken token;
	
	/**
	 * Index of first unimproved char.
	 */
	private int currentIndex;
	
	/**
	 * Current state of lexer.
	 */
	private SmartLexerState state;
	
	public SmartScriptLexer(String text) {
		if(text == null) throw new NullPointerException();
		state = SmartLexerState.BASIC;
		
		data = text.toCharArray();
		currentIndex = 0;
	}
	
	/**
	 * Generates and returns next token
	 * @return
	 * @throws LexerException
	 */
	public SmartToken nextToken() {
		if(currentIndex > data.length) throw new LexerException("All tokens are generated already");
		String currentToken = "";
		SmartTokenType currentType = null;
		
		while(state == SmartLexerState.BASIC) {
			//Reached end of file
			if(currentIndex == data.length) {
				if(currentType != null) {
					token = new SmartToken(currentType, currentToken);
					break;
				}
				token = new SmartToken(SmartTokenType.EOF, null);
				currentIndex++;
				break;
			}
			//Escaping chars case
			if(data[currentIndex] == '\\') {
				if(data[currentIndex + 1] == '\\' || data[currentIndex + 1] == '{') {
					currentToken += data[currentIndex + 1];
					currentIndex += 2;
					continue;
				} else {
					throw new LexerException("Wrongly used escape char");
				}
			}
			
			//Found tag, change state
			if(data[currentIndex] == '{' && data[currentIndex + 1] == '$') {
				if(currentToken.isEmpty()) {
					currentToken = "{$";
					token = new SmartToken(SmartTokenType.BEGIN_TAG, currentToken);
					currentIndex += 2;
				} else {
					token = new SmartToken(SmartTokenType.TEXT, currentToken);
				}
				break;
			}
			currentType = SmartTokenType.TEXT;
			currentToken += data[currentIndex++];
			
		}
		
		while(state == SmartLexerState.EXTENDED) {
			//Reached end of file
			if(currentIndex == data.length) {
				if(currentType != null) {
					token = new SmartToken(currentType, currentToken);
					break;
				}
				token = new SmartToken(SmartTokenType.EOF, null);
				currentIndex++;
				break;
			}
			//Escape empty chars
			if(currentToken.isEmpty() && data[currentIndex] == ' ') {
					currentIndex++;
					continue;
			}
			//Close tag, change state
			if(data[currentIndex] == '$' && data[currentIndex + 1] == '}') {
				if(currentToken.isEmpty()) {
					currentToken = "$}";
					token = new SmartToken(SmartTokenType.CLOSE_TAG, currentToken);
					currentIndex += 2;
				} else {
					token = new SmartToken(currentType, currentToken);
				}
				break;
			}
			
			//Include blank space if current token type is string
			if(data[currentIndex] == ' ' && currentType == SmartTokenType.STRING) {
				currentToken += data[currentIndex++];
				continue;
			}
			//Finish generating token
			if(data[currentIndex] == ' ') {
				if(!currentToken.isEmpty()) {
					token = new SmartToken(currentType, currentToken);
					break;
				}
			}

			//Echo tag keyword
			if(data[currentIndex] == '=') {
				currentIndex ++;
				token = new SmartToken(SmartTokenType.KEYWORD, '=');
				break;
			}
			//For tag ekyword
			if(data[currentIndex] == 'F' || data[currentIndex] == 'f') {
				String tmp = "F";
				tmp += data[currentIndex + 1];
				tmp += data[currentIndex + 2];
				if(tmp.toUpperCase().equals("FOR")) {
					currentToken += "FOR";
					currentIndex += 3;
					token = new SmartToken(SmartTokenType.KEYWORD, currentToken);
					break;
				}
			}
			//End tag keyword
			if(data[currentIndex] == 'e' || data[currentIndex] == 'E') {
				String tmp = "E";
				tmp += data[currentIndex + 1];
				tmp += data[currentIndex + 2];
				if(tmp.toUpperCase().equals("END")) {
					currentToken += "END";
					currentIndex += 3;
					token = new SmartToken(SmartTokenType.KEYWORD, currentToken);
					break;
				}
			}
			
			//Escaping chars in string
			if(data[currentIndex] == '\"') {
				if(currentType == SmartTokenType.STRING) {
					currentToken += data[currentIndex++];
				}
				if(!currentToken.isEmpty()) {
					token = new SmartToken(currentType, currentToken);
					break;
				} else {
					currentToken += data[currentIndex++];
					currentType = SmartTokenType.STRING;
					continue;
				}
			}
			//Escaping chars in string
			if(currentType == SmartTokenType.STRING) {
				if(data[currentIndex] == '\\') {
					if(data[currentIndex + 1] == '\"' || data[currentIndex + 1] == '\\') {
						currentToken += data[currentIndex + 1];
						currentIndex += 2;
						continue;
					}
					switch(data[currentIndex + 1]) {
						case 'n': 
							currentToken += '\n';
							currentIndex += 2;
							continue;
						case 'r': 
							currentToken += '\r';
							currentIndex += 2;
							continue;
						case 't':
							currentToken += '\t';
							currentIndex += 2;
							continue;
					}
					throw new LexerException("Wrongly used escape char1");
				}
				
				currentToken += data[currentIndex++];
				continue;
				
			}


			//Rest is general flow of lexer
			if(Character.isLetter(data[currentIndex])) {
				if(currentToken.isEmpty()) {
					currentType = SmartTokenType.IDENT;
				}
				if(currentType == SmartTokenType.CONSTANT) {
					token = new SmartToken(currentType, currentToken);
					break;
				}
				currentToken += data[currentIndex++];
				continue;
			}
			
			if(Character.isDigit(data[currentIndex])) {
				if(currentToken.isEmpty()) {
					currentType = SmartTokenType.CONSTANT;
				}
				if(currentType == SmartTokenType.STRING) {
					token = new SmartToken(currentType, currentToken);
					break;
				}
				currentToken += data[currentIndex++];
				continue;
			}
			if(data[currentIndex] == '.') {
				if(Character.isDigit(data[currentIndex - 1]) && Character.isDigit(data[currentIndex + 1])) {
					currentToken += data[currentIndex++];
					continue;
				} else {
					throw new LexerException(". is wrongly used");
				}
			}
			if(data[currentIndex] == '-') {
				if(Character.isDigit(data[currentIndex + 1])) {
					if(!currentToken.isEmpty()) {
						token = new SmartToken(currentType, currentToken);
						break;
					}
					currentType = SmartTokenType.CONSTANT;
					currentToken += data[currentIndex++];
					continue;
				}
			}
			if(data[currentIndex] == '@') {
				if(Character.isLetter(data[currentIndex + 1])) {
					if(!currentToken.isEmpty()) {
						token = new SmartToken(currentType, currentToken);
						break;
					}
					currentType = SmartTokenType.FUNCTION;
					currentToken += data[currentIndex++];
					continue;
				}
			}
			if(data[currentIndex] == '_') {
				if(currentToken.isEmpty() || currentType == SmartTokenType.CONSTANT) {
					throw new LexerException();
				}
				currentToken += data[currentIndex++];
				continue;
			}
			
			switch(data[currentIndex]) {
				case '+':
				case '-':
				case '*':
				case '/':
				case '^':
					if(!currentToken.isEmpty()) {
						return new SmartToken(currentType, currentToken);
					} else {
						return new SmartToken(SmartTokenType.OPERATOR, data[currentIndex++]);
					}
			}
			throw new LexerException();
			
		}
		
		
		if(token.getType() == SmartTokenType.BEGIN_TAG) {
			state = SmartLexerState.EXTENDED;
		}
		
		if(token.getType() == SmartTokenType.CLOSE_TAG) {
			state = SmartLexerState.BASIC;
		}
		return token;
	}
	
	/**
	 * Returns last generated token.
	 * @return
	 */
	public SmartToken getToken() {
		return token;
	}
	
}
