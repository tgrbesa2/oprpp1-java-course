package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Inherited class from base Element class.
 * Stores operator as string.
 * @author tgrbesa
 *
 */
public class ElementOperator extends Element {
	private String symbol;
	
	public ElementOperator(String symbol) {
		this.symbol = symbol;
	}
	
	@Override
	public String asText() {
		return symbol;
	}

	public String getSymbol() {
		return symbol;
	}
	
}
