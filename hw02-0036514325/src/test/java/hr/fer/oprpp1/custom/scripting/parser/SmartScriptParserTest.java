package hr.fer.oprpp1.custom.scripting.parser;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import hr.fer.oprpp1.custom.scripting.nodes.DocumentNode;
import hr.fer.oprpp1.custom.scripting.nodes.EchoNode;
import hr.fer.oprpp1.custom.scripting.nodes.ForLoopNode;
import hr.fer.oprpp1.custom.scripting.nodes.TextNode;


public class SmartScriptParserTest {
		
	@Test
	public void testPrimjer1() {
		String text = readExample(1);
		SmartScriptParser ssp = new SmartScriptParser(text);
		DocumentNode document = ssp.getDocument();
		
		assertTrue(document.getChild(0) instanceof TextNode);
		assertTrue(document.numberOfChildren() == 1);
	}
	
	@Test
	public void testPrimjer2() {
		String text = readExample(2);
		SmartScriptParser ssp = new SmartScriptParser(text);
		DocumentNode document = ssp.getDocument();
		
		assertTrue(document.getChild(0) instanceof TextNode);
		assertTrue(document.numberOfChildren() == 1);
	}
	
	@Test
	public void testPrimjer3() {
		String text = readExample(3);
		SmartScriptParser ssp = new SmartScriptParser(text);
		DocumentNode document = ssp.getDocument();
		
		assertTrue(document.getChild(0) instanceof TextNode);
		assertTrue(document.numberOfChildren() == 1);
	}
	
	@Test
	public void testPrimjer4() {
		String text = readExample(4);
		
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(text));
	}
	
	@Test
	public void testPrimjer5() {
		String text = readExample(5);
		
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(text));
	}
	
	@Test
	public void testPrimjer6() {
		String text = readExample(6);
		SmartScriptParser ssp = new SmartScriptParser(text);
		DocumentNode document = ssp.getDocument();
		
		assertTrue(document.getChild(0) instanceof TextNode);
		assertTrue(document.getChild(1) instanceof EchoNode);
		assertTrue(document.numberOfChildren() == 3);
	}
	
	@Test
	public void testPrimjer7() {
		String text = readExample(7);
		SmartScriptParser ssp = new SmartScriptParser(text);
		DocumentNode document = ssp.getDocument();
		
		
		assertTrue(document.getChild(0) instanceof TextNode);
		assertTrue(document.getChild(1) instanceof EchoNode);
		assertTrue(document.numberOfChildren() == 3);
	}
	
	@Test
	public void testPrimjer8() {
		String text = readExample(8);
		
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(text));
	}
	
	@Test
	public void testPrimjer9() {
		String text = readExample(9);
		
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(text));
	}
	
	@Test
	public void testPrimjer10() {
		String text = readExample(10);
		SmartScriptParser ssp = new SmartScriptParser(text);
		DocumentNode document = ssp.getDocument();
		
		SmartScriptParser ssp2 = new SmartScriptParser(document.toString());
		DocumentNode document2 = ssp2.getDocument();
				
		assertTrue(document.equals(document2));
		assertTrue(document.getChild(0) instanceof TextNode);
		assertTrue(document.getChild(1) instanceof ForLoopNode);
		assertTrue(document.getChild(2) instanceof TextNode);
		assertTrue(document.getChild(3) instanceof ForLoopNode);		
	}
	
	@Test
	public void testPrimjer11() {
		String text = readExample(11);
		//Will throw cause of invalid for loop
		
		assertThrows(SmartScriptParserException.class, () -> new SmartScriptParser(text));
	}
	
	
	
	
	
	
	
	private String readExample(int n) {
		  try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("extra/primjer"+n+".txt")) {
		    if(is==null) throw new RuntimeException("Datoteka extra/primjer"+n+".txt je nedostupna.");
		    byte[] data = is.readAllBytes();
		    String text = new String(data, StandardCharsets.UTF_8);
		    return text;
		  } catch(IOException ex) {
		    throw new RuntimeException("Greška pri čitanju datoteke.", ex);
		  }
		}
}
