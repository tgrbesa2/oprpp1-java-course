package hr.fer.oprpp1.custom.scripting.lexer;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import hr.fer.oprpp1.hw02.prob1.Lexer;
import hr.fer.oprpp1.hw02.prob1.LexerException;

public class SmartScriptLexerTest {
	@Test
	public void testNotNull() {
		SmartScriptLexer lexer = new SmartScriptLexer("");
		
		assertNotNull(lexer.nextToken(), "Token was expected but null was returned");
	}
	
	@Test
	public void testNullInput() {
		// must throw!
		assertThrows(NullPointerException.class, () -> new SmartScriptLexer(null));
	}
	
	@Test
	public void testRadAfterEOF() {
		SmartScriptLexer lexer = new SmartScriptLexer("");

		// will obtain EOF
		lexer.nextToken();
		// will throw!
		assertThrows(LexerException.class, () -> lexer.nextToken());
	}
	
	@Test
	public void testForTag() {
		SmartScriptLexer lexer = new SmartScriptLexer("DADA{$FOR i 1 10 1$}");
		
		assertEquals(lexer.nextToken().getType(), SmartTokenType.TEXT);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.BEGIN_TAG);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.KEYWORD);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.IDENT);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.CONSTANT);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.CONSTANT);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.CONSTANT);
	}
	
	@Test
	public void testEchoTag() {
		SmartScriptLexer lexer = new SmartScriptLexer("DADA{$= i @foo  @sin \"John \\\"LONG\\\"\" 1$}");
		
		assertEquals(lexer.nextToken().getType(), SmartTokenType.TEXT);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.BEGIN_TAG);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.KEYWORD);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.IDENT);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.FUNCTION);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.FUNCTION);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.STRING);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.CONSTANT);
		assertEquals(lexer.nextToken().getType(), SmartTokenType.CLOSE_TAG);


	}
}
