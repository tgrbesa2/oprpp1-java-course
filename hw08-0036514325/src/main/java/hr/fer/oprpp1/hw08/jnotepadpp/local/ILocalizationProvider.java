package hr.fer.oprpp1.hw08.jnotepadpp.local;

public interface ILocalizationProvider {
	/**
	 * Adds localization listener.
	 * @param l
	 */
	void addLocalizationListener(ILocalizationListener l);
	
	/**
	 * Removes localization listener.
	 * @param l
	 */
	void removeLocalizationListener(ILocalizationListener l);
	
	/**
	 * Returns localization for specified key.
	 * @param s
	 * @return
	 */
	String getString(String s);
	
	/**
	 * Returns current langauge.
	 * @return
	 */
	String getCurrentLangauge(); 
}
