package hr.fer.oprpp1.hw08.jnotepadpp.impl;

import javax.swing.Action;
import javax.swing.JButton;

import hr.fer.oprpp1.hw08.jnotepadpp.SingleDocumentModel;

public class NotepadCloseButton extends JButton {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SingleDocumentModel docModel;	
	
	public NotepadCloseButton(SingleDocumentModel docModel, Action a) {
		super(a);
		this.docModel = docModel;
	}

	public SingleDocumentModel getDocModel() {
		return docModel;
	}
	
	
}
