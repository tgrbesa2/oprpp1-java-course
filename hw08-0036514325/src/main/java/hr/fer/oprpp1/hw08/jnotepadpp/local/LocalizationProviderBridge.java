package hr.fer.oprpp1.hw08.jnotepadpp.local;

public class LocalizationProviderBridge extends AbstractLocalizationProvider {
	private boolean connected;
	private ILocalizationProvider provider;
	private ILocalizationListener listener = () -> {
		this.fire();
	};
	
	public LocalizationProviderBridge(ILocalizationProvider provider) {
		this.provider = provider;
	}
	
	public void connect() {
		if(connected == false) {
			provider.addLocalizationListener(listener);
		}
		connected = true;
	}
	
	public void disconnect() {
		connected = false;
		provider.removeLocalizationListener(listener);
	}
	
	
	
	@Override
	public String getString(String s) {
		return provider.getString(s);
	}

	@Override
	public String getCurrentLangauge() {
		return provider.getCurrentLangauge();
	}
	
	
	
}
