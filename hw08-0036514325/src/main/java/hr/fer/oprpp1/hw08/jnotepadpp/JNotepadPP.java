package hr.fer.oprpp1.hw08.jnotepadpp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;

import hr.fer.oprpp1.hw08.jnotepadpp.impl.DefaultMultipleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.oprpp1.hw08.jnotepadpp.local.LocalizableAction;
import hr.fer.oprpp1.hw08.jnotepadpp.local.LocalizableMenu;
import hr.fer.oprpp1.hw08.jnotepadpp.local.LocalizationProvider;


//TODO Lokalizacija na njemackom, 
public class JNotepadPP extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private DefaultMultipleDocumentModel tabbedModel;
	private JToolBar statusBar;
	private Watch watch;
	private JMenu changeCase;
	private JMenu sortMenu;
	private String copyBuffer;
	private FormLocalizationProvider localizationProvider;
	
	
	public JNotepadPP() {
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setTitle("JNotepad++");
		setSize(800, 500);
		
		this.tabbedModel = new DefaultMultipleDocumentModel();
		tabbedModel.addMultipleDocumentListener(mdl);
		
		statusBar = new JToolBar("Status");
		statusBar.setBackground(Color.GRAY);
		statusBar.setLayout(new GridLayout(1, 0));
		statusBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		this.watch = new Watch();
		this.sortMenu = new LocalizableMenu("sort", LocalizationProvider.getInstance());
		this.changeCase = new LocalizableMenu("changecase", LocalizationProvider.getInstance());
		this.copyBuffer = "";
		this.localizationProvider = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);
		initGUI();
		
		this.addWindowListener(wl);
		setLocationRelativeTo(null);
	}
	
	private void initGUI() {
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(tabbedModel, BorderLayout.CENTER);
			
		createActions();
		createMenus();
		createToolbars();
	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new JNotepadPP().setVisible(true);
		});
	}
	
	
	private void createActions() {
		createEmptyDocument.putValue(Action.NAME, "Open");
		createEmptyDocument.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
		createEmptyDocument.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
		createEmptyDocument.putValue(Action.SHORT_DESCRIPTION, "Creates new unnamed document");
		
		loadDocument.putValue(Action.NAME, "Load");
		loadDocument.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control L"));
		loadDocument.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_L);
		loadDocument.putValue(Action.SHORT_DESCRIPTION, "Loads document from file system");
		
		
		saveDocument.putValue(Action.NAME, "Save");
		saveDocument.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
		saveDocument.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		saveDocument.putValue(Action.SHORT_DESCRIPTION, "Saves current working document");
		
		saveAsDocument.putValue(Action.NAME, "Save as");
		saveAsDocument.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control A"));
		saveAsDocument.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
		saveAsDocument.putValue(Action.SHORT_DESCRIPTION, "Saves document at specified location");
		
		exitApp.putValue(Action.NAME, "Exit");
		exitApp.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control Q"));
		exitApp.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_Q);
		exitApp.putValue(Action.SHORT_DESCRIPTION, "Closes application");
		
		
		showStatistics.putValue(Action.NAME, "Show statistics");
		showStatistics.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control B"));
		showStatistics.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_B);
		showStatistics.putValue(Action.SHORT_DESCRIPTION, "Displays document statistics");
		
		copyText.putValue(Action.NAME, "Copy");
		copyText.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
		copyText.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		copyText.putValue(Action.SHORT_DESCRIPTION, "Copies selected part of document");
		
		pasteText.putValue(Action.NAME, "Paste");
		pasteText.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
		pasteText.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_V);
		pasteText.putValue(Action.SHORT_DESCRIPTION, "Pastes cut/copied part of document");
		
		cutText.putValue(Action.NAME, "Cut");
		cutText.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
		cutText.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
		cutText.putValue(Action.SHORT_DESCRIPTION, "Cuts selected part of document");
	}
	
	
	private void createToolbars() {
		JToolBar toolBar = new JToolBar("Alati");
		toolBar.setFloatable(true);
		
		toolBar.add(new JButton(createEmptyDocument));
		toolBar.add(new JButton(loadDocument));
		toolBar.add(new JButton(saveDocument));
		toolBar.add(new JButton(saveAsDocument));
		
		toolBar.add(new JButton(showStatistics));
		toolBar.add(new JButton(copyText));
		toolBar.add(new JButton(cutText));
		toolBar.add(new JButton(pasteText));
		
		statusBar.setFloatable(true);
		
		JPanel panelInfo = new JPanel();
		panelInfo.setBackground(Color.GRAY);
		panelInfo.add(new JLabel("Ln: "));
		panelInfo.add(new JLabel("Col: "));
		panelInfo.add(new JLabel("Sel: "));
		
		statusBar.add(new JLabel("Length: "));
		statusBar.add(panelInfo);
		statusBar.add(watch);
		
		
		this.getContentPane().add(statusBar, BorderLayout.PAGE_END);
		this.getContentPane().add(toolBar, BorderLayout.PAGE_START);
	}
	
	private void createMenus() {
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new LocalizableMenu("file", LocalizationProvider.getInstance());
		menuBar.add(fileMenu);
		
		JMenu toolsMenu = new LocalizableMenu("tools", LocalizationProvider.getInstance());
		menuBar.add(toolsMenu);
		
		JMenu langaugeMenu = new LocalizableMenu("langauge", LocalizationProvider.getInstance());
		menuBar.add(langaugeMenu);
		
		
		
		fileMenu.add(new JMenuItem(createEmptyDocument)); 
		fileMenu.add(new JMenuItem(loadDocument));
		fileMenu.add(new JMenuItem(saveDocument));
		fileMenu.add(new JMenuItem(saveAsDocument));
		fileMenu.add(new JMenuItem(exitApp));
		
		toolsMenu.add(new JMenuItem(showStatistics));
		toolsMenu.add(new JMenuItem(copyText));
		toolsMenu.add(new JMenuItem(pasteText));
		toolsMenu.add(new JMenuItem(cutText));
		
		langaugeMenu.add(new JMenuItem(factoryActionLangauge("hr")));
		langaugeMenu.add(new JMenuItem(factoryActionLangauge("en")));
		langaugeMenu.add(new JMenuItem(factoryActionLangauge("de")));
		
		sortMenu.setEnabled(false);
		sortMenu.add(new JMenuItem(sortAscendingLine));
		sortMenu.add(new JMenuItem(sortDescendingLine));
		sortMenu.add(new JMenuItem(uniqueLines));
		menuBar.add(sortMenu);
		
		this.changeCase.setEnabled(false);
		this.changeCase.add(new JMenuItem(factoryActionCase("uppercase")));
		this.changeCase.add(new JMenuItem(factoryActionCase("lowercase")));
		this.changeCase.add(new JMenuItem(factoryActionCase("invertcase")));
		menuBar.add(changeCase);
		
		this.setJMenuBar(menuBar);
	}
	
	
	private Action createEmptyDocument = new LocalizableAction("open", LocalizationProvider.getInstance()) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			tabbedModel.createNewDocument();
		}
		
	};
	
	private Action loadDocument = new LocalizableAction("load", LocalizationProvider.getInstance()) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fc = new JFileChooser();
			fc.setDialogTitle("Open file");
			if(fc.showOpenDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
				return;
			}
			Path filePath = fc.getSelectedFile().toPath();
			if(!Files.isReadable(filePath)) {
				JOptionPane.showMessageDialog(
						JNotepadPP.this,
						"File doesn't exist!",
						"Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			
		
			
			tabbedModel.loadDocument(fc.getSelectedFile().toPath());
		}
		
	};
	
	private Action saveDocument = new LocalizableAction("save", LocalizationProvider.getInstance()) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			if(tabbedModel.getCurrentDocument().getFilePath() == null) {
				saveAsDocument.actionPerformed(e);
			} else {
				
				tabbedModel.saveDocument(tabbedModel.getCurrentDocument(), tabbedModel.getCurrentDocument().getFilePath());
			}
			
		}
		
	};
	
	private Action saveAsDocument = new LocalizableAction("saveAs", LocalizationProvider.getInstance()) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			helpDocumentSaving(tabbedModel.getCurrentDocument());
		}
	};
	
	private Action exitApp = new LocalizableAction("exit", LocalizationProvider.getInstance()) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JNotepadPP.this.wl.windowClosing(null);
		}
	};
	
	private Action showStatistics = new LocalizableAction("showStatistics", LocalizationProvider.getInstance()) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			if(tabbedModel.getCurrentDocument() == null) return;
			StringBuilder sb = new StringBuilder();
			sb.append("Number of characters: ");
			sb.append(tabbedModel.getCurrentDocument().getTextComponent().getText().toString().length() + "\n");
			
			sb.append("Number of characters without whitespace: ");
			sb.append(tabbedModel.getCurrentDocument().getTextComponent().getText().toString().replaceAll("\\s+", "").length() + "\n");
			
			sb.append("Number of rows: ");
			int count = 1;
			for(Character c : tabbedModel.getCurrentDocument().getTextComponent().getText().toString().toCharArray()) {
				if(c.equals('\n')) count++;
			}
			sb.append(count);
			
			JOptionPane.showMessageDialog(JNotepadPP.this, sb.toString());
		}
	};
	
	private Action copyText = new LocalizableAction("copy", LocalizationProvider.getInstance()) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea tmp = tabbedModel.getCurrentDocument().getTextComponent();
			int len = Math.abs(tmp.getCaret().getDot() - tmp.getCaret().getMark());
			int offset = Math.min(tmp.getCaret().getDot(), tmp.getCaret().getMark());
			
			try {
				copyBuffer = tmp.getText(offset, len);
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
			System.out.println(copyBuffer);
		}
		
	};
	
	private Action cutText = new LocalizableAction("cut", LocalizationProvider.getInstance()) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea tmp = tabbedModel.getCurrentDocument().getTextComponent();
			int len = Math.abs(tmp.getCaret().getDot() - tmp.getCaret().getMark());
			int offset = Math.min(tmp.getCaret().getDot(), tmp.getCaret().getMark());
			
			try {
				copyBuffer = tmp.getText(offset, len);
				tmp.getDocument().remove(offset, len);
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
			
		}
		
	};
	
	private Action pasteText = new LocalizableAction("paste", LocalizationProvider.getInstance()) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea tmp = tabbedModel.getCurrentDocument().getTextComponent();
			
			try {
				tmp.getDocument().insertString(tmp.getCaret().getDot(), copyBuffer, null);
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
			

		}
		
	};
	
	private Action factoryActionLangauge(String arg) {
		Action a = new LocalizableAction(arg, LocalizationProvider.getInstance()) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			
			@Override
			public void actionPerformed(ActionEvent e) {
				LocalizationProvider.getInstance().setLangauge(this.getKey());
			}
		};
		
		return a;
	}
	
	
	private Action factoryActionCase(String arg) {
		Action a = new LocalizableAction(arg, LocalizationProvider.getInstance()) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				JTextArea tmp = tabbedModel.getCurrentDocument().getTextComponent();
				int len = Math.abs(tmp.getCaret().getDot() - tmp.getCaret().getMark());
				int offset = Math.min(tmp.getCaret().getDot(), tmp.getCaret().getMark());
				try {
					String text = tabbedModel.getCurrentDocument().getTextComponent().getText(offset, len);
					if(arg.equals("uppercase")) {
						text = text.toUpperCase();
					} else if(arg.equals("lowercase")) {
						text = text.toLowerCase();
					} else {
						char[] chars = text.toCharArray();
						for(int i = 0; i < chars.length; i++) {
							char c = chars[i];
							if(Character.isLowerCase(c)) {
								chars[i] = Character.toUpperCase(c);
							} else {
								chars[i] = Character.toLowerCase(c);
							}
						}
						text = new String(chars);
					}
					
					tmp.getDocument().remove(offset, len);
					tmp.getDocument().insertString(offset, text, null);
				} catch (BadLocationException eX) {
					eX.printStackTrace();
				}
			}
			
		};
		
		
		return a;
	}

	private Action sortAscendingLine = new LocalizableAction("ascending", LocalizationProvider.getInstance()) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea tmp = tabbedModel.getCurrentDocument().getTextComponent();
			try {
				int firstLine = tabbedModel.getCurrentDocument().getTextComponent().getCaret().getDot();
				int lastLine = tabbedModel.getCurrentDocument().getTextComponent().getCaret().getMark();
				
				if(lastLine < firstLine) {
					int helper = firstLine;
					firstLine = lastLine;
					lastLine = helper;
				}
				firstLine = tmp.getLineOfOffset(firstLine);
				lastLine = tmp.getLineOfOffset(lastLine);
				
				
				for(int i = firstLine; i <= lastLine; i++) {
					int offsetStart = tmp.getLineStartOffset(i);
					int offsetEnd = tmp.getLineEndOffset(i);
					if((offsetEnd - offsetStart) == 0) {
						break;
					}
					
					List<String> lista = new ArrayList<>(Arrays.stream(tmp.getText(offsetStart, offsetEnd - offsetStart).replace("\n", "").split(" ")).collect(Collectors.toList()));
					Locale loc = new Locale(LocalizationProvider.getInstance().getCurrentLangauge());
					lista.sort(Collator.getInstance(loc));
					String sorted = "";
					for(int j = 0; j < lista.size(); j++) {
						if(j == 0) {
							sorted += lista.get(j);
							continue;
						}
						sorted += " " + lista.get(j);
					}
					sorted += "\n";
					
					tmp.getDocument().remove(offsetStart, offsetEnd - offsetStart);
					tmp.getDocument().insertString(offsetStart, sorted, null);
				}
				

			
				
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}
	};
	
	private Action sortDescendingLine = new LocalizableAction("descending", LocalizationProvider.getInstance()) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea tmp = tabbedModel.getCurrentDocument().getTextComponent();
			try {
				int firstLine = tabbedModel.getCurrentDocument().getTextComponent().getCaret().getDot();
				int lastLine = tabbedModel.getCurrentDocument().getTextComponent().getCaret().getMark();
				
				if(lastLine < firstLine) {
					int helper = firstLine;
					firstLine = lastLine;
					lastLine = helper;
				}
				firstLine = tmp.getLineOfOffset(firstLine);
				lastLine = tmp.getLineOfOffset(lastLine);
				
				
				for(int i = firstLine; i <= lastLine; i++) {
					int offsetStart = tmp.getLineStartOffset(i);
					int offsetEnd = tmp.getLineEndOffset(i);
					if((offsetEnd - offsetStart) == 0) {
						break;
					}
					
					List<String> lista = new ArrayList<>(Arrays.stream(tmp.getText(offsetStart, offsetEnd - offsetStart).replace("\n", "").split(" ")).collect(Collectors.toList()));
					Locale loc = new Locale(LocalizationProvider.getInstance().getCurrentLangauge());
					lista.sort(Collator.getInstance(loc).reversed());
					String sorted = "";
					for(int j = 0; j < lista.size(); j++) {
						if(j == 0) {
							sorted += lista.get(j);
							continue;
						}
						sorted += " " + lista.get(j);
					}
					sorted += "\n";
					
					tmp.getDocument().remove(offsetStart, offsetEnd - offsetStart);
					tmp.getDocument().insertString(offsetStart, sorted, null);
				}
				
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}
	};
	
	private Action uniqueLines = new LocalizableAction("unique", LocalizationProvider.getInstance()) {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			List<String> listBefore = new ArrayList<>();
			List<String> listAfter = new ArrayList<>();
			LinkedHashSet<String> set = new LinkedHashSet<>();
			JTextArea tmp = tabbedModel.getCurrentDocument().getTextComponent();
			
			int firstLine = tabbedModel.getCurrentDocument().getTextComponent().getCaret().getDot();
			int lastLine = tabbedModel.getCurrentDocument().getTextComponent().getCaret().getMark();
			
			
			if(lastLine < firstLine) {
				int helper = firstLine;
				firstLine = lastLine;
				lastLine = helper;
			}
			
			try {
				firstLine = tmp.getLineOfOffset(firstLine);
				lastLine = tmp.getLineOfOffset(lastLine);
				
				for(int i = 0; i < firstLine; i++) {
					int offsetStart = tmp.getLineStartOffset(i);
					int offsetEnd = tmp.getLineEndOffset(i);
					
					if((offsetEnd - offsetStart) == 0) {
						break;
					}
					listBefore.add(tmp.getText(offsetStart, offsetEnd - offsetStart).replace("\n", ""));
				}
				for(int i = firstLine; i <= lastLine; i++) {
					int offsetStart = tmp.getLineStartOffset(i);
					int offsetEnd = tmp.getLineEndOffset(i);
					
					if((offsetEnd - offsetStart) == 0) {
						break;
					}
					if(!set.contains(tmp.getText(offsetStart, offsetEnd - offsetStart).replace("\n", ""))) {
						set.add(tmp.getText(offsetStart, offsetEnd - offsetStart).replace("\n", ""));
					}
				}
				
				for(int i = lastLine + 1; i < tmp.getLineOfOffset(tmp.getText().length()); i++) {
					int offsetStart = tmp.getLineStartOffset(i);
					int offsetEnd = tmp.getLineEndOffset(i);
					
					if((offsetEnd - offsetStart) == 0) {
						break;
					}
					listAfter.add(tmp.getText(offsetStart, offsetEnd - offsetStart).replace("\n", ""));
				}
				tmp.getDocument().remove(0, tmp.getText().length());
				
				StringBuilder sb = new StringBuilder();
				for(var s : listBefore) {
					sb.append(s + '\n');
				}
				
				String[] array = new String[set.size()];
				set.toArray(array);
				
				for(int i = 0; i < array.length; i++) {
					sb.append(array[i] + '\n');
				}
				
				for(var s : listAfter) {
					sb.append(s + '\n');
				}
				tmp.getDocument().insertString(0, sb.toString(), null);
				
				
			} catch (BadLocationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	};

 	private MultipleDocumentListener mdl = new MultipleDocumentListener() {

		@Override
		public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
			if(currentModel == null) {
				JNotepadPP.this.setTitle("JNotepad++");
				((JLabel)statusBar.getComponent(0)).setText("Length: ");
				JPanel panel = ((JPanel)statusBar.getComponent(1));
				((JLabel)panel.getComponent(0)).setText("Col: ");
				((JLabel)panel.getComponent(1)).setText("Sel: ");
				((JLabel)panel.getComponent(2)).setText("Sel: ");
			} else if(currentModel.getFilePath() == null) {
				JNotepadPP.this.setTitle("(unnamed) - JNotepad++");
			} else if(currentModel != null) {
				JNotepadPP.this.setTitle(currentModel.getFilePath().toString() + " - JNotepad++");
			}
			
			
			if(previousModel != null) {
				CaretListener[] tmp = previousModel.getTextComponent().getCaretListeners();
				for(var l : tmp) {
					previousModel.getTextComponent().removeCaretListener(l);
				}
				
			}
			
			if(currentModel != null) {
				tabbedModel.getCurrentDocument().getTextComponent().addCaretListener(caretPosition);
				JTextArea tmp = tabbedModel.getCurrentDocument().getTextComponent();
				
				caretHelper(tmp);
			}
			

		}

		@Override
		public void documentAdded(SingleDocumentModel model) {
		}

		@Override
		public void documentRemoved(SingleDocumentModel model) {
		}
		
	};
	
	
	//TODO Izmjeniti na engleski
	private WindowListener wl = new WindowAdapter() {
		@Override
		public void windowClosing(WindowEvent e) {
			
			
			
			Iterator<SingleDocumentModel> it = tabbedModel.iterator();
			while(it.hasNext()) {
				SingleDocumentModel tmp = it.next();
				if(tmp.isModified()) {
					int rezultat = JOptionPane.showConfirmDialog(JNotepadPP.this,
							"Document " + (tmp.getFilePath() == null ? "unnamed" : tmp.getFilePath().toString()) + "  is not saved. Do you want to save?",
							"Warning", JOptionPane.YES_NO_CANCEL_OPTION);
					switch(rezultat) {
					
					case 0:
						if(tmp.getFilePath() == null) {
							helpDocumentSaving(tmp);
						} else {
							tabbedModel.saveDocument(tmp, tmp.getFilePath());
						}
						break;
					case 1:
						break;
					case 2:
					case JOptionPane.CLOSED_OPTION:
						return;
					}
				}
			}
			
			dispose();
			watch.setStop();

		}
			
	};
	
	private void helpDocumentSaving(SingleDocumentModel model) {
		JFileChooser jfc = new JFileChooser();
		jfc.setDialogTitle("Save document");
		if(jfc.showSaveDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
			return;
		}
		
		Path p = jfc.getSelectedFile().toPath();
		
		if(Files.exists(p) && !p.equals(model.getFilePath())) {
			int result = JOptionPane.showConfirmDialog(
						JNotepadPP.this,
						"File already Exists. Do you wish to proceed ?", 
						"Warning",
						JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE
						);
			if(result == 1) return;
			
		}
		
		
		tabbedModel.saveDocument(model, p);
	}
	
	private CaretListener caretPosition = new CaretListener() {

		@Override
		public void caretUpdate(CaretEvent e) {
		
			JTextArea tmp = (JTextArea) e.getSource();
			caretHelper(tmp);
		}
		
	};
	
	private static class Watch extends JLabel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private boolean stop;
		
		public Watch() {
			stop = false;
			this.setHorizontalAlignment(SwingConstants.RIGHT);
			
			updateTime();
			
			Thread t = new Thread(() -> {
				while(true) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(stop) break;
					SwingUtilities.invokeLater(() -> {
						updateTime();
					});
				}
			});
			t.setDaemon(true);
			t.start();
		}
		
		private void updateTime() {
			this.setAlignmentX(RIGHT_ALIGNMENT);
			this.setText("    " + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));
		}
		
		private void setStop() {
			stop = true;
		}
		
	}
	
	private void caretHelper(JTextArea tmp) {
		
		
		JLabel labelLength = (JLabel) statusBar.getComponent(0);
		JPanel panelInfo = (JPanel) statusBar.getComponent(1);
		JLabel label2 = (JLabel) panelInfo.getComponent(0);
		JLabel label3 =	(JLabel) panelInfo.getComponent(1);
		JLabel label4 = (JLabel) panelInfo.getComponent(2);
		
		
		labelLength.setText("Length: " + tmp.getText().length());
		int lineCount = 1;
		int columnNum = 1;
		try {
			lineCount += tmp.getLineOfOffset(tmp.getCaretPosition());
			label2.setText("Ln: " + lineCount);
		} catch (BadLocationException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		try {
			columnNum += tmp.getCaretPosition() - tmp.getLineStartOffset(lineCount - 1); 
			label3.setText("Col: " + columnNum);
		} catch (BadLocationException e1) {
		
			e1.printStackTrace();
		}
		
		Integer dif = Math.abs(tmp.getCaret().getDot() - tmp.getCaret().getMark());
		sortMenu.setEnabled(false);
		changeCase.setEnabled(false);
		if(dif > 0) {
			changeCase.setEnabled(true);
			sortMenu.setEnabled(true);
		}
		label4.setText("Sel: " +  dif);
	}
	
}