package hr.fer.oprpp1.hw08.jnotepadpp;

import java.nio.file.Path;

public interface MultipleDocumentModel extends Iterable<SingleDocumentModel> {
	/**
	 * Creates new document tab.
	 * @return
	 */
	SingleDocumentModel createNewDocument();
	
	/**
	 * Returns currently selected tab/document.
	 * @return
	 */
	SingleDocumentModel getCurrentDocument();
	
	/**
	 * Loads document and opens in new tab.
	 * If document is already opened, view is
	 * switched to that tab/document.
	 * @param path
	 * @return
	 */
	SingleDocumentModel loadDocument(Path path);
	
	/**
	 * Saves current working document.
	 * @param model
	 * @param newPath
	 */
	void saveDocument(SingleDocumentModel model, Path newPath);
	
	/**
	 * Removes document from editing tab.
	 * No changes are saved.
	 * @param model
	 */
	void closeDocument(SingleDocumentModel model);
	
	/**
	 * Adds document listener.
	 * @param l
	 */
	void addMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Removes document listener.
	 * @param l
	 */
	void removeMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Returns number of currently opened documents.
	 * @return
	 */
	int getNumberOfDocuments();
	
	/**
	 * Returns document at specified index.
	 * @param index
	 * @return
	 */
	SingleDocumentModel getDocument(int index);
}
