package hr.fer.oprpp1.hw08.jnotepadpp.local;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractLocalizationProvider implements ILocalizationProvider {
	List<ILocalizationListener> listListeners;
	
	public AbstractLocalizationProvider() {
		this.listListeners = new ArrayList<>();
	}
	
	@Override
	public void addLocalizationListener(ILocalizationListener l) {
		listListeners.add(l);
		
	}
	
	@Override
	public void removeLocalizationListener(ILocalizationListener l) {
		listListeners.remove(l);	
	}
	
	public void fire() {
		for(var listener : listListeners) {
			listener.localizationChanged();
		}
	}
	
	
	
}
