package hr.fer.oprpp1.hw08.jnotepadpp;

import java.nio.file.Path;

import javax.swing.JTextArea;

public interface SingleDocumentModel {
	/**
	 * Returns text area component of this document.
	 * @return
	 */
	JTextArea getTextComponent();
	
	/**
	 * Returns file path of document.
	 * @return
	 */
	Path getFilePath();
	
	/**
	 * Sets file path for document.
	 * @param path
	 */
	void setFilePath(Path path);
	
	/**
	 * Is document modified since last save.
	 * @return
	 */
	boolean isModified();
	
	/**
	 * Sets flag of modification to specified value.
	 * @param modified
	 */
	void setModified(boolean modified);
	
	/**
	 * Adds document listener.
	 * @param l
	 */
	void addSingleDocumentListener(SingleDocumentListener l);
	
	/**
	 * Removes document listener.
	 * @param l
	 */
	void removeSingleDocumentListener(SingleDocumentListener l);
	
	 boolean isSecondModification();

	void setSecondModification(boolean secondModification);
}
