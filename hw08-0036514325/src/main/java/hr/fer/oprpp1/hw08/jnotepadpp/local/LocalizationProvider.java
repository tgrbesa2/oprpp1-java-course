package hr.fer.oprpp1.hw08.jnotepadpp.local;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocalizationProvider extends AbstractLocalizationProvider {
	private static final LocalizationProvider instance = new LocalizationProvider();
	
	private String langauge;
	private ResourceBundle bundle;
	
	private LocalizationProvider() {
		super();
		langauge = "en";
		Locale locale = Locale.forLanguageTag(langauge);
		bundle = ResourceBundle.getBundle("hr.fer.oprpp1.hw08.jnotepadpp.local.prijevodi", locale);
		
	}
	
	public static LocalizationProvider getInstance() {
		return instance;
	}
	
	public void setLangauge(String langauge) {
		LocalizationProvider.instance.langauge = langauge;
		Locale locale = Locale.forLanguageTag(langauge);
		
		LocalizationProvider.instance.bundle = ResourceBundle.getBundle("hr.fer.oprpp1.hw08.jnotepadpp.local.prijevodi", locale);
		this.fire();
	}
	
	@Override
	public String getString(String s) {
		return bundle.getString(s);
	}

	@Override
	public String getCurrentLangauge() {
		return langauge;
	}

}
