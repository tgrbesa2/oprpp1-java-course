package hr.fer.oprpp1.hw08.jnotepadpp.local;

import javax.swing.JMenu;

public class LocalizableMenu extends JMenu {
	
	private String key;
	private ILocalizationProvider provider;
	private ILocalizationListener listener = () ->  {
		this.setText(provider.getString(key));
	};
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public LocalizableMenu(String key, ILocalizationProvider provider) {
		this.key = key;
		this.provider = provider;
		provider.addLocalizationListener(listener);
		this.setText(provider.getString(key));
		
	}
}
