package hr.fer.oprpp1.hw08.jnotepadpp.impl;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import hr.fer.oprpp1.hw08.jnotepadpp.SingleDocumentListener;
import hr.fer.oprpp1.hw08.jnotepadpp.SingleDocumentModel;

public class DefaultSingleDocumentModel implements SingleDocumentModel {
	private Path filePath;
	private JTextArea textComponent;
	private boolean modified;
	private List<SingleDocumentListener> listeners;
	private boolean secondModification;
	
	
	


	public DefaultSingleDocumentModel(Path filePath, JTextArea textComponent) {
		super();
		this.filePath = filePath;
		this.textComponent = textComponent;
		this.modified = false;
		this.secondModification = false;
		this.listeners = new ArrayList<SingleDocumentListener>();
		
		
		textComponent.getDocument().addDocumentListener(textWritten);

	}

	@Override
	public JTextArea getTextComponent() {
		return textComponent;
	}

	@Override
	public Path getFilePath() {
		return filePath;
	}

	@Override
	public void setFilePath(Path path) {
		if(path == null) throw new IllegalArgumentException("File path cannot be null!");
		
		this.filePath = path;
		
		for(var l : listeners) {
			l.documentFilePathUpdated(this);
		}
		
	}

	@Override
	public boolean isModified() {
		return modified;
	}

	@Override
	public void setModified(boolean modified) {
		this.modified = modified;
		
		for(var l : listeners) {
			l.documentModifyStatusUpdated(this);
		}
	}

	@Override
	public void addSingleDocumentListener(SingleDocumentListener l) {
		listeners.add(l);
	}

	@Override
	public void removeSingleDocumentListener(SingleDocumentListener l) {
		listeners.remove(l);
	}
	
	public boolean isSecondModification() {
		return secondModification;
	}

	public void setSecondModification(boolean secondModification) {
		this.secondModification = secondModification;
	}
	
	private DocumentListener textWritten = new DocumentListener() {

		@Override
		public void insertUpdate(DocumentEvent e) {
			DefaultSingleDocumentModel.this.setModified(true);
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			DefaultSingleDocumentModel.this.setModified(true);
		}

		@Override
		public void changedUpdate(DocumentEvent e) {

		}
		
	};
	
	 
	


	
}
