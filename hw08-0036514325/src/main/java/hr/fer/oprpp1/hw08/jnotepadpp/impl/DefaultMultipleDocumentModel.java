package hr.fer.oprpp1.hw08.jnotepadpp.impl;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import hr.fer.oprpp1.hw08.jnotepadpp.JNotepadPP;
import hr.fer.oprpp1.hw08.jnotepadpp.MultipleDocumentListener;
import hr.fer.oprpp1.hw08.jnotepadpp.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadpp.SingleDocumentListener;
import hr.fer.oprpp1.hw08.jnotepadpp.SingleDocumentModel;

public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<SingleDocumentModel> listDocuments;
	private SingleDocumentModel currentDocument;
	private List<MultipleDocumentListener> multipleModelListener;
	
	public DefaultMultipleDocumentModel() {
		listDocuments = new ArrayList<>();
		multipleModelListener = new ArrayList<>();
		currentDocument = null;
		
		this.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				DefaultMultipleDocumentModel tmp = DefaultMultipleDocumentModel.this;
			
				SingleDocumentModel previous = tmp.currentDocument;
				if(tmp.getNumberOfDocuments() == 0) {
					tmp.currentDocument = null;
				} else {
					tmp.currentDocument = tmp.getDocument(tmp.getSelectedIndex());
				}
				for(var l : multipleModelListener) {
					l.currentDocumentChanged(previous, tmp.currentDocument);
				}
			}
			
		});
	}
	
	@Override
	public Iterator<SingleDocumentModel> iterator() {
		return listDocuments.iterator();
	}

	@Override
	public SingleDocumentModel createNewDocument() {
		return helperNewDocument(null, new JTextArea());
	}

	@Override
	public SingleDocumentModel getCurrentDocument() {
		return currentDocument;
	}

	@Override
	public SingleDocumentModel loadDocument(Path path) {
		if(!Files.exists(path)) {
			throw new RuntimeException("File doesn't exist!");
		}
		if(Files.isDirectory(path)) {
			throw new RuntimeException("File is directory!");
		}
		
		byte[] data;
		try {
			data = Files.readAllBytes(path);
		} catch (IOException e1) {
			throw new RuntimeException("Error while reading file");
		}
		
		SingleDocumentModel tmp = helperNewDocument(path, new JTextArea(new String(data, StandardCharsets.UTF_8)));
		tmp.setSecondModification(false);
		tmp.setModified(false);
				
		return tmp;
	}

	@Override
	public void saveDocument(SingleDocumentModel model, Path newPath) {
		if(newPath == null) {
			newPath = model.getFilePath();
		}
		byte[] data = model.getTextComponent().getText().getBytes(StandardCharsets.UTF_8);
		if(model.getFilePath() != newPath) {
			model.setFilePath(newPath);
			for(var l : multipleModelListener) {
				l.currentDocumentChanged(model, model);
			}
		}
		try {
			Files.write(newPath, data);
		} catch (IOException e1) {
			throw new RuntimeException("Something went wrong while writing file!");
		}

		
		model.setSecondModification(false);
		model.setModified(false);
		
		
	}

	@Override
	public void closeDocument(SingleDocumentModel model) {
		int tmp = listDocuments.indexOf(model);
		
		listDocuments.remove(model);
		this.remove(tmp);
	}

	@Override
	public void addMultipleDocumentListener(MultipleDocumentListener l) {
		multipleModelListener.add(l);
	}

	@Override
	public void removeMultipleDocumentListener(MultipleDocumentListener l) {
		multipleModelListener.remove(l);
	}

	@Override
	public int getNumberOfDocuments() {
		return listDocuments.size();
	}

	@Override
	public SingleDocumentModel getDocument(int index) {
		return listDocuments.get(index);
	}
	
	private SingleDocumentModel helperNewDocument(Path p, JTextArea textArea) {
		SingleDocumentModel tmp = new DefaultSingleDocumentModel(p, textArea);
		listDocuments.add(tmp);
		currentDocument = tmp;
		tmp.addSingleDocumentListener(modelChanged);
		
		
		this.add(new JScrollPane(tmp.getTextComponent()));
		
		if(p == null) {
			this.setTabComponentAt(this.getNumberOfDocuments() - 1,  createPanel(tmp, "green", "unnamed"));
			this.setToolTipTextAt(this.getNumberOfDocuments() - 1, "unnamed");
		} else {
			this.setTabComponentAt(this.getNumberOfDocuments() - 1,  createPanel(tmp, "green", p.toString().substring(p.toString().lastIndexOf("/") + 1)));
			this.setToolTipTextAt(this.getNumberOfDocuments() - 1, p.toString());
		}
		this.setSelectedIndex(this.getNumberOfDocuments() - 1);


		
		return tmp;
	}
	
	private SingleDocumentListener modelChanged = new SingleDocumentListener() {

		@Override
		public void documentModifyStatusUpdated(SingleDocumentModel model) {
			if(model.isSecondModification()) return;
			
			DefaultMultipleDocumentModel tmp = DefaultMultipleDocumentModel.this;
			String color;
			if(model.isModified()) {
				model.setSecondModification(true);
				color = "red";
			} else {
				color = "green";
			}
			if(model.getFilePath() == null) {
				tmp.setTabComponentAt(tmp.listDocuments.indexOf(model), createPanel(model, color, "unnamed"));
			} else {
				tmp.setTabComponentAt(tmp.listDocuments.indexOf(model), createPanel(model, color, model.getFilePath().toString().substring(model.getFilePath().toString().lastIndexOf("/") + 1)));
			}
		}

		@Override
		public void documentFilePathUpdated(SingleDocumentModel model) {
			DefaultMultipleDocumentModel tmp = DefaultMultipleDocumentModel.this;
			tmp.setToolTipTextAt(listDocuments.indexOf(model), model.getFilePath().toString());
		}
		
	};
	
	private JPanel createPanel(SingleDocumentModel model, String argColor, String argName) {
		JPanel panel = new JPanel();
		panel.setOpaque(false);
		
		closeTab.putValue(Action.NAME, "x");
		panel.add(new JLabel(loadDiskete(argColor)));
		panel.add(new JLabel(argName));
		panel.add(new NotepadCloseButton(model, closeTab));
		
		return panel;
	}
	
	
	private Action closeTab = new AbstractAction() {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			DefaultMultipleDocumentModel tmp = DefaultMultipleDocumentModel.this;
			
			if(!tmp.getCurrentDocument().isModified()) {
				tmp.closeDocument(((NotepadCloseButton) e.getSource()).getDocModel());
				return;
			}
			int result = JOptionPane.showConfirmDialog(
					tmp,
					"Document is not saved! Close anyway?", 
					"Warning",
					JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE
					);
			if(result == JOptionPane.YES_OPTION) {
				tmp.closeDocument(((NotepadCloseButton) e.getSource()).getDocModel());

			}
		}
		
		
	};
	
	private ImageIcon loadDiskete(String arg) {
		InputStream is = this.getClass().getResourceAsStream("icons/" + arg + "-diskete.png");
		if(is==null) throw new NullPointerException("No picture present!");
		byte[] bytes = null;
		try {
			bytes = is.readAllBytes();
			is.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ImageIcon icon =  new ImageIcon(bytes);
		Image image = icon.getImage();
		Image newImage = image.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		
		return new ImageIcon(newImage);
	}

}
